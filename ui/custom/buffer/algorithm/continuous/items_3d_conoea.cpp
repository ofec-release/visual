#include "items_3d_conoea.h"
#include "../../../../core/buffer/geometrics/geom_3d.h"
#include "../../../../core/global_ui.h"

namespace ofec_demo {
	void PopFitLndscp3d::updateMultiPops(
		const std::vector<std::unique_ptr<ofec::Individual<>>> &mp,
		const std::vector<int> &pi, int np,
		const std::vector<std::pair<ofec::Real, ofec::Real>> &range,
		const std::pair<ofec::Real, ofec::Real> &range_fit)
	{
		std::vector<std::array<ofec::Real, 3>> pts(mp.size());
		for (size_t i = 0; i < mp.size(); ++i) {
			pts[i][0] = (mp[i]->variable()[0] - range[0].first) / (range[0].second - range[0].first) * 10 - 5;
			pts[i][1] = (mp[i]->variable()[1] - range[1].first) / (range[1].second - range[1].first) * 10 - 5;
			pts[i][2] = (mp[i]->fitness() - range_fit.first) / (range_fit.second - range_fit.first) * 6 - 3 + 0.05;
		}
		auto id_pop_scalars = vtkSmartPointer<vtkFloatArray>::New();
		id_pop_scalars->SetNumberOfTuples(mp.size());
		for (size_t i = 0; i < mp.size(); ++i)
			id_pop_scalars->SetTuple1(i, pi[i]);			
		m_multi_pop = Geom3d::createColoredPoints(pts, id_pop_scalars, g_rand_color_lut, 4);
	}

	void PopFitLndscp3d::addMultiPops(vtkRenderer *renderer) {
		renderer->AddActor(m_multi_pop);
	}

	void PopFitLndscp3d::removeMultiPops(vtkRenderer *renderer) {
		renderer->RemoveActor(m_multi_pop);
	}

	void PopFitLndscp3d::setMultiPopsVisible(bool flag) {
		m_multi_pop->SetVisibility(flag);
	}
	
	void PopVarSpace3d::updateMultiPops(
		const std::vector<std::unique_ptr<ofec::Individual<>>> &mp, 
		const std::vector<int> &pi, int np,
		const std::vector<std::pair<ofec::Real, ofec::Real>> &range)
	{
		std::vector<std::array<ofec::Real, 3>> pts(mp.size());
		for (size_t i = 0; i < mp.size(); ++i) {
			pts[i][0] = (mp[i]->variable()[0] - range[0].first) / (range[0].second - range[0].first) * 10 - 5;
			pts[i][1] = (mp[i]->variable()[1] - range[1].first) / (range[1].second - range[1].first) * 10 - 5;
			pts[i][2] = (mp[i]->variable()[2] - range[2].first) / (range[2].second - range[2].first) * 10 - 5;
		}
		auto id_pop_scalars = vtkSmartPointer<vtkFloatArray>::New();
		id_pop_scalars->SetNumberOfTuples(mp.size());
		for (size_t i = 0; i < mp.size(); ++i)
			id_pop_scalars->SetTuple1(i, pi[i]);
		m_multi_pop = Geom3d::createColoredPoints(pts, id_pop_scalars, g_rand_color_lut, 4);
	}
	
	void PopVarSpace3d::addMultiPops(vtkRenderer *renderer) {
		renderer->AddActor(m_multi_pop);
	}
	
	void PopVarSpace3d::removeMultiPops(vtkRenderer *renderer) {
		renderer->RemoveActor(m_multi_pop);
	}
	
	void PopVarSpace3d::setMultiPopsVisible(bool flag) {
		m_multi_pop->SetVisibility(flag);
	}
}