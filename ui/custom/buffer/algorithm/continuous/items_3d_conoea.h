#ifndef OFEC_DEMO_ITEMS_3D_CONOEA_H
#define OFEC_DEMO_ITEMS_3D_CONOEA_H

#include "../../../../core/buffer/geometrics/geom_3d.h"
#include <vtkActor.h>
#include <vtkSmartPointer.h>
#include <vtkRenderer.h>
#include <core/algorithm/individual.h>

namespace ofec_demo {
	class PopFitLndscp3d {
	private:
		vtkSmartPointer<vtkActor> m_multi_pop;
	public:
		void updateMultiPops(
			const std::vector<std::unique_ptr<ofec::Individual<>>>& mp, 
			const std::vector<int>& pi, int np, 
			const std::vector<std::pair<ofec::Real, ofec::Real>> &range,
			const std::pair<ofec::Real, ofec::Real>& range_fit);
		void addMultiPops(vtkRenderer *renderer);
		void removeMultiPops(vtkRenderer *renderer);
		void setMultiPopsVisible(bool flag);
	};

	class PopVarSpace3d {
	private:
		vtkSmartPointer<vtkActor> m_multi_pop;
	public:
		void updateMultiPops(
			const std::vector<std::unique_ptr<ofec::Individual<>>> &mp,
			const std::vector<int> &pi, int np,
			const std::vector<std::pair<ofec::Real, ofec::Real>> &range);
		void addMultiPops(vtkRenderer *renderer);
		void removeMultiPops(vtkRenderer *renderer);
		void setMultiPopsVisible(bool flag);
	};
}

#endif // !OFEC_DEMO_ITEMS_3D_CONOEA_H
