//AddCustom BufferConOEA "" ConOP

#ifndef OFEC_DEMO_BUFFER_COEA_H
#define OFEC_DEMO_BUFFER_COEA_H

#include "../../../../core/buffer/buffer_ea.h"
#include "items_2d_conoea.h"
#include "items_3d_conoea.h"
#include <core/algorithm/individual.h>
#include <core/problem/continuous/continuous.h>
#include "../../../../core/buffer/graphic_view/chart_view_2d.h"
#include <vtkChartXY.h>

namespace ofec_demo {
	/* Buffer of Continuous Optimization Evolutionary Algorithm */
	class BufferConOEA : public BufferEA<ofec::Individual<>> {
	private:
		struct BufferDatum {
			int id_buffer = 0;
			std::unique_ptr<PopFitLndscp2d> pop_fit_lndscp_2d;
			std::unique_ptr<PopFitLndscp3d> pop_fit_lndscp_3d;
			std::unique_ptr<PopVarSpace2d> pop_var_space_2d;
			std::unique_ptr<PopVarSpace3d> pop_var_space_3d;
			std::vector<bool> is_opt_vars_found;
			vtkSmartPointer<vtkChartXY> chart_error;
		};
		std::map<int, std::unique_ptr<BufferDatum>> m_buffer_data;

		std::unique_ptr<ChartView2d> m_chart_error_view_2d;			/* 2D view for error */

		BufferDatum *m_cur_datum;

		std::vector<ofec::Real> m_errors, m_iters;
		double m_color_error[3] = { 0,0,0 };

	public:
		BufferConOEA(GraphicsArea *graphic_dock_set, int id_alg, BufferPro *buffer_pro);
		~BufferConOEA() override;
		void initializeGraphicViews() override;
		bool graphicViewsInitialized() override;
		void appendBuffer(int max_buffer_id) override;
		void popupBuffer(int buffer_id) override;
		void updateViews() override;
		void updateGraphicViews(int buffer_id) override;
		void setSolutionVisible(bool flag) override;

	private:
		void updatePopFitness(PopInfo *pop_info);
		void updatePopFitLndscp3d(PopInfo *pop_info, BufferDatum *buffer_datum);
		void updatePopFitLndscp2d(PopInfo *pop_info, BufferDatum *buffer_datum);
		void updatePopVarSpace2d(PopInfo *pop_info, BufferDatum *buffer_datum);
		void updatePopVarSpace3d(PopInfo *pop_info, BufferDatum *buffer_datum);
		void updateChartError(PopInfo* pop_info, BufferDatum* buffer_datum);
		void updateOptimaFound(BufferDatum *buffer_datum);
		void removeCurDatum();
		void addNewDatum(int buffer_id);
	};
}

#endif // !OFEC_DEMO_BUFFER_COEA_H
