#ifndef OFEC_DEMO_ITEMS_2D_CONOEA_H
#define OFEC_DEMO_ITEMS_2D_CONOEA_H

#include "../../../../core/buffer/geometrics/geom_2d.h"
#include <core/algorithm/individual.h>

namespace ofec_demo {
	class PopFitLndscp2d {
	private:
		std::list<std::unique_ptr<Points2d>> m_multi_pop;
	public:
		void updateMultiPops(
			const std::vector<std::unique_ptr<ofec::Individual<>>> &mp,
			const std::vector<int> &pi, int np,
			const std::vector<std::pair<ofec::Real, ofec::Real>> &boundary,
			const std::pair<ofec::Real, ofec::Real> &range_fit
		);
		void addMultiPops(QGraphicsScene *scene);
		void removeMultiPops(QGraphicsScene *scene);
		void setMultiPopsVisible(bool flag);
	};

	class PopVarSpace2d {
	private:
		std::list<std::unique_ptr<Points2d>> m_multi_pop;
	public:
		void updateMultiPops(
			const std::vector<std::unique_ptr<ofec::Individual<>>> &mp, 
			const std::vector<int> &pi, int np, 
			const std::vector<std::pair<ofec::Real, ofec::Real>> &range);
		void addMultiPops(QGraphicsScene *scene);
		void removeMultiPops(QGraphicsScene *scene);
		void setMultiPopsVisible(bool flag);
	};
}

#endif // !OFEC_DEMO_ITEMS_2D_CONOEA_H
