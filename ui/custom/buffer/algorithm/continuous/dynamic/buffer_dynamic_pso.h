//AddCustom BufferDynamicPSO "^(?:CP|FTMP|Hm|SP)SOR?$" ConOP,DOP

#ifndef OFEC_DEMO_BUFFER_DYNAMIC_PSO_H
#define OFEC_DEMO_BUFFER_DYNAMIC_PSO_H

#include "buffer_dynamic.h"
#include <vtkChartXY.h>
#include "../../../../../core/buffer/graphic_view/chart_view_2d.h"
#include <ui/core/buffer/buffer_ea.h>

namespace ofec_demo {
	/* Text2d */
	class PopVarTextSpace2d {
	private:
		std::list<std::unique_ptr<Text2d>> m_multi_pop_text;
		std::list<std::unique_ptr<Rect2d>> m_multi_pop_rect;
		std::list<std::unique_ptr<Circle2d>> m_multi_pop_cirle;
	public:
		void updateMultiPopsTextRect(const std::vector<std::unique_ptr<ofec::Individual<>>>& mp, 
			const std::vector<int>& pi, int np,
			std::vector<bool> state,
			const std::vector<std::pair<ofec::Real, ofec::Real>>& range,
			ofec::Real x, ofec::Real y);
		void updateMultiPopsGbestLocationCircle(std::vector<ofec::Solution<>> state,
			const std::vector<std::pair<ofec::Real, ofec::Real>>& range);
		void addMultiPopsText(QGraphicsScene* scene);
		void removeMultiPopsText(QGraphicsScene* scene);
		void setMultiPopsTextVisible(bool flag);
	};


	/* Buffer of dynamic PSOs */
	class BufferDynamicPSO : public BufferDynamicConOEA {
	private:
		struct BufferDatum {
			std::unique_ptr<PopVarTextSpace2d> pop_var_text_space_2d;
			int id_buffer = 0;
		};
		std::map<int, std::unique_ptr<BufferDatum>> m_buffer_data;
		BufferDatum* m_cur_datum;

	public:
		BufferDynamicPSO(GraphicsArea *graphics_area, int id_alg, BufferPro* buffer_pro);
		virtual ~BufferDynamicPSO();
		void initializeGraphicViews() override;
		bool graphicViewsInitialized() override;
		void appendBuffer(int max_buffer_id) override;
		void popupBuffer(int buffer_id) override;
		void updateGraphicViews(int buffer_id) override;
		void updateViews() override;
	private:
		void updatePopText(BufferDatum* buffer_datum, std::vector<bool> &state, std::vector<ofec::Solution<>> gbests, PopInfo* pop_info);
		void removeCurDatum();
		void addNewDatum(int buffer_id);
	};
}
#endif // !OFEC_DEMO_BUFFER_DYNAMIC_PSO_H