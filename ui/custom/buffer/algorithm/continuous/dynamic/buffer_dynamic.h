//AddCustom BufferDynamicConOEA "" ConOP,DOP

#ifndef OFEC_DEMO_BUFFER_DYNAMIC_COEA_H
#define OFEC_DEMO_BUFFER_DYNAMIC_COEA_H

#include "../buffer_conoea.h"

namespace ofec_demo {
	/* Buffer of Dynamic Continuous Optimization Evolutionary Algorithm */
	class BufferDynamicConOEA : public BufferConOEA {
	private:
		struct BufferDatum {
			vtkSmartPointer<vtkChartXY> dynamic_error;
		};
		std::map<int, std::unique_ptr<BufferDatum>> m_buffer_data;

		std::unique_ptr<ChartView2d> m_offline_error_view_2d;			/* 2D view for offline error */

		BufferDatum *m_cur_datum;

		std::vector<ofec::Real> m_offline_errors, m_best_before_change_errors, m_numeffective;
		double m_color_offline_error[3] = { 1,0,0 };
		double m_color_bbc_error[3] = { 0,0,0 };

	public:
		BufferDynamicConOEA(GraphicsArea *graphics_area, int id_alg, BufferPro *buffer_pro);
		virtual ~BufferDynamicConOEA();
		void initializeGraphicViews() override;
		bool graphicViewsInitialized() override;
		void appendBuffer(int max_buffer_id) override;
		void popupBuffer(int buffer_id) override;
		void updateViews() override;
		void updateGraphicViews(int buffer_id) override;
	private:
		void updateDynamicError(BufferDatum* buffer_datum, ofec::Real offline_error, ofec::Real bbc_error);
		void removeDynamicError();
		void addNewGraphicOfflineError(int buffer_id);
	};
}

#endif // !OFEC_DEMO_BUFFER_DYNAMIC_COEA_H
