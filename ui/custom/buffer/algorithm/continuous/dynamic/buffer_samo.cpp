﻿#include <instance/algorithm/dynamic/samo/samo.h>
#include "buffer_samo.h"
#include "../../../../../core/buffer/geometrics/chart_2d.h"
#include <vtkAxis.h>

namespace ofec_demo {
	BufferSAMO::BufferSAMO(GraphicsArea *graphics_area, int id_alg, BufferPro* buffer_pro) :
		BufferDynamicConOEA(graphics_area, id_alg, buffer_pro),
		m_cur_datum(nullptr) {}

	BufferSAMO::~BufferSAMO() {
		m_graphics_area->removeGraphic("Number of populations");
	}

	void BufferSAMO::initializeGraphicViews() {
		BufferDynamicConOEA::initializeGraphicViews();
		m_popsize_view_2d.reset(new ChartView2d);
		m_graphics_area->addGraphic("Number of populations", m_popsize_view_2d.get());
	}

	bool BufferSAMO::graphicViewsInitialized() {
		return BufferDynamicConOEA::graphicViewsInitialized() && m_popsize_view_2d;
	}

	void BufferSAMO::appendBuffer(int max_buffer_id) {
		using namespace ofec;
		if (g_term_alg)	return;
		BufferDynamicConOEA::appendBuffer(max_buffer_id);
		auto pop_info = m_buffer_pop_info.at(max_buffer_id + 1).get();
		auto graphics = new BufferDatum;
		auto mfree = dynamic_cast<const SAMO&>(GET_ALG(m_id_alg)).getMfree();
		updatePopsize(pop_info, graphics, mfree);
		m_buffer_data[max_buffer_id + 1].reset(graphics);
	}

	void BufferSAMO::popupBuffer(int buffer_id) {
		m_buffer_data.erase(buffer_id);
		BufferDynamicConOEA::popupBuffer(buffer_id);
	}

	void BufferSAMO::updateGraphicViews(int buffer_id) {
		if (m_cur_datum) {
			removePopsize();
			m_cur_datum = nullptr;
		}
		if (m_buffer_data.count(buffer_id) > 0) {
			addNewGraphicPopsize(buffer_id);
			m_cur_datum = m_buffer_data.at(buffer_id).get();
		}
		BufferDynamicConOEA::updateGraphicViews(buffer_id);
	}

	void BufferSAMO::updateViews() {
		BufferDynamicConOEA::updateViews();
		m_popsize_view_2d->updateView();
	}

	void BufferSAMO::updatePopsize(PopInfo* pop_info, BufferDatum*buffer_datum,int m_free ) {
		if (g_term_alg)	return;
		using namespace ofec;
		int num_pops = pop_info->num_pops;
		m_numpops.push_back(num_pops);
		m_freepops.push_back(m_free);
		m_convpops.push_back(num_pops - m_free);

		m_iters.push_back(m_numpops.size());
		buffer_datum->m_popsize = vtkChartXY::New();
		if (m_numpops.empty())
			return;
		else if (m_numpops.size() < 5) {
			//Chart2d::addScatter(buffer_datum->m_popsize, m_iters, m_numpops, "", m_color_error);
			Chart2d::addScatter(buffer_datum->m_popsize, m_iters, m_freepops, "free pop", m_color_free);
			Chart2d::addScatter(buffer_datum->m_popsize, m_iters, m_convpops, "conv pop", m_color_cov);
		}
		else {
			//Chart2d::addLine(buffer_datum->m_popsize, m_iters, m_numpops, "", m_color_error);
			Chart2d::addLine(buffer_datum->m_popsize, m_iters, m_freepops, "free pop", m_color_free);
			Chart2d::addLine(buffer_datum->m_popsize, m_iters, m_convpops, "conv pop", m_color_cov);
		}
		buffer_datum->m_popsize->GetAxis(vtkAxis::LEFT)->SetTitle("Number of populations");
		buffer_datum->m_popsize->GetAxis(vtkAxis::LEFT)->SetGridVisible(false);
		buffer_datum->m_popsize->GetAxis(vtkAxis::BOTTOM)->SetTitle("Iteration");
		buffer_datum->m_popsize->GetAxis(vtkAxis::BOTTOM)->SetGridVisible(false);
		buffer_datum->m_popsize->SetShowLegend(true);
	}
	
	void BufferSAMO::addNewGraphicPopsize(int buffer_id) {
		auto& new_graphics = m_buffer_data.at(buffer_id);
		m_popsize_view_2d->scene()->AddItem(new_graphics->m_popsize);
	}
	
	void BufferSAMO::removePopsize() {
		m_popsize_view_2d->scene()->RemoveItem(m_cur_datum->m_popsize);
	}
}