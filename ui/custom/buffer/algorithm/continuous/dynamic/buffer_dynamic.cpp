#include <instance/algorithm/dynamic/metrics_dynamic.h>
#include "buffer_dynamic.h"
#include "../../../../../core/buffer/geometrics/chart_2d.h"
#include <vtkAxis.h>

namespace ofec_demo {
	BufferDynamicConOEA::BufferDynamicConOEA(GraphicsArea *graphics_area, int id_alg, BufferPro* buffer_pro) :
		BufferConOEA(graphics_area, id_alg, buffer_pro),
		m_cur_datum(nullptr) {}

	BufferDynamicConOEA::~BufferDynamicConOEA() {
		m_graphics_area->removeGraphic("Offline Error");
	}

	void BufferDynamicConOEA::initializeGraphicViews() {
		BufferConOEA::initializeGraphicViews();
		m_offline_error_view_2d.reset(new ChartView2d);
		m_graphics_area->addGraphic("Offline Error", m_offline_error_view_2d.get());
	}

	bool BufferDynamicConOEA::graphicViewsInitialized() {
		return BufferConOEA::graphicViewsInitialized() && m_offline_error_view_2d;
	}

	void BufferDynamicConOEA::appendBuffer(int max_buffer_id) {
		using namespace ofec;
		if (g_term_alg)	return;
		BufferConOEA::appendBuffer(max_buffer_id);
		auto graphics = new BufferDatum;
		auto offline_error = dynamic_cast<const MetricsDynamicConOEA&>(GET_ALG(m_id_alg)).getOfflineError();
		auto bbc_error = dynamic_cast<const MetricsDynamicConOEA&>(GET_ALG(m_id_alg)).getBBCError();
		updateDynamicError(graphics, offline_error, bbc_error);
		m_buffer_data[max_buffer_id + 1].reset(graphics);
	}

	void BufferDynamicConOEA::popupBuffer(int buffer_id) {
		m_buffer_data.erase(buffer_id);
		BufferConOEA::popupBuffer(buffer_id);
	}

	void BufferDynamicConOEA::updateGraphicViews(int buffer_id) {
		if (m_cur_datum) {
			removeDynamicError();
			m_cur_datum = nullptr;
		}
		if (m_buffer_data.count(buffer_id) > 0) {
			addNewGraphicOfflineError(buffer_id);
			m_cur_datum = m_buffer_data.at(buffer_id).get();
		}
		BufferConOEA::updateGraphicViews(buffer_id);
	}

	void BufferDynamicConOEA::updateDynamicError(BufferDatum* buffer_datum, ofec::Real offline_error, ofec::Real bbc_error) {
		using namespace ofec;
		m_offline_errors.push_back(offline_error);
		m_best_before_change_errors.push_back(bbc_error);
		m_numeffective.push_back(GET_ALG(m_id_alg).numEffectiveEvals());
		buffer_datum->dynamic_error = vtkChartXY::New();
		if (m_offline_errors.empty())
			return;
		else if (m_offline_errors.size() < 5) {
			Chart2d::addScatter(buffer_datum->dynamic_error, m_numeffective, m_offline_errors, "Offline Error", m_color_offline_error);
			Chart2d::addScatter(buffer_datum->dynamic_error, m_numeffective, m_best_before_change_errors, "Best Before Change Error", m_color_bbc_error);
		}
		else {
			Chart2d::addLine(buffer_datum->dynamic_error, m_numeffective, m_offline_errors, "Offline Error", m_color_offline_error);
			Chart2d::addLine(buffer_datum->dynamic_error, m_numeffective, m_best_before_change_errors, "Best Before Change Error", m_color_bbc_error);
		}
		buffer_datum->dynamic_error->GetAxis(vtkAxis::LEFT)->SetTitle("Error");
		buffer_datum->dynamic_error->GetAxis(vtkAxis::LEFT)->SetGridVisible(true);
		buffer_datum->dynamic_error->GetAxis(vtkAxis::LEFT)->SetLogScale(true);
		buffer_datum->dynamic_error->GetAxis(vtkAxis::BOTTOM)->SetTitle("Evaluations");
		buffer_datum->dynamic_error->GetAxis(vtkAxis::BOTTOM)->SetGridVisible(true);
	}

	void BufferDynamicConOEA::updateViews() {
		BufferConOEA::updateViews();
		m_offline_error_view_2d->updateView();
	}

	void BufferDynamicConOEA::addNewGraphicOfflineError(int buffer_id) {
		auto& new_graphics = m_buffer_data.at(buffer_id);
		m_offline_error_view_2d->scene()->AddItem(new_graphics->dynamic_error);
		m_offline_error_view_2d->updateView();
	}

	void BufferDynamicConOEA::removeDynamicError() {
		m_offline_error_view_2d->scene()->RemoveItem(m_cur_datum->dynamic_error);
	}
}