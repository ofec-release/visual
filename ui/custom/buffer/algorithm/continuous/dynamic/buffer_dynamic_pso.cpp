﻿#include <instance/algorithm/dynamic/dynamic_pso.h>
#include "buffer_dynamic_pso.h"
#include "../../../problem/continuous/buffer_conop.h"

namespace ofec_demo {
	void PopVarTextSpace2d::updateMultiPopsTextRect(
		const std::vector<std::unique_ptr<ofec::Individual<>>>& mp,
		const std::vector<int>& pi, int np,
		std::vector<bool> state,
		const std::vector<std::pair<ofec::Real, ofec::Real>>& range,
		ofec::Real x, ofec::Real y) 
	{
		std::vector<std::vector<ofec::Individual<>>> pop(np);
		for (size_t i = 0; i < np; ++i) {
			for (size_t j = 0; j < mp.size(); ++j)
				if (pi[j] == i) {
					pop[i].push_back(*mp[j]);
				}
		}
		for (size_t i = 0; i < np; ++i) {
			auto& c = g_rand_colors[i % g_num_rand_colors];
			ofec::Real rectx_min = pop[i][0].variable()[0];
			ofec::Real recty_min = pop[i][0].variable()[1];
			ofec::Real rectx_max = pop[i][0].variable()[0];
			ofec::Real recty_max = pop[i][0].variable()[1];
			for (size_t j = 1; j < pop[i].size(); ++j) {
				if (pop[i][j].variable()[0] > rectx_max) rectx_max = pop[i][j].variable()[0];
				if (pop[i][j].variable()[1] > recty_max) recty_max = pop[i][j].variable()[1];
				if (pop[i][j].variable()[0] < rectx_min) rectx_min = pop[i][j].variable()[0];
				if (pop[i][j].variable()[1] < recty_min) recty_min = pop[i][j].variable()[1];
			}
			rectx_min = ((rectx_min - range[0].first) / (range[0].second - range[0].first) * 2 - 1) * VarSpace2d::ms_lenth;
			recty_min = (1 - (recty_min - range[1].first) / (range[1].second - range[1].first) * 2) * VarSpace2d::ms_lenth;
			rectx_max = ((rectx_max - range[0].first) / (range[0].second - range[0].first) * 2 - 1) * VarSpace2d::ms_lenth;
			recty_max = (1 - (recty_max - range[1].first) / (range[1].second - range[1].first) * 2) * VarSpace2d::ms_lenth;
			m_multi_pop_rect.emplace_back(new Rect2d(rectx_min, recty_min, rectx_max - rectx_min, recty_max - recty_min, Qt::transparent, Qt::SolidLine, QColor(c[0] * 255, c[1] * 255, c[2] * 255)));
		}
		
		if (!state.size()) return;
		for (size_t i = 0; i < np; ++i) {
			auto& c = g_rand_colors[i % g_num_rand_colors];
			QString text;
			if (state[i] == false) {
				text = "Population " + QString::number(i) + ": Normal";
			}
			else if (state[i] == true) {
				text = "Population " + QString::number(i) + ": Hibernated";
			}

			QFont font("Times", 10, QFont::Bold);
			m_multi_pop_text.emplace_back(new Text2d(x, y + i * 20, 200, 50, text, font, QColor(c[0] * 255, c[1] * 255, c[2] * 255)));
		}
	}

	void PopVarTextSpace2d::updateMultiPopsGbestLocationCircle(std::vector<ofec::Solution<>> gbest,
		const std::vector<std::pair<ofec::Real, ofec::Real>>& range){
		qreal circle_r = 10;
		for (size_t i(0); i < gbest.size(); i++) {
			qreal x = ((gbest[i].variable()[0] - range[0].first) / (range[0].second - range[0].first) * 2 - 1) * VarSpace2d::ms_lenth - circle_r/2;
			qreal y = (1 - (gbest[i].variable()[1] - range[1].first) / (range[1].second - range[1].first) * 2) * VarSpace2d::ms_lenth - circle_r/2;
			m_multi_pop_cirle.emplace_back(new Circle2d(x, y, circle_r,Qt::transparent ,Qt::SolidLine ,Qt::red));
		}
	}

	void PopVarTextSpace2d::addMultiPopsText(QGraphicsScene* scene) {
		//for (auto& pop_text : m_multi_pop_text)
		//	scene->addItem(pop_text.get());
		for (auto& pop_rect : m_multi_pop_rect)
			scene->addItem(pop_rect.get());
		for (auto& pop_circ : m_multi_pop_cirle)
			scene->addItem(pop_circ.get());
	}

	void PopVarTextSpace2d::removeMultiPopsText(QGraphicsScene* scene) {
		//for (auto& pop_text : m_multi_pop_text)
		//	scene->removeItem(pop_text.get());
		for (auto& pop_rect : m_multi_pop_rect)
			scene->removeItem(pop_rect.get());
		for (auto& pop_circ : m_multi_pop_cirle)
			scene->removeItem(pop_circ.get());
	}

	void PopVarTextSpace2d::setMultiPopsTextVisible(bool flag) {
		//for (auto& pop_text : m_multi_pop_text)
		//	pop_text->setVisible(false);
		for (auto& pop_rect : m_multi_pop_rect)
			pop_rect->setVisible(flag);
		for (auto& pop_circ : m_multi_pop_cirle)
			pop_circ->setVisible(flag);
	}



	BufferDynamicPSO::BufferDynamicPSO(GraphicsArea *graphics_area, int id_alg, BufferPro* buffer_pro) :
		BufferDynamicConOEA(graphics_area, id_alg, buffer_pro),
		m_cur_datum(nullptr) {}

	BufferDynamicPSO::~BufferDynamicPSO() {
	}

	void BufferDynamicPSO::initializeGraphicViews(){
		BufferDynamicConOEA::initializeGraphicViews();
	}

	bool BufferDynamicPSO::graphicViewsInitialized(){
		return BufferDynamicConOEA::graphicViewsInitialized();
	}

	void BufferDynamicPSO::appendBuffer(int max_buffer_id){
		using namespace ofec;
		if (g_term_alg)	return;
		BufferDynamicConOEA::appendBuffer(max_buffer_id);

		if (GET_CONOP(m_buffer_pro->idPro()).numVariables() == 2) {
			auto pop_info = m_buffer_pop_info.at(max_buffer_id + 1).get();
			auto graphics = new BufferDatum;
			graphics->id_buffer = max_buffer_id + 1;
			auto hiber_state = dynamic_cast<const DynamicPSO&>(GET_ALG(m_id_alg)).getPopHiberState();
			auto gbests = dynamic_cast<const DynamicPSO &>(GET_ALG(m_id_alg)).getGbestLocation();
			auto scene = BUFFER_CONOP->varSpace2dScene();
			updatePopText(graphics, hiber_state, gbests, pop_info);
			m_buffer_data[max_buffer_id + 1].reset(graphics);
		}
	}

	void BufferDynamicPSO::popupBuffer(int buffer_id){
		m_buffer_data.erase(buffer_id);
		BufferDynamicConOEA::popupBuffer(buffer_id);
	}

	void BufferDynamicPSO::updateGraphicViews(int buffer_id){
		if (m_cur_datum) {
			removeCurDatum();
			m_cur_datum = nullptr;
		}
		if (m_buffer_data.count(buffer_id) > 0) {
			addNewDatum(buffer_id);
			m_cur_datum = m_buffer_data.at(buffer_id).get();
		}
		BufferDynamicConOEA::updateGraphicViews(buffer_id);
	}

	void BufferDynamicPSO::updateViews(){
		BufferDynamicConOEA::updateViews();
	}

	void BufferDynamicPSO::removeCurDatum(){
		using namespace ofec;
		auto scene = BUFFER_CONOP->varSpace2dScene();
		if (GET_CONOP(m_buffer_pro->idPro()).numVariables() == 2) {
			m_cur_datum->pop_var_text_space_2d->removeMultiPopsText(scene);
		}
		m_cur_datum = nullptr;
	}

	void BufferDynamicPSO::addNewDatum(int buffer_id){
		using namespace ofec;
		auto& new_graphics = m_buffer_data.at(buffer_id);
		auto scene = BUFFER_CONOP->varSpace2dScene();
		if (GET_CONOP(m_buffer_pro->idPro()).numVariables() == 2) {
			new_graphics->pop_var_text_space_2d->addMultiPopsText(scene);
		}
		m_cur_datum = new_graphics.get();
	}

	void BufferDynamicPSO::updatePopText(BufferDatum* buffer_datum, std::vector<bool>& state, std::vector<ofec::Solution<>> gbests, PopInfo* pop_info){
		if (g_term_alg)	return;
		using namespace ofec;
		auto scene = BUFFER_CONOP->varSpace2dScene();
		const auto& range_var = BUFFER_CONOP->rangeVar(g_buffer->idBufferAlg2Pro(buffer_datum->id_buffer));
		buffer_datum->pop_var_text_space_2d.reset(new PopVarTextSpace2d);
		buffer_datum->pop_var_text_space_2d->updateMultiPopsTextRect(pop_info->inds,pop_info->popIDs_of_inds,pop_info->num_pops,state, range_var,VarSpace2d::ms_lenth, -VarSpace2d::ms_lenth);
		buffer_datum->pop_var_text_space_2d->updateMultiPopsGbestLocationCircle(gbests, range_var);
	}
}