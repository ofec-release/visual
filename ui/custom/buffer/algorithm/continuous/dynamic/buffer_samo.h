//AddCustom BufferSAMO "^SAMO$" ConOP,DOP

#ifndef OFEC_DEMO_BUFFER_SAMO_H
#define OFEC_DEMO_BUFFER_SAMO_H

#include "buffer_dynamic.h"
#include <vtkChartXY.h>
#include "../../../../../core/buffer/graphic_view/chart_view_2d.h"

namespace ofec_demo {
	/* Buffer of SaDE */
	class BufferSAMO : public BufferDynamicConOEA {
	private:
		std::unique_ptr<ChartView2d> m_popsize_view_2d;

		struct BufferDatum {
			vtkSmartPointer<vtkChartXY> m_popsize;
		};
		std::map<int, std::unique_ptr<BufferDatum>> m_buffer_data;
		BufferDatum* m_cur_datum;

		std::vector<ofec::Real> m_numpops, m_freepops, m_convpops, m_iters;
		double m_color_free[3] = { 1,0,0 };
		double m_color_cov[3] = { 0,0,1 };

	public:
		BufferSAMO(GraphicsArea* graphics_area, int id_alg, BufferPro* buffer_pro);
		virtual ~BufferSAMO();
		void initializeGraphicViews() override;
		bool graphicViewsInitialized() override;
		void appendBuffer(int max_buffer_id) override;
		void popupBuffer(int buffer_id) override;
		void updateGraphicViews(int buffer_id) override;
		void updateViews() override;

	private:
		void updatePopsize(PopInfo* pop_info, BufferDatum* buffer_datum, int m_free);
		void addNewGraphicPopsize(int buffer_id);
		void removePopsize();
	};
}

#endif // !OFEC_DEMO_BUFFER_SAMO_H