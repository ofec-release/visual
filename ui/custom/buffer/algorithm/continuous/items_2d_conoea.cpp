#include "items_2d_conoea.h"
#include <QGraphicsScene>
#include "../../../../core/global_ui.h"
#include "../../problem/continuous/items_2d_conop.h"

namespace ofec_demo {
	void PopFitLndscp2d::updateMultiPops(
		const std::vector<std::unique_ptr<ofec::Individual<>>> &mp, 
		const std::vector<int> &pi, int np, 
		const std::vector<std::pair<ofec::Real, ofec::Real>> &range,
		const std::pair<ofec::Real, ofec::Real> &range_fit)
	{
		QPointF point;
		std::vector<std::vector<QPointF>> pops_points(np);
		for (size_t i = 0; i < mp.size(); ++i) {
			//pts[i].setX(((samples[i]->variable()[0] - boundary[0].first) / (boundary[0].second - boundary[0].first) * 2 - 1) * ms_lenth);
			//pts[i].setY((1 - (samples[i]->fit() - range_fit.first) / (range_fit.second - range_fit.first) * 2) * ms_lenth / 2);


			point.setX(((mp[i]->variable()[0] - range[0].first) / (range[0].second - range[0].first) * 2 - 1) * FitLndscp2d::ms_lenth);
			point.setY((1 - (mp[i]->fitness() - range_fit.first) / (range_fit.second - range_fit.first) * 2) * FitLndscp2d::ms_lenth / 2);
			pops_points[pi[i]].push_back(point);
		}
		for (size_t i = 0; i < np; ++i) {
			auto &c = g_rand_colors[i % g_num_rand_colors];
			m_multi_pop.emplace_back(new Points2d(pops_points[i], 4, QColor(c[0] * 255, c[1] * 255, c[2] * 255)));
		}
	}

	void PopFitLndscp2d::addMultiPops(QGraphicsScene *scene) {
		for (auto &pop : m_multi_pop)
			scene->addItem(pop.get());
	}
	
	void PopFitLndscp2d::removeMultiPops(QGraphicsScene *scene) {
		for (auto &pop : m_multi_pop)
			scene->removeItem(pop.get());
	}
	
	void PopFitLndscp2d::setMultiPopsVisible(bool flag) {
		for (auto &pop : m_multi_pop)
			pop->setVisible(flag);
	}

	void PopVarSpace2d::updateMultiPops(
		const std::vector<std::unique_ptr<ofec::Individual<>>> &mp, 
		const std::vector<int> &pi, int np, 
		const std::vector<std::pair<ofec::Real, ofec::Real>> &range)
	{
		QPointF point;
		std::vector<std::vector<QPointF>> pops_points(np);
		for (size_t i = 0; i < mp.size(); ++i) {
			point.setX(((mp[i]->variable()[0] - range[0].first) / (range[0].second - range[0].first) * 2 - 1) * VarSpace2d::ms_lenth);
			point.setY((1 - (mp[i]->variable()[1] - range[1].first) / (range[1].second - range[1].first) * 2) * VarSpace2d::ms_lenth);
			pops_points[pi[i]].push_back(point);
		}
		for (size_t i = 0; i < np; ++i) {
			auto &c = g_rand_colors[i % g_num_rand_colors];
			m_multi_pop.emplace_back(new Points2d(pops_points[i], 4, QColor(c[0] * 255, c[1] * 255, c[2] * 255)));
		}
	}

	void PopVarSpace2d::addMultiPops(QGraphicsScene *scene) {
		for (auto &pop : m_multi_pop)
			scene->addItem(pop.get());
	}

	void PopVarSpace2d::removeMultiPops(QGraphicsScene *scene) {
		for (auto &pop : m_multi_pop)
			scene->removeItem(pop.get());
	}
	
	void PopVarSpace2d::setMultiPopsVisible(bool flag) {
		for (auto &pop : m_multi_pop)
			pop->setVisible(flag);
	}
}