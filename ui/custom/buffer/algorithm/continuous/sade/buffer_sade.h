//AddCustom BufferSaDE "^SaDE$" ConOP

#ifndef OFEC_DEMO_BUFFER_SADE_H
#define OFEC_DEMO_BUFFER_SADE_H

#include "../buffer_conoea.h"
#include <vtkChartXY.h>
#include "../../../../../core/buffer/graphic_view/chart_view_2d.h"

namespace ofec_demo {
	/* Buffer of SaDE */
	class BufferSaDE : public BufferConOEA {
	private:
		std::unique_ptr<ChartView2d> m_graphic_ratio_strategy;

		struct BufferDatum {
			vtkSmartPointer<vtkChartXY> ratio_strategy;
		};
		std::map<int, std::unique_ptr<BufferDatum>> m_buffer_data;
		BufferDatum* m_cur_datum;

	public:
		BufferSaDE(GraphicsArea* graphic_dock_set, int id_alg, BufferPro* buffer_pro);
		virtual ~BufferSaDE();
		void initializeGraphicViews() override;
		bool graphicViewsInitialized() override;
		void appendBuffer(int max_buffer_id) override;
		void popupBuffer(int buffer_id) override;
		void updateGraphicViews(int buffer_id) override;
		void updateViews() override;

	private:
		void updateBufferRatioStrategy(BufferDatum* buffer_datum);
		void addNewGraphicRatioStrategy(int buffer_id);
		void removeCurGraphicRatioStrategy();
	};
}

#endif // !OFEC_DEMO_BUFFER_SADE_H