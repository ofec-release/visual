﻿#include "buffer_sade.h"
#include "../../../../../core/buffer/geometrics/chart_2d.h"
#include <instance/algorithm/global/sade/sade.h>
#include <vtkAxis.h>

namespace ofec_demo {
	BufferSaDE::BufferSaDE(GraphicsArea* graphic_dock_set, int id_alg, BufferPro* buffer_pro) :
		BufferConOEA(graphic_dock_set, id_alg, buffer_pro),
		m_cur_datum(nullptr) {}

	BufferSaDE::~BufferSaDE() {
		m_graphics_area->removeGraphic("Ratio of strategies");
		m_graphics_area->tileSubWindows();
	}

	void BufferSaDE::initializeGraphicViews() {
		BufferConOEA::initializeGraphicViews();
		m_graphic_ratio_strategy.reset(new ChartView2d);
		m_graphics_area->addGraphic("Ratio of strategies", m_graphic_ratio_strategy.get());
		m_graphics_area->tileSubWindows();
	}

	bool BufferSaDE::graphicViewsInitialized() {
		return BufferConOEA::graphicViewsInitialized() && m_graphic_ratio_strategy;
	}

	void BufferSaDE::appendBuffer(int max_buffer_id) {
		if (g_term_alg)	return;
		BufferConOEA::appendBuffer(max_buffer_id);
		auto graphics = new BufferDatum;
		updateBufferRatioStrategy(graphics);
		m_buffer_data[max_buffer_id + 1].reset(graphics);
	}

	void BufferSaDE::popupBuffer(int buffer_id) {
		m_buffer_data.erase(buffer_id);
		BufferConOEA::popupBuffer(buffer_id);
	}

	void BufferSaDE::updateGraphicViews(int buffer_id) {
		if (m_cur_datum) {
			removeCurGraphicRatioStrategy();
			m_cur_datum = nullptr;
		}
		if (m_buffer_data.count(buffer_id) > 0) {
			addNewGraphicRatioStrategy(buffer_id);
			m_cur_datum = m_buffer_data.at(buffer_id).get();
		}
		BufferConOEA::updateGraphicViews(buffer_id);
	}

	void BufferSaDE::updateViews() {
		BufferConOEA::updateViews();
		m_graphic_ratio_strategy->updateView();
	}

	void BufferSaDE::updateBufferRatioStrategy(BufferDatum*buffer_datum) {
		if (g_term_alg)	return;
		using namespace ofec;
		auto &cml_ratio = dynamic_cast<SaDE&>(GET_ALG(m_id_alg)).ratioStrategy();
		auto ratio = cml_ratio;
		for (size_t i = 1; i < cml_ratio.size(); i++)
			ratio[i] = cml_ratio[i] - cml_ratio[i - 1];
		std::vector<std::string> labels = { 
			"DE/rand/1/bin",
			"DE/rand-to-best/2/bin",
			"DE/rand/2/bin",
			"DE/current-to-rand/1" };
		buffer_datum->ratio_strategy = vtkSmartPointer<vtkChartXY>::New();
		Chart2d::addBars(buffer_datum->ratio_strategy, ratio, labels);
		buffer_datum->ratio_strategy->GetAxis(vtkAxis::LEFT)->SetTitle("Ratio");
	}
	
	void BufferSaDE::addNewGraphicRatioStrategy(int buffer_id) {
		auto& new_graphics = m_buffer_data.at(buffer_id);
		m_graphic_ratio_strategy->scene()->AddItem(new_graphics->ratio_strategy);
	}
	
	void BufferSaDE::removeCurGraphicRatioStrategy() {
		m_graphic_ratio_strategy->scene()->RemoveItem(m_cur_datum->ratio_strategy);
	}
}