#include "buffer_conoea.h"
#include "../../problem/continuous/buffer_conop.h"
#include "../../../../core/parameter/visual_alg.h"
#include "../../../../core/buffer/geometrics/chart_2d.h"
#include <vtkAxis.h>

namespace ofec_demo {
	BufferConOEA::BufferConOEA(GraphicsArea *graphic_dock_set, int id_alg, BufferPro *buffer_pro) :
		BufferEA(graphic_dock_set, id_alg, buffer_pro),
		m_cur_datum(nullptr) {}

	BufferConOEA::~BufferConOEA() {
		if (m_chart_error_view_2d) {
			m_graphics_area->removeGraphic("Error Chart");
		}
	}

	void BufferConOEA::initializeGraphicViews() {
		BufferEA::initializeGraphicViews();
		using namespace ofec;
		if (GET_CONOP(m_buffer_pro->idPro()).isOptimaObjGiven() && GET_CONOP(m_buffer_pro->idPro()).numObjectives() == 1) {
			m_chart_error_view_2d.reset(new ChartView2d);
			m_graphics_area->addGraphic("Error Chart", m_chart_error_view_2d.get());
		}
	}

	bool BufferConOEA::graphicViewsInitialized() {
		using namespace ofec;
		bool flag = true;
		if (GET_CONOP(m_buffer_pro->idPro()).isOptimaObjGiven() && GET_CONOP(m_buffer_pro->idPro()).numObjectives() == 1)
			flag = flag && m_chart_error_view_2d;
		return BufferEA::graphicViewsInitialized() && flag;
	}

	void BufferConOEA::appendBuffer(int max_buffer_id) {
		BufferEA::appendBuffer(max_buffer_id);
		using namespace ofec;
		auto pop_info = m_buffer_pop_info.at(max_buffer_id + 1).get();
		auto buffer_datum = new BufferDatum;
		buffer_datum->id_buffer = max_buffer_id + 1;
		updatePopFitness(pop_info);
		if (GET_CONOP(m_buffer_pro->idPro()).numVariables() == 1)
			updatePopFitLndscp2d(pop_info, buffer_datum);
		else if (GET_CONOP(m_buffer_pro->idPro()).numVariables() == 2) {
			updatePopFitLndscp3d(pop_info, buffer_datum);
			updatePopVarSpace2d(pop_info, buffer_datum);
		}
		else if (GET_CONOP(m_buffer_pro->idPro()).numVariables() == 3) 
			updatePopVarSpace3d(pop_info, buffer_datum);
		if (GET_PRO(m_buffer_pro->idPro()).isOptimaVarGiven()
			&& GET_ALG(m_id_alg).keepCandidatesUpdated()
			&& !GET_PRO(m_buffer_pro->idPro()).hasTag(ProTag::kMOP))
			updateOptimaFound(buffer_datum);
		if (GET_CONOP(m_buffer_pro->idPro()).isOptimaObjGiven() && GET_CONOP(m_buffer_pro->idPro()).numObjectives() == 1)
			updateChartError(pop_info, buffer_datum);
		m_buffer_data[max_buffer_id + 1].reset(buffer_datum);
	}

	void BufferConOEA::popupBuffer(int buffer_id) {
		BufferEA::popupBuffer(buffer_id);
		m_buffer_data.erase(buffer_id);
	}

	void BufferConOEA::updateViews() {
		using namespace ofec;
		BufferEA::updateViews();
		if (GET_CONOP(m_buffer_pro->idPro()).isOptimaObjGiven() && GET_CONOP(m_buffer_pro->idPro()).numObjectives() == 1)
			m_chart_error_view_2d->updateView();
	}

	void BufferConOEA::updateGraphicViews(int buffer_id) {
		BufferEA::updateGraphicViews(buffer_id);
		if (m_cur_datum)
			removeCurDatum();
		if (m_buffer_data.count(buffer_id) > 0)
			addNewDatum(buffer_id);
	}
	
	void BufferConOEA::setSolutionVisible(bool flag) {
		BufferEA::setSolutionVisible(flag);
		using namespace ofec;
		if (m_cur_datum && GET_CONOP(m_buffer_pro->idPro()).numVariables() == 1)
			m_cur_datum->pop_fit_lndscp_2d->setMultiPopsVisible(flag);
		else if (m_cur_datum && GET_CONOP(m_buffer_pro->idPro()).numVariables() == 2) {
			m_cur_datum->pop_fit_lndscp_3d->setMultiPopsVisible(flag);
			m_cur_datum->pop_var_space_2d->setMultiPopsVisible(flag);
		}
	}

	void BufferConOEA::updatePopFitness(PopInfo *pop_info) {
		using namespace ofec;
		if (GET_PRO(m_buffer_pro->idPro()).numObjectives() == 1) {
			ofec::Real pos = (GET_PRO(m_buffer_pro->idPro()).optMode(0) == ofec::OptMode::kMaximize) ? 1 : -1;
			for (auto &ind : pop_info->inds)
				ind->setFitness(pos * ind->objective(0));
		}
		else {
			for (auto &ind : pop_info->inds)
				ind->setFitness(1);
		}
	}

	void BufferConOEA::updatePopFitLndscp3d(PopInfo *pop_info, BufferDatum* buffer_datum) {
		using namespace ofec;
		const auto &range_fit = BUFFER_CONOP->rangeFit();
		const auto &range_var = BUFFER_CONOP->rangeVar(g_buffer->idBufferAlg2Pro(buffer_datum->id_buffer));
		buffer_datum->pop_fit_lndscp_3d.reset(new PopFitLndscp3d);
		buffer_datum->pop_fit_lndscp_3d->updateMultiPops(pop_info->inds, pop_info->popIDs_of_inds, pop_info->num_pops, range_var, range_fit);
	}

	void BufferConOEA::updatePopFitLndscp2d(PopInfo *pop_info, BufferDatum *buffer_datum) {
		using namespace ofec;
		const auto &range_fit = BUFFER_CONOP->rangeFit();
		const auto &range_var = BUFFER_CONOP->rangeVar(g_buffer->idBufferAlg2Pro(buffer_datum->id_buffer));
		buffer_datum->pop_fit_lndscp_2d.reset(new PopFitLndscp2d);
		buffer_datum->pop_fit_lndscp_2d->updateMultiPops(pop_info->inds, pop_info->popIDs_of_inds, pop_info->num_pops, range_var, range_fit);
	}
	
	void BufferConOEA::updatePopVarSpace2d(PopInfo *pop_info, BufferDatum *buffer_datum) {
		using namespace ofec;
		const auto &range_var = BUFFER_CONOP->rangeVar(g_buffer->idBufferAlg2Pro(buffer_datum->id_buffer));
		buffer_datum->pop_var_space_2d.reset(new PopVarSpace2d);
		buffer_datum->pop_var_space_2d->updateMultiPops(pop_info->inds, pop_info->popIDs_of_inds, pop_info->num_pops, range_var);
	}

	void BufferConOEA::updatePopVarSpace3d(PopInfo *pop_info, BufferDatum *buffer_datum) {
		using namespace ofec;
		const auto &range_var = BUFFER_CONOP->rangeVar(g_buffer->idBufferAlg2Pro(buffer_datum->id_buffer));
		buffer_datum->pop_var_space_3d.reset(new PopVarSpace3d);
		buffer_datum->pop_var_space_3d->updateMultiPops(pop_info->inds, pop_info->popIDs_of_inds, pop_info->num_pops, range_var);
	}

	void BufferConOEA::updateChartError(PopInfo* pop_info, BufferDatum* buffer_datum){
		using namespace ofec;
		if (!pop_info->inds.empty()) {
			Real offline_error = 0.;
			Real global_obj = GET_CONOP(m_buffer_pro->idPro()).getOptima().objective(0)[0];
			size_t id_best = 0;
			Real cur_best_obj = pop_info->inds[id_best]->objective(0);
			for (size_t i = 1; i < pop_info->inds.size(); i++) {
				if (pop_info->inds[i]->dominate(*pop_info->inds[id_best], m_buffer_pro->idPro())) {
					id_best = i;
					cur_best_obj = pop_info->inds[id_best]->objective(0);
				}
			}
			m_errors.push_back(abs(global_obj - cur_best_obj));
			m_iters.push_back(m_errors.size());
		}
		buffer_datum->chart_error = vtkChartXY::New();
		if (m_errors.empty())
			return;
		else if(m_errors.size()<5)
			Chart2d::addScatter(buffer_datum->chart_error, m_iters, m_errors, "", m_color_error);
		else
			Chart2d::addLine(buffer_datum->chart_error, m_iters, m_errors, "", m_color_error);
		buffer_datum->chart_error->GetAxis(vtkAxis::LEFT)->SetTitle("Error");
		buffer_datum->chart_error->GetAxis(vtkAxis::LEFT)->SetLogScale(true);
		buffer_datum->chart_error->GetAxis(vtkAxis::LEFT)->SetGridVisible(false);
		buffer_datum->chart_error->GetAxis(vtkAxis::BOTTOM)->SetTitle("Iteration");
		buffer_datum->chart_error->GetAxis(vtkAxis::BOTTOM)->SetGridVisible(false);
	}

	void BufferConOEA::updateOptimaFound(BufferDatum *buffer_datum) {
		using namespace ofec;
		auto &optima = GET_CONOP(m_buffer_pro->idPro()).getOptima();
		auto &candidates = GET_ALG(m_id_alg).candidates();
		auto accuracy = GET_PRO(m_buffer_pro->idPro()).objectiveAccuracy();
		auto niche_radius = GET_PRO(m_buffer_pro->idPro()).variableNicheRadius();
		buffer_datum->is_opt_vars_found.assign(optima.numberVariables(), false);
		Real dis_obj, dis_var;
		for (size_t i = 0; i < optima.numberVariables(); i++) {
			for (auto &c : candidates) {
				dis_obj = c->objectiveDistance(optima.objective(i));
				dis_var = c->variableDistance(optima.variable(i), m_buffer_pro->idPro());
				if (dis_obj < accuracy && dis_var < niche_radius)
					buffer_datum->is_opt_vars_found[i] = true;
			}
		}
	}

	void BufferConOEA::removeCurDatum() {
		using namespace ofec;
		if (GET_CONOP(m_buffer_pro->idPro()).numVariables() == 1) {
			auto scene = BUFFER_CONOP->fitLndscp2dScene();
			m_cur_datum->pop_fit_lndscp_2d->removeMultiPops(scene);
		}
		else if (GET_CONOP(m_buffer_pro->idPro()).numVariables() == 2) {	
			auto renderer = BUFFER_CONOP->fitLndscp3dRenderer();
			m_cur_datum->pop_fit_lndscp_3d->removeMultiPops(renderer);
			auto scene = BUFFER_CONOP->varSpace2dScene();
			m_cur_datum->pop_var_space_2d->removeMultiPops(scene);
		}
		else if (GET_CONOP(m_buffer_pro->idPro()).numVariables() == 3) {
			auto renderer = BUFFER_CONOP->varSpace3dRenderer();
			m_cur_datum->pop_var_space_3d->removeMultiPops(renderer);
		}
		if (GET_CONOP(m_buffer_pro->idPro()).isOptimaObjGiven() && GET_CONOP(m_buffer_pro->idPro()).numObjectives() == 1)
			m_chart_error_view_2d->scene()->RemoveItem(m_cur_datum->chart_error);
		if (GET_PRO(m_buffer_pro->idPro()).isOptimaVarGiven()
			&& GET_ALG(m_id_alg).keepCandidatesUpdated()
			&& !GET_PRO(m_buffer_pro->idPro()).hasTag(ProTag::kMOP))
			BUFFER_CONOP->setOptVarsFound(g_buffer->idBufferAlg2Pro(m_cur_datum->id_buffer), false);
		m_cur_datum = nullptr;
	}

	void BufferConOEA::addNewDatum(int buffer_id) {
		using namespace ofec;
		auto& new_graph = m_buffer_data.at(buffer_id);
		if (GET_CONOP(m_buffer_pro->idPro()).numVariables() == 1) {
			auto scene = BUFFER_CONOP->fitLndscp2dScene();
			new_graph->pop_fit_lndscp_2d->addMultiPops(scene);
			new_graph->pop_fit_lndscp_2d->setMultiPopsVisible(VisualAlg::ms_show_solution);
		}
		else if (GET_CONOP(m_buffer_pro->idPro()).numVariables() == 2) {
			auto renderer = BUFFER_CONOP->fitLndscp3dRenderer();
			new_graph->pop_fit_lndscp_3d->addMultiPops(renderer);
			new_graph->pop_fit_lndscp_3d->setMultiPopsVisible(VisualAlg::ms_show_solution);

			auto scene = BUFFER_CONOP->varSpace2dScene();
			new_graph->pop_var_space_2d->addMultiPops(scene);
			new_graph->pop_var_space_2d->setMultiPopsVisible(VisualAlg::ms_show_solution);
		}
		else if (GET_CONOP(m_buffer_pro->idPro()).numVariables() == 3) {
			auto renderer = BUFFER_CONOP->varSpace3dRenderer();
			new_graph->pop_var_space_3d->addMultiPops(renderer);
			new_graph->pop_var_space_3d->setMultiPopsVisible(VisualAlg::ms_show_solution);
		}
		if (GET_CONOP(m_buffer_pro->idPro()).isOptimaObjGiven() && GET_CONOP(m_buffer_pro->idPro()).numObjectives() == 1) {
			m_chart_error_view_2d->scene()->AddItem(new_graph->chart_error);
			m_chart_error_view_2d->updateView();
		}

		if (GET_PRO(m_buffer_pro->idPro()).isOptimaVarGiven()
			&& GET_ALG(m_id_alg).keepCandidatesUpdated()
			&& !GET_PRO(m_buffer_pro->idPro()).hasTag(ProTag::kMOP))
			BUFFER_CONOP->setOptVarsFound(g_buffer->idBufferAlg2Pro(new_graph->id_buffer), new_graph->is_opt_vars_found);
		m_cur_datum = new_graph.get();
	}
}