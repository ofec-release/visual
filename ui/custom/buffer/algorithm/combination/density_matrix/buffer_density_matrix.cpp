#include "buffer_density_matrix.h"
#include <utility/functional.h>

namespace ofec_demo {
	int BufferDensityMatrix::ms_number = 2;
	std::vector<std::vector<std::vector<ofec::Real>>> BufferDensityMatrix::ms_density_matrix(ms_number);
	std::vector<std::pair<ofec::Real, ofec::Real>> BufferDensityMatrix::ms_density_range(ms_number);
	std::vector<QString> BufferDensityMatrix::ms_name = { "Individuals Distribution","Probabilisty Matrix" };

	BufferDensityMatrix::BufferDensityMatrix(GraphicsArea *graphics_area, int id_alg, BufferPro *buffer_pro)
		:BufferAlg(graphics_area, id_alg, buffer_pro),
		m_density_matrix_2d(ms_number),
		m_density_matrix_view_2d(ms_number),
		m_density_fill_color(ms_number),
		m_density_frame_color(ms_number)
	{
		m_lut = vtkLookupTable::New();
		m_lut->SetTableRange(0, 1);
		m_lut->SetNumberOfColors(m_max_num);
		m_lut->Build();
	}

	BufferDensityMatrix::~BufferDensityMatrix() {
		for (int idx(0); idx < ms_number; ++idx) {
			m_graphics_area->removeGraphic(ms_name[idx]);
			m_density_matrix_view_2d[idx]->scene()->removeItem(m_density_matrix_2d[idx].get());
		}
	}

	void BufferDensityMatrix::initializeGraphicViews() {
		for (int idx(0); idx < ms_number; ++idx) {
			m_density_matrix_view_2d[idx].reset(new GraphicView2d);
			m_graphics_area->addGraphic(ms_name[idx], m_density_matrix_view_2d[idx].get());
			m_density_matrix_2d[idx].reset(new Matrix2d);
			m_density_matrix_view_2d[idx]->scene()->addItem(m_density_matrix_2d[idx].get());
			m_density_matrix_view_2d[idx]->resetCamera();
		}
	}

	bool BufferDensityMatrix::graphicViewsInitialized() {
		bool flag = true;
		for (int idx(0); idx < ms_number; ++idx)
			flag = flag && m_density_matrix_view_2d[idx];
		return flag;
	}

	void BufferDensityMatrix::appendBuffer(int max_buffer_id) {
		double color[3] = { 0,0,0 };
		for (int idx(0); idx < ms_number; ++idx) {
			auto &mat(ms_density_matrix[idx]);
			auto &range(ms_density_range[idx]);
			auto &frame_color(m_density_frame_color[idx][max_buffer_id + 1]);
			auto &fill_color(m_density_fill_color[idx][max_buffer_id + 1]);
			frame_color.resize(mat.size());
			fill_color.resize(mat.size());
			for (int x(0); x < mat.size(); ++x) {
				frame_color[x].resize(mat[x].size());
				fill_color[x].resize(mat[x].size());
				for (int y(0); y < mat[x].size(); ++y) {
					m_lut->GetColor(ofec::mapReal<double>(mat[x][y], range.first, range.second, 0, 1), color);
					frame_color[x][y].setRgb(color[0] * 255., color[1] * 255., color[2] * 255.);
					fill_color[x][y].setRgb(color[0] * 255., color[1] * 255., color[2] * 255.);
				}
			}
		}
	}

	void ofec_demo::BufferDensityMatrix::popupBuffer(int buffer_id) {
		for (int idx(0); idx < ms_number; ++idx) {
			m_density_fill_color[idx].erase(buffer_id);
			m_density_frame_color[idx].erase(buffer_id);
		}
	}

	void ofec_demo::BufferDensityMatrix::updateGraphicViews(int buffer_id) {
		for (int idx(0); idx < ms_number; ++idx) {
			if (m_density_fill_color[idx].count(buffer_id) > 0 && m_density_frame_color[idx].count(buffer_id) > 0) {
				m_density_matrix_2d[idx]->setFillColor(m_density_fill_color[idx][buffer_id]);
				m_density_matrix_2d[idx]->setFrameColor(m_density_frame_color[idx][buffer_id]);
			}
		}
	}

	void ofec_demo::BufferDensityMatrix::updateViews() {}
}