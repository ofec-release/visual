//AddCustom BufferDensityMatrix "^(?:AS|ACS|MMAS|PACO)_TSP$" TSP

#ifndef OFEC_DEMO_BUFFER_DENSITY_MATRIX_H
#define OFEC_DEMO_BUFFER_DENSITY_MATRIX_H

#include <core/definition.h>
#include "../../../../../core/buffer/buffer_alg.h"
#include "../../../../../core/buffer/graphic_view/graphic_view_2d.h"

namespace ofec_demo {
	/* Buffer of Algorithm */
	class BufferDensityMatrix : public BufferAlg {
	protected:
		vtkSmartPointer<vtkLookupTable> m_lut;
		const int m_max_num = 2e5;

		std::vector<std::unique_ptr<GraphicView2d>> m_density_matrix_view_2d;
		std::vector<std::unique_ptr<Matrix2d>> m_density_matrix_2d;
		std::vector<std::map<int, std::vector<std::vector<QColor>>>> m_density_fill_color;
		std::vector<std::map<int, std::vector<std::vector<QColor>>>> m_density_frame_color;

	public:
		static std::vector<QString> ms_name;
		static std::vector<std::vector<std::vector<ofec::Real>>> ms_density_matrix;
		static std::vector<std::pair<ofec::Real, ofec::Real>> ms_density_range;
		static int ms_number;

	public:
		BufferDensityMatrix(GraphicsArea * graphics_area, int id_alg, BufferPro* buffer_pro);
		virtual ~BufferDensityMatrix();
		void initializeGraphicViews() override;
		bool graphicViewsInitialized() override;
		void appendBuffer(int max_buffer_id) override;
		void popupBuffer(int buffer_id) override;
		void updateGraphicViews(int buffer_id) override;
		void updateViews() override;
		void setSolutionVisible(bool flag) {}
	};
}

#endif // !OFEC_DEMO_BUFFER_ALG_H
