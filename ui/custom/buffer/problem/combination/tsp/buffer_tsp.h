//AddCustom BufferTSP "" TSP

#ifndef OFEC_DEMO_BUFFER_TSP_H
#define OFEC_DEMO_BUFFER_TSP_H

#include "../../../../../core/buffer/buffer_pro.h"
#include "items_2d_tsp.h"
#include "../../../../../core/buffer/graphic_view/graphic_view_2d.h"

namespace ofec_demo {
	class BufferTSP : public BufferPro {
	protected:
		std::unique_ptr<GraphicView2d> m_map_view_2d; /* 2D view of cities and ruotes */
		struct BufferDatum {
			std::unique_ptr<Map2dTSP> map_2d;
		};	
		std::map<int, std::unique_ptr<BufferDatum>> m_buffer_data;
		BufferDatum *m_cur_datum;

	public:
		BufferTSP(GraphicsArea *graphic_dock_set, int id_pro);
		virtual ~BufferTSP();
		void initializeGraphicViews() override;
		bool graphicViewsInitialized() override;
		void appendBuffer(int max_buffer_id) override;
		void popupBuffer(int buffer_id) override;
		void updateGraphicViews(int buffer_id) override;
		void updateViews()  override;
		void setOptimaVisible(bool flag) override;

		QGraphicsScene *mapScene() { return m_map_view_2d->scene(); }
	};
}

#endif // !OFEC_DEMO_BUFFER_TSP_H
