#include "buffer_tsp.h"
#include <instance/problem/combination/travelling_salesman/travelling_salesman.h>
#include <core/instance_manager.h>
#include "../../../../../core/global_ui.h"
#include "../../../../../core/parameter/visual_pro.h"

namespace ofec_demo {
	BufferTSP::BufferTSP(GraphicsArea *graphic_dock_set, int id_pro) :
		BufferPro(graphic_dock_set, id_pro),
		m_cur_datum(nullptr)	{}

	BufferTSP::~BufferTSP() {
		if (m_cur_datum)
			m_cur_datum->map_2d->removeMap(m_map_view_2d->scene());
		m_graphics_area->removeGraphic("Map of Cities");
	}

	void BufferTSP::initializeGraphicViews() {
		BufferPro::initializeGraphicViews();
		m_map_view_2d.reset(new GraphicView2d);
		m_graphics_area->addGraphic("Map of Cities", m_map_view_2d.get());
	}

	bool BufferTSP::graphicViewsInitialized() {
		return BufferPro::graphicViewsInitialized() && m_map_view_2d;
	}

	void BufferTSP::appendBuffer(int max_buffer_id) {
		using namespace ofec;
		if (g_term_alg) return;
		auto& cities = GET_TSP(m_id_pro).coordinate();
		auto graph = new BufferDatum;
		graph->map_2d.reset(new Map2dTSP);
		graph->map_2d->updateMap(cities);
		if (GET_TSP(m_id_pro).getOptima().isVariableGiven()) {
			auto &route = GET_TSP(m_id_pro).getOptima().variable(0).vect();
			graph->map_2d->updateOptima(cities, route);
		}
		m_buffer_data[max_buffer_id + 1].reset(graph);
		BufferPro::appendBuffer(max_buffer_id);
	}

	void BufferTSP::popupBuffer(int buffer_id) {
		m_buffer_data.erase(buffer_id);
		BufferPro::popupBuffer(buffer_id);
	}

	void BufferTSP::updateGraphicViews(int buffer_id) {
		using namespace ofec;
		if (m_cur_datum) {
			m_cur_datum->map_2d->removeMap(m_map_view_2d->scene());
			if (GET_TSP(m_id_pro).getOptima().isVariableGiven())
				m_cur_datum->map_2d->removeOptima(m_map_view_2d->scene());
			m_cur_datum = nullptr;
		}
		if (m_buffer_data.count(buffer_id) > 0) {
			auto &new_datum = m_buffer_data.at(buffer_id);
			new_datum->map_2d->addMap(m_map_view_2d->scene());
			if (GET_TSP(m_id_pro).getOptima().isVariableGiven()) {
				new_datum->map_2d->addOptima(m_map_view_2d->scene());
				new_datum->map_2d->setOptimaVisible(VisualPro::ms_show_optima);
			}
			m_map_view_2d->resetCamera();
			m_cur_datum = new_datum.get();
		}
		BufferPro::updateGraphicViews(buffer_id);
	}

	void BufferTSP::updateViews() {
		m_map_view_2d->updateView();
		BufferPro::updateViews();
	}
	
	void BufferTSP::setOptimaVisible(bool flag) {
		m_cur_datum->map_2d->setOptimaVisible(flag);
		BufferPro::setOptimaVisible(flag);
	}
}