#ifndef OFEC_DEMO_ITEMS_2D_TSP_H
#define OFEC_DEMO_ITEMS_2D_TSP_H

#include "../../../../../core/buffer/geometrics/geom_2d.h"
#include <core/definition.h>

namespace ofec_demo {
	class Map2dTSP {
	public:
		static const qreal ms_lenth;  // domain in coordinate of graphic scnene: [-ms_lenth, ms_lenth]

	protected:
		std::unique_ptr<Points2d> m_cities;
		std::unique_ptr<Rect2d> m_boundary;
		std::unique_ptr<PolyLine2d> m_optima;

	public:
		void updateMap(const std::vector<std::vector<ofec::Real>>& cities);
		void setMapVisible(bool flag);
		void addMap(QGraphicsScene *scene);
		void removeMap(QGraphicsScene *scene);

		void updateOptima(const std::vector<std::vector<ofec::Real>>& cities, const std::vector<int>& route);
		void setOptimaVisible(bool flag);
		void addOptima(QGraphicsScene *scene);
		void removeOptima(QGraphicsScene *scene);
	};
}

#endif // !OFEC_DEMO_ITEMS_2D_TSP_H