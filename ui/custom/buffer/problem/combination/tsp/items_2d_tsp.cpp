#include "items_2d_tsp.h"
#include <QGraphicsScene>

namespace ofec_demo {
	const qreal Map2dTSP::ms_lenth(100);
	
	void Map2dTSP::updateMap(const std::vector<std::vector<ofec::Real>> &cities) {
		ofec::Real min_x, max_x, min_y, max_y;
		min_x = max_x = cities.front()[0];
		min_y = max_y = cities.front()[1];
		for (size_t i = 0; i < cities.size(); i++) {
			if (min_x > cities[i][0])	min_x = cities[i][0];
			if (max_x < cities[i][0])	max_x = cities[i][0];
			if (min_y > cities[i][1])	min_y = cities[i][1];
			if (max_y < cities[i][1])	max_y = cities[i][1];
		}
		std::vector<QPointF> pts(cities.size());
		for (size_t i = 0; i < cities.size(); i++) {
			pts[i].setX(((cities[i][0] - min_x) / (max_x - min_x) * 2 - 1) * ms_lenth);
			pts[i].setY((1 - (cities[i][1] - min_y) / (max_y - min_y) * 2) * ms_lenth);
		}
		m_cities.reset(new Points2d(pts, 3, Qt::black));
		m_boundary.reset(new Rect2d(-1.2 * ms_lenth, -1.2 * ms_lenth, 2.4 * ms_lenth, 2.4 * ms_lenth, Qt::transparent, Qt::NoPen));
	}
	
	void Map2dTSP::setMapVisible(bool flag) {
		m_boundary->setVisible(flag);
		m_cities->setVisible(flag);
	}
	
	void Map2dTSP::addMap(QGraphicsScene *scene) {
		scene->addItem(m_boundary.get());
		scene->addItem(m_cities.get());
	}
	
	void Map2dTSP::removeMap(QGraphicsScene *scene) {
		scene->removeItem(m_boundary.get());
		scene->removeItem(m_cities.get());
	}

	void Map2dTSP::updateOptima(const std::vector<std::vector<ofec::Real>> &cities,
		const std::vector<int> &route)
	{
		ofec::Real min_x, max_x, min_y, max_y;
		min_x = max_x = cities.front()[0];
		min_y = max_y = cities.front()[1];
		for (size_t i = 0; i < cities.size(); i++) {
			if (min_x > cities[i][0])	min_x = cities[i][0];
			if (max_x < cities[i][0])	max_x = cities[i][0];
			if (min_y > cities[i][1])	min_y = cities[i][1];
			if (max_y < cities[i][1])	max_y = cities[i][1];
		}
		std::vector<QPointF> pts(route.size());
		for (size_t i = 0; i < route.size(); i++) {
			pts[i].setX(((cities[route[i]][0] - min_x) / (max_x - min_x) * 2 - 1) * ms_lenth);
			pts[i].setY((1 - (cities[route[i]][1] - min_y) / (max_y - min_y) * 2) * ms_lenth);
		}
		m_optima.reset(new PolyLine2d(pts, 1, Qt::black));
	}

	void Map2dTSP::setOptimaVisible(bool flag) {
		m_optima->setVisible(flag);
	}

	void Map2dTSP::addOptima(QGraphicsScene *scene) {
		scene->addItem(m_optima.get());
	}

	void Map2dTSP::removeOptima(QGraphicsScene *scene) {
		scene->removeItem(m_optima.get());
	}
}