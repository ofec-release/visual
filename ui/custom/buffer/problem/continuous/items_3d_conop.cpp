#include "items_3d_conop.h"
#include "../../../../core/buffer/geometrics/geom_3d.h"
#include <core/global_ui.h>

#include <vtkVertexGlyphFilter.h>
#include <vtkProperty.h>

namespace ofec_demo {
	void FitLndscp3d::updateFitnessLandscape(
		const std::vector<std::unique_ptr<ofec::Individual<>>> &samples,
		const std::vector<std::pair<ofec::Real, ofec::Real>> &boundary,
		const std::pair<ofec::Real, ofec::Real> &range_fit)
	{
		std::vector<double> sample_fits(samples.size());
		std::vector<std::array<ofec::Real, 3>> pts(samples.size());
		for (size_t i = 0; i < samples.size(); ++i) {
			pts[i][0] = (samples[i]->variable()[0] - boundary[0].first) / (boundary[0].second - boundary[0].first) * 10 - 5;
			pts[i][1] = (samples[i]->variable()[1] - boundary[1].first) / (boundary[1].second - boundary[1].first) * 10 - 5;
			pts[i][2] = sample_fits[i] = (samples[i]->fitness() - range_fit.first) / (range_fit.second - range_fit.first) * 6 - 3;
		}
		m_fit_lut = Geom3d::createRainbowLuT(sample_fits);
		m_fit_land = Geom3d::createSurface(pts, m_fit_lut);

		std::vector<std::array<ofec::Real, 3>> pts2(2);
		std::array<ofec::Real, 3> drc;
		pts2[0] = { -5, -5, -3 };
		pts2[1] = { 5 * 1.15, -5, -3 };
		drc = { 1, 0, 0 };
		m_axis_x1_line = Geom3d::createPolyLine(pts2, 0, 0, 0);
		m_axis_x1_cone = Geom3d::createCone(pts2[1], drc, 0.45, 0.15, 0, 0, 0);
		m_axis_x1_label = Geom3d::createText(pts2[1], "x1", 15, 0, 0, 0);
		pts2[1] = { -5, 5 * 1.15, -3 };
		drc = { 0, 1, 0 };
		m_axis_x2_line = Geom3d::createPolyLine(pts2, 0, 0, 0);
		m_axis_x2_cone = Geom3d::createCone(pts2[1], drc, 0.45, 0.15, 0, 0, 0);
		m_axis_x2_label = Geom3d::createText(pts2[1], "x2", 15, 0, 0, 0);
		pts2[1] = { -5, -5, 3 * 1.15 };
		drc = { 0, 0, 1 };
		m_axis_f_line = Geom3d::createPolyLine(pts2, 0, 0, 0);
		m_axis_f_cone = Geom3d::createCone(pts2[1], drc, 0.45, 0.15, 0, 0, 0);
		m_axis_f_label = Geom3d::createText(pts2[1], "f(x)", 15, 0, 0, 0);

		std::array<ofec::Real, 3> pts3, size;
		pts3 = { -5, -5, -3 };
		size = { 10, 10, 6 };
		m_boundary = Geom3d::createCuboidFrame(pts3, size, 0, 0, 0, 0.2);
	}

	void FitLndscp3d::addFitnessLandscape(vtkRenderer *renderer) {
		renderer->AddActor(m_fit_land);
		renderer->AddActor(m_axis_x1_line);
		renderer->AddActor(m_axis_x1_cone);
		renderer->AddActor(m_axis_x1_label);
		renderer->AddActor(m_axis_x2_line);
		renderer->AddActor(m_axis_x2_cone);
		renderer->AddActor(m_axis_x2_label);
		renderer->AddActor(m_axis_f_line);
		renderer->AddActor(m_axis_f_cone);
		renderer->AddActor(m_axis_f_label);
		renderer->AddActor(m_boundary);
	}

	void FitLndscp3d::removeFitnessLandscape(vtkRenderer *renderer) {
		renderer->RemoveActor(m_fit_land);
		renderer->RemoveActor(m_axis_x1_line);
		renderer->RemoveActor(m_axis_x1_cone);
		renderer->RemoveActor(m_axis_x1_label);
		renderer->RemoveActor(m_axis_x2_line);
		renderer->RemoveActor(m_axis_x2_cone);
		renderer->RemoveActor(m_axis_x2_label);
		renderer->RemoveActor(m_axis_f_line);
		renderer->RemoveActor(m_axis_f_cone);
		renderer->RemoveActor(m_axis_f_label);
		renderer->RemoveActor(m_boundary);
	}

	void FitLndscp3d::updateOptima(
		const std::vector<std::unique_ptr<ofec::Individual<>>>& optima, 
		const std::vector<std::pair<ofec::Real, ofec::Real>>& boundary, 
		const std::pair<ofec::Real, ofec::Real>& range_fit, bool is_pareto_set)
	{
		std::vector<std::array<ofec::Real, 3>> pts(optima.size());
		for (size_t i = 0; i < optima.size(); ++i) {
			pts[i][0] = (optima[i]->variable()[0] - boundary[0].first) / (boundary[0].second - boundary[0].first) * 10 - 5;
			pts[i][1] = (optima[i]->variable()[1] - boundary[1].first) / (boundary[1].second - boundary[1].first) * 10 - 5;
			pts[i][2] = (optima[i]->fitness() - range_fit.first) / (range_fit.second - range_fit.first) * 6 - 3 + 0.05;
		}
		if (!is_pareto_set) {
			m_fnd_lut = vtkLookupTable::New();
			m_fnd_lut->SetTableRange(0, 1);
			m_fnd_lut->SetNumberOfColors(2);
			m_fnd_lut->SetTableValue(1, 0, 0, 0);
			m_fnd_lut->SetTableValue(0, 0.6, 0.6, 0.6);
			m_fnd_lut->Build();
			m_fnd_scalar = vtkFloatArray::New();
			m_fnd_scalar->SetNumberOfTuples(pts.size());
			for (size_t i = 0; i < pts.size(); i++)
				m_fnd_scalar->SetTuple1(i, 0);

			m_optima_glyph = vtkVertexGlyphFilter::New();
			m_optima = Geom3d::createColoredGlyphs(pts, m_fnd_scalar, m_fnd_lut, m_optima_glyph);
			m_optima->GetProperty()->SetPointSize(8);
		}
		else 
			m_optima = Geom3d::createPoints(pts, 4, 1.0, 0.0, 0.0);
	}

	void FitLndscp3d::addOptima(vtkRenderer *renderer) {
		renderer->AddActor(m_optima);
	}

	void FitLndscp3d::removeOptima(vtkRenderer *renderer) {
		renderer->RemoveActor(m_optima);
	}

	void FitLndscp3d::setOptimaVisible(bool flag) {
		m_optima->SetVisibility(flag);
	}

	void FitLndscp3d::setOptimaFound(bool flag) {
		for (size_t i = 0; i < m_fnd_scalar->GetNumberOfTuples(); i++)
			m_fnd_scalar->SetTuple1(i, flag ? 1.0 : 0.0);
		m_optima_glyph->GetPolyDataInput(0)->Modified();
	}

	void FitLndscp3d::setOptimaFound(const std::vector<bool> &flag) {
		for (size_t i = 0; i < flag.size(); i++)
			m_fnd_scalar->SetTuple1(i, flag[i] ? 1.0 : 0.0);
		m_optima_glyph->GetPolyDataInput(0)->Modified();
	}

	void VarSpace3d::updateBoundary(const std::vector<std::pair<ofec::Real, ofec::Real>> &boundary) {
		std::vector<std::array<ofec::Real, 3>> pts2(2);
		std::array<ofec::Real, 3> drc;
		pts2[0] = { -5, -5, -5 };
		pts2[1] = { 5 * 1.15, -5, -5 };
		drc = { 1, 0, 0 };
		m_axis_x1_line = Geom3d::createPolyLine(pts2, 0, 0, 0);
		m_axis_x1_cone = Geom3d::createCone(pts2[1], drc, 0.45, 0.15, 0, 0, 0);
		m_axis_x1_label = Geom3d::createText(pts2[1], "x1", 15, 0, 0, 0);
		pts2[1] = { -5, 5 * 1.15, -5 };
		drc = { 0, 1, 0 };
		m_axis_x2_line = Geom3d::createPolyLine(pts2, 0, 0, 0);
		m_axis_x2_cone = Geom3d::createCone(pts2[1], drc, 0.45, 0.15, 0, 0, 0);
		m_axis_x2_label = Geom3d::createText(pts2[1], "x2", 15, 0, 0, 0);
		pts2[1] = { -5, -5, 5 * 1.15 };
		drc = { 0, 0, 1 };
		m_axis_x3_line = Geom3d::createPolyLine(pts2, 0, 0, 0);
		m_axis_x3_cone = Geom3d::createCone(pts2[1], drc, 0.45, 0.15, 0, 0, 0);
		m_axis_x3_label = Geom3d::createText(pts2[1], "x3", 15, 0, 0, 0);

		std::array<ofec::Real, 3> pts3, size;
		pts3 = { -5, -5, -5 };
		size = { 10, 10, 10 };
		m_boundary = Geom3d::createCuboidFrame(pts3, size, 0, 0, 0, 0.2);
	}
	
	void VarSpace3d::addBoundary(vtkRenderer *renderer) {
		renderer->AddActor(m_axis_x1_line);
		renderer->AddActor(m_axis_x1_cone);
		renderer->AddActor(m_axis_x1_label);
		renderer->AddActor(m_axis_x2_line);
		renderer->AddActor(m_axis_x2_cone);
		renderer->AddActor(m_axis_x2_label);
		renderer->AddActor(m_axis_x3_line);
		renderer->AddActor(m_axis_x3_cone);
		renderer->AddActor(m_axis_x3_label);
		renderer->AddActor(m_boundary);
	}

	void VarSpace3d::removeBoundary(vtkRenderer *renderer) {
		renderer->RemoveActor(m_axis_x1_line);
		renderer->RemoveActor(m_axis_x1_cone);
		renderer->RemoveActor(m_axis_x1_label);
		renderer->RemoveActor(m_axis_x2_line);
		renderer->RemoveActor(m_axis_x2_cone);
		renderer->RemoveActor(m_axis_x2_label);
		renderer->RemoveActor(m_axis_x3_line);
		renderer->RemoveActor(m_axis_x3_cone);
		renderer->RemoveActor(m_axis_x3_label);
		renderer->RemoveActor(m_boundary);
	}
	
	void VarSpace3d::updateOptima(
		const std::vector<std::unique_ptr<ofec::Individual<>>> &optima, 
		const std::vector<std::pair<ofec::Real, ofec::Real>> &boundary, 
		bool is_pareto_set)
	{
		std::vector<std::array<ofec::Real, 3>> pts(optima.size());
		for (size_t i = 0; i < optima.size(); ++i) {
			pts[i][0] = (optima[i]->variable()[0] - boundary[0].first) / (boundary[0].second - boundary[0].first) * 10 - 5;
			pts[i][1] = (optima[i]->variable()[1] - boundary[1].first) / (boundary[1].second - boundary[1].first) * 10 - 5;
			pts[i][2] = (optima[i]->variable()[2] - boundary[2].first) / (boundary[2].second - boundary[2].first) * 10 - 5;
		}
		if (!is_pareto_set) {
			m_fnd_lut = vtkLookupTable::New();
			m_fnd_lut->SetTableRange(0, 1);
			m_fnd_lut->SetNumberOfColors(2);
			m_fnd_lut->SetTableValue(1, 0, 0, 0);
			m_fnd_lut->SetTableValue(0, 0.6, 0.6, 0.6);
			m_fnd_lut->Build();
			m_fnd_scalar = vtkFloatArray::New();
			m_fnd_scalar->SetNumberOfTuples(pts.size());
			for (size_t i = 0; i < pts.size(); i++)
				m_fnd_scalar->SetTuple1(i, 0);

			m_optima_glyph = vtkVertexGlyphFilter::New();
			m_optima = Geom3d::createColoredGlyphs(pts, m_fnd_scalar, m_fnd_lut, m_optima_glyph);
			m_optima->GetProperty()->SetPointSize(8);
		}
		else
			m_optima = Geom3d::createPoints(pts, 4, 1.0, 0.0, 0.0);
	}
	
	void VarSpace3d::addOptima(vtkRenderer *renderer) {
		renderer->AddActor(m_optima);
	}
	
	void VarSpace3d::removeOptima(vtkRenderer *renderer) {
		renderer->RemoveActor(m_optima);
	}
	
	void VarSpace3d::setOptimaVisible(bool flag) {
		m_optima->SetVisibility(flag);
	}
	
	void VarSpace3d::setOptimaFound(bool flag) {
		for (size_t i = 0; i < m_fnd_scalar->GetNumberOfTuples(); i++)
			m_fnd_scalar->SetTuple1(i, flag ? 1.0 : 0.0);
		m_optima_glyph->GetPolyDataInput(0)->Modified();
	}

	void VarSpace3d::setOptimaFound(const std::vector<bool> &flag) {
		for (size_t i = 0; i < flag.size(); i++)
			m_fnd_scalar->SetTuple1(i, flag[i] ? 1.0 : 0.0);
		m_optima_glyph->GetPolyDataInput(0)->Modified();
	}
}