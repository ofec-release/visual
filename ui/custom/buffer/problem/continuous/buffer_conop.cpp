#include "buffer_conop.h"
#include "../../../param/problem/continuous/visual_conop.h"
#include "../../../../core/global_ui.h"
#include <core/problem/continuous/continuous.h>
#include <utility/functional.h>
#include <utility/nondominated_sorting/fast_sort.h>

namespace ofec_demo {
	BufferConOP::BufferConOP(GraphicsArea *graphic_dock_set, int id_pro) :
		BufferPro(graphic_dock_set, id_pro),
		m_cur_datum(nullptr)	{}

	BufferConOP::~BufferConOP() {
		using namespace ofec;
		if (m_cur_datum)
			removeCurDatum();
		if (GET_CONOP(m_id_pro).numVariables() == 1) {
			m_graphics_area->removeGraphic("Fitness Landscape");
		}
		else if (GET_CONOP(m_id_pro).numVariables() == 2) {
			m_graphics_area->removeGraphic("Fitness Landscape");
			m_graphics_area->removeGraphic("Decision Space");
		}
		else if (GET_CONOP(m_id_pro).numVariables() == 3) {
			m_graphics_area->removeGraphic("Decision Space");
		}
		//m_graphics_area->removeGraphic("Pikachu");
	}

	void BufferConOP::initializeGraphicViews() {
		BufferPro::initializeGraphicViews();
		using namespace ofec;
		if (GET_CONOP(m_id_pro).numVariables() == 1) {
			m_fit_lndscp_view_2d.reset(new GraphicView2d);
			m_graphics_area->addGraphic("Fitness Landscape", m_fit_lndscp_view_2d.get());
		}
		else if (GET_CONOP(m_id_pro).numVariables() == 2) {
			m_var_space_view_2d.reset(new GraphicView2d);
			m_graphics_area->addGraphic("Decision Space", m_var_space_view_2d.get());

			m_fit_lndscp_view_3d.reset(new GraphicView3d);
			m_graphics_area->addGraphic("Fitness Landscape", m_fit_lndscp_view_3d.get());	
		}
		else if (GET_CONOP(m_id_pro).numVariables() == 3) {
			m_var_space_view_3d.reset(new GraphicView3d);
			m_graphics_area->addGraphic("Decision Space", m_var_space_view_3d.get());
		}
		//m_pikachu_view_3d.reset(new ModelView3d);
		//m_pikachu_view_3d->importObjModel(FIG_DIR "/pikachu", "pikachu-pokemon-go");
		//m_graphics_area->addGraphic("Pikachu", m_pikachu_view_3d.get());
	}

	bool BufferConOP::graphicViewsInitialized() {
		using namespace ofec;
		bool flag = true;
		if (GET_CONOP(m_id_pro).numVariables() == 1)
			flag = flag && m_fit_lndscp_view_2d;
		else if (GET_CONOP(m_id_pro).numVariables() == 2)
			flag = flag && m_fit_lndscp_view_3d && m_var_space_view_2d;
		else if (GET_CONOP(m_id_pro).numVariables() == 3)
			flag = flag && m_var_space_view_3d;
		return BufferPro::graphicViewsInitialized() && flag;
	}

	void BufferConOP::appendBuffer(int max_buffer_id) {
		using namespace ofec;
		updateSample();
		updateOptima();
		auto buffer_datum = new BufferDatum;
		buffer_datum->range_var = GET_CONOP(m_id_pro).boundary();
		if (GET_CONOP(m_id_pro).numVariables() == 1)
			updateFitLndscap2d(buffer_datum);
		else if (GET_CONOP(m_id_pro).numVariables() == 2) {
			updateFitLndscap3d(buffer_datum);
			updateVarSpace2d(buffer_datum);
		}
		else if (GET_CONOP(m_id_pro).numVariables() == 3)
			updateVarSpace3d(buffer_datum);
		m_buffer_data[max_buffer_id + 1].reset(buffer_datum);
		BufferPro::appendBuffer(max_buffer_id);
	}

	void BufferConOP::popupBuffer(int buffer_id) {
		m_buffer_data.erase(buffer_id);
		BufferPro::popupBuffer(buffer_id);
	}

	void BufferConOP::updateGraphicViews(int buffer_id) {	
		if (m_cur_datum)
			removeCurDatum();
		if (m_buffer_data.count(buffer_id) > 0)
			addNewDatum(buffer_id);
		BufferPro::updateGraphicViews(buffer_id);
	}

	void BufferConOP::updateViews() {
		using namespace ofec;
		if (GET_CONOP(m_id_pro).numVariables() == 1)
			m_fit_lndscp_view_2d->updateView();
		else if (GET_CONOP(m_id_pro).numVariables() == 2) {
			m_fit_lndscp_view_3d->updateView();
			m_var_space_view_2d->updateView();
		}
		else if (GET_CONOP(m_id_pro).numVariables() == 3)
			m_var_space_view_3d->updateView();
		BufferPro::updateViews();
	}

	void BufferConOP::setOptimaVisible(bool flag) {
		using namespace ofec;
		if (m_cur_datum && GET_PRO(m_id_pro).isOptimaVarGiven()) {
			if (GET_CONOP(m_id_pro).getOptima().isVariableGiven()) {
				if (GET_CONOP(m_id_pro).numVariables() == 1)
					m_cur_datum->fit_lndscp_2d->setOptimaVisible(flag);
				else if (GET_CONOP(m_id_pro).numVariables() == 2) {
					m_cur_datum->fit_lndscp_3d->setOptimaVisible(flag);
					m_cur_datum->var_space_2d->setOptimaVisible(flag);
				}
				else if (GET_CONOP(m_id_pro).numVariables() == 3) 
					m_cur_datum->var_space_3d->setOptimaVisible(flag);				
			}
		}
		BufferPro::setOptimaVisible(flag);
	}

	void BufferConOP::setOptVarsFound(int id_buffer, bool flag) {
		using namespace ofec;
		if (!m_buffer_data.count(id_buffer) > 0)
			return;
		if (GET_CONOP(m_id_pro).getOptima().isVariableGiven()) {
			if (GET_CONOP(m_id_pro).numVariables() == 1)
				m_buffer_data[id_buffer]->fit_lndscp_2d->setOptimaFound(flag);
			else if (GET_CONOP(m_id_pro).numVariables() == 2) {
				m_buffer_data[id_buffer]->fit_lndscp_3d->setOptimaFound(flag);
				m_buffer_data[id_buffer]->var_space_2d->setOptimaFound(flag);
			}
			else if (GET_CONOP(m_id_pro).numVariables() == 3)
				m_buffer_data[id_buffer]->var_space_3d->setOptimaFound(flag);
		}
	}

	void BufferConOP::setOptVarsFound(int id_buffer, const std::vector<bool> &flag) {
		using namespace ofec;
		if (!m_buffer_data.count(id_buffer) > 0)
			return;
		if (GET_CONOP(m_id_pro).getOptima().isVariableGiven()) {
			if (GET_CONOP(m_id_pro).numVariables() == 1)
				m_buffer_data[id_buffer]->fit_lndscp_2d->setOptimaFound(flag);
			else if (GET_CONOP(m_id_pro).numVariables() == 2) {
				m_buffer_data[id_buffer]->fit_lndscp_3d->setOptimaFound(flag);
				m_buffer_data[id_buffer]->var_space_2d->setOptimaFound(flag);
			}
			else if (GET_CONOP(m_id_pro).numVariables() == 3)
				m_buffer_data[id_buffer]->var_space_3d->setOptimaFound(flag);
		}
	}

	void BufferConOP::removeItemsVarSpaceView2d() {
		using namespace ofec;
		m_cur_datum->var_space_2d->removeBoundary(m_var_space_view_2d->scene());
		if (m_cur_datum && GET_CONOP(m_id_pro).getOptima().isVariableGiven())
			m_cur_datum->var_space_2d->removeOptima(m_var_space_view_2d->scene());
	}

	void BufferConOP::addItemsVarSpaceView2d() {
		using namespace ofec;
		m_cur_datum->var_space_2d->addBoundary(m_var_space_view_2d->scene());
		if (m_cur_datum && GET_CONOP(m_id_pro).getOptima().isVariableGiven())
			m_cur_datum->var_space_2d->addOptima(m_var_space_view_2d->scene());
	}

	void BufferConOP::updateSample() {
		using namespace ofec;
		size_t num_vars = GET_CONOP(m_id_pro).numVariables();
		size_t num_objs = GET_PRO(m_id_pro).numObjectives();
		size_t num_cons = GET_PRO(m_id_pro).numConstraints();
		if (num_vars > 2)
			return;
		auto boundary = GET_CONOP(m_id_pro).boundary();
		if (num_vars == 1) {
			m_samples.resize(VisualConOP::ms_num_div);
			for (size_t i = 0; i < VisualConOP::ms_num_div; ++i) {
				m_samples[i].reset(new Individual<>(num_objs, num_cons, num_vars));
				m_samples[i]->variable()[0] = boundary[0].first + i * (boundary[0].second - boundary[0].first) / (VisualConOP::ms_num_div - 1);
			}
		}
		else { // num_vars == 2
			m_samples.resize(VisualConOP::ms_num_div * VisualConOP::ms_num_div);
			for (auto& ind : m_samples)
				ind.reset(new Individual<>(num_objs, num_cons, num_vars));
			ofec::Real x1;
			for (size_t i = 0; i < VisualConOP::ms_num_div; ++i) {
				x1 = boundary[0].first + i * (boundary[0].second - boundary[0].first) / (VisualConOP::ms_num_div - 1);
				for (size_t j = 0; j < VisualConOP::ms_num_div; ++j)
					m_samples[j * VisualConOP::ms_num_div + i]->variable()[0] = x1;
			}
			ofec::Real x2;
			for (size_t i = 0; i < VisualConOP::ms_num_div; ++i) {
				x2 = boundary[1].first + i * (boundary[1].second - boundary[1].first) / (VisualConOP::ms_num_div - 1);
				for (size_t j = 0; j < VisualConOP::ms_num_div; ++j)
					m_samples[i * VisualConOP::ms_num_div + j]->variable()[1] = x2;
			}
		}

		std::vector<std::thread> thrds;
		int num_task = std::thread::hardware_concurrency();
		int rest = m_samples.size() % num_task;
		int id1 = 0, id2 = id1 + m_samples.size() / num_task - 1 + (rest-- > 0 ? 1 : 0);
		for (size_t i = 0; i < num_task; ++i) {
			std::vector<int> ids;
			for (int r = id1; r <= id2; ++r)
				ids.push_back(r);
			id1 = id2 + 1;
			id2 = id1 + m_samples.size() / num_task - 1 + (rest-- > 0 ? 1 : 0);
			thrds.push_back(std::thread(&BufferConOP::evaluateSamples, this, ids));
		}
		for (auto &thrd : thrds)
			thrd.join();
		
		if (m_range_obj.size() != num_objs) {
			m_range_obj.resize(num_objs);
			for (size_t j = 0; j < num_objs; ++j)
				m_range_obj[j].first = m_range_obj[j].second = m_samples.front()->objective(j);
		}
		for (size_t j = 0; j < num_objs; ++j) {
			for (size_t i = 1; i < m_samples.size(); ++i) {
				if (m_samples[i]->objective(j) > m_range_obj[j].second) m_range_obj[j].second = m_samples[i]->objective(j);
				if (m_samples[i]->objective(j) < m_range_obj[j].first) m_range_obj[j].first = m_samples[i]->objective(j);
			}
		}

		if (num_objs == 1) {
			ofec::Real pos = (GET_PRO(m_id_pro).optMode(0) == ofec::OptMode::kMaximize) ? 1 : -1;
			for (auto& ind : m_samples)
				ind->setFitness(pos * ind->objective(0));
		}
		else {
			std::vector<std::vector<ofec::Real>*> objs;
			for (auto& sol : m_samples)
				objs.push_back(&sol->objective());
			std::vector<int> rank;
			ofec::nd_sort::fastSort(objs, rank, GET_PRO(m_id_pro).optMode());
			for (size_t i = 0; i < m_samples.size(); ++i)
				m_samples[i]->setFitness(-rank[i]);
		}

		m_range_fit.first = m_range_fit.second = m_samples.front()->fitness();
		for (auto& ind : m_samples) {
			if (ind->fitness() > m_range_fit.second)	m_range_fit.second = ind->fitness();
			if (ind->fitness() < m_range_fit.first)	m_range_fit.first = ind->fitness();
		}
	}

	void BufferConOP::evaluateSamples(std::vector<int> ids) {
		for (int id : ids)
			m_samples[id]->evaluate(m_id_pro, -1, false);
	}

	void BufferConOP::updateOptima() {
		using namespace ofec;
		m_optima.clear();
		m_optima_obj.clear();
		size_t num_vars = GET_CONOP(m_id_pro).numVariables();
		size_t num_objs = GET_PRO(m_id_pro).numObjectives();
		size_t num_cons = GET_PRO(m_id_pro).numConstraints();
		if (GET_PRO(m_id_pro).isOptimaVarGiven() && num_vars <= 3) {
			size_t num_opts = GET_CONOP(m_id_pro).getOptima().numberVariables();
			m_optima.resize(num_opts);
			for (size_t i = 0; i < num_opts; ++i) {
				m_optima[i].reset(new Individual<>(num_objs, num_cons, num_vars));
				m_optima[i]->variable() = GET_CONOP(m_id_pro).getOptima().variable(i);
				m_optima[i]->evaluate(m_id_pro, -1, false);
				m_optima_obj.push_back(m_optima[i]->objective());
			}
			if (num_objs == 1) {
				ofec::Real pos = (GET_PRO(m_id_pro).optMode(0) == ofec::OptMode::kMaximize) ? 1 : -1;
				for (auto &opt : m_optima)
					opt->setFitness(pos * opt->objective(0));
			}
			else {
				for (auto &opt : m_optima)
					opt->setFitness(1); 					// To Do
			}
		}
		else if (GET_PRO(m_id_pro).isOptimaObjGiven()) {
			size_t num_opts = GET_CONOP(m_id_pro).getOptima().numberObjectives();
			for (size_t i = 0; i < num_opts; ++i)
				m_optima_obj.push_back(GET_CONOP(m_id_pro).getOptima().objective(i));
		}
		if (!m_optima_obj.empty()) {
			m_range_obj.resize(num_objs);
			for (size_t j = 0; j < num_objs; ++j)
				m_range_obj[j].first = m_range_obj[j].second = m_optima_obj[0][j];
			for (size_t j = 0; j < num_objs; ++j) {
				for (size_t i = 1; i < m_optima_obj.size(); ++i) {
					if (m_optima_obj[i][j] > m_range_obj[j].second) m_range_obj[j].second = m_optima_obj[i][j];
					if (m_optima_obj[i][j] < m_range_obj[j].first) m_range_obj[j].first = m_optima_obj[i][j];
				}
			}
		}
	}

	void BufferConOP::updateFitLndscap2d(BufferDatum *buffer_datum) {
		using namespace ofec;
		buffer_datum->fit_lndscp_2d.reset(new FitLndscp2d);
		buffer_datum->fit_lndscp_2d->updateFitnessLandscape(m_samples, buffer_datum->range_var, m_range_fit);
		if (GET_PRO(m_id_pro).isOptimaVarGiven())
			buffer_datum->fit_lndscp_2d->updateOptima(
				m_optima, GET_CONOP(m_id_pro).boundary(), m_range_fit, GET_PRO(m_id_pro).hasTag(ProTag::kMOP));
	}

	void BufferConOP::updateFitLndscap3d(BufferDatum *buffer_datum) {
		using namespace ofec;
		buffer_datum->fit_lndscp_3d.reset(new FitLndscp3d);
		buffer_datum->fit_lndscp_3d->updateFitnessLandscape(m_samples, buffer_datum->range_var, m_range_fit);
		if (GET_PRO(m_id_pro).isOptimaVarGiven())
			buffer_datum->fit_lndscp_3d->updateOptima(
				m_optima, GET_CONOP(m_id_pro).boundary(), m_range_fit, GET_PRO(m_id_pro).hasTag(ProTag::kMOP));
	}

	void BufferConOP::updateVarSpace2d(BufferDatum *buffer_datum) {
		using namespace ofec;
		buffer_datum->var_space_2d.reset(new VarSpace2d);
		buffer_datum->var_space_2d->updateBoundary(buffer_datum->range_var);
		if (GET_PRO(m_id_pro).isOptimaVarGiven())
			buffer_datum->var_space_2d->updateOptima(
				m_optima, buffer_datum->range_var, GET_PRO(m_id_pro).hasTag(ProTag::kMOP));
	}

	void BufferConOP::updateVarSpace3d(BufferDatum *buffer_datum) {
		using namespace ofec;
		buffer_datum->var_space_3d.reset(new VarSpace3d);
		buffer_datum->var_space_3d->updateBoundary(buffer_datum->range_var);
		if (GET_PRO(m_id_pro).isOptimaVarGiven())
			buffer_datum->var_space_3d->updateOptima(
				m_optima, buffer_datum->range_var, GET_PRO(m_id_pro).hasTag(ProTag::kMOP));
	}

	void BufferConOP::removeCurDatum() {
		using namespace ofec;
		if (GET_CONOP(m_id_pro).numVariables() == 1) {
			m_cur_datum->fit_lndscp_2d->removeFitnessLandscape(m_fit_lndscp_view_2d->scene());
			if (GET_PRO(m_id_pro).isOptimaVarGiven())
				m_cur_datum->fit_lndscp_2d->removeOptima(m_fit_lndscp_view_2d->scene());
		}
		else if (GET_CONOP(m_id_pro).numVariables() == 2) {
			m_cur_datum->fit_lndscp_3d->removeFitnessLandscape(m_fit_lndscp_view_3d->renderer());
			m_cur_datum->var_space_2d->removeBoundary(m_var_space_view_2d->scene());
			if (GET_PRO(m_id_pro).isOptimaVarGiven()) {
				m_cur_datum->fit_lndscp_3d->removeOptima(m_fit_lndscp_view_3d->renderer());
				m_cur_datum->var_space_2d->removeOptima(m_var_space_view_2d->scene());
			}
		}
		else if (GET_CONOP(m_id_pro).numVariables() == 3) {
			m_cur_datum->var_space_3d->removeBoundary(m_var_space_view_3d->renderer());
			if (GET_PRO(m_id_pro).isOptimaVarGiven())
				m_cur_datum->var_space_3d->removeOptima(m_var_space_view_3d->renderer());
		}
		m_cur_datum = nullptr;
	}

	void BufferConOP::addNewDatum(int buffer_id) {
		using namespace ofec;
		auto& new_datum = m_buffer_data.at(buffer_id);
		if (GET_CONOP(m_id_pro).numVariables() == 1) {
			new_datum->fit_lndscp_2d->addFitnessLandscape(m_fit_lndscp_view_2d->scene());
			if (GET_PRO(m_id_pro).isOptimaVarGiven()) {
				new_datum->fit_lndscp_2d->addOptima(m_fit_lndscp_view_2d->scene());
				new_datum->fit_lndscp_2d->setOptimaVisible(VisualPro::ms_show_optima);
			}
		}
		else if (GET_CONOP(m_id_pro).numVariables() == 2) {
			new_datum->fit_lndscp_3d->addFitnessLandscape(m_fit_lndscp_view_3d->renderer());
			new_datum->var_space_2d->addBoundary(m_var_space_view_2d->scene());
			if (GET_PRO(m_id_pro).isOptimaVarGiven()) {
				new_datum->fit_lndscp_3d->addOptima(m_fit_lndscp_view_3d->renderer());
				new_datum->var_space_2d->addOptima(m_var_space_view_2d->scene());
				new_datum->fit_lndscp_3d->setOptimaVisible(VisualPro::ms_show_optima);
				new_datum->var_space_2d->setOptimaVisible(VisualPro::ms_show_optima);
			}
			m_fit_lndscp_view_3d->resetCamera();
		}
		else if (GET_CONOP(m_id_pro).numVariables() == 3) {
			new_datum->var_space_3d->addBoundary(m_var_space_view_3d->renderer());
			if (GET_PRO(m_id_pro).isOptimaVarGiven()) {
				new_datum->var_space_3d->addOptima(m_var_space_view_3d->renderer());
				new_datum->var_space_3d->setOptimaVisible(VisualPro::ms_show_optima);
			}
			m_var_space_view_3d->resetCamera();
		}
		m_cur_datum = new_datum.get();
	}
}