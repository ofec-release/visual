#ifndef OFEC_DEMO_ITEMS_3D_CONOP_H
#define OFEC_DEMO_ITEMS_3D_CONOP_H

#include <vtkActor.h>
#include <vtkSmartPointer.h>
#include <vtkFloatArray.h>
#include <vtkLookupTable.h>
#include <vtkRenderer.h>
#include <vtkBillboardTextActor3D.h>
#include <core/algorithm/individual.h>

#include <vtkPolyDataAlgorithm.h>

namespace ofec_demo {
	class FitLndscp3d {
	private:
		vtkSmartPointer<vtkActor> m_fit_land;
		vtkSmartPointer<vtkLookupTable> m_fit_lut;
		vtkSmartPointer<vtkActor> m_axis_x1_line, m_axis_x2_line, m_axis_f_line;
		vtkSmartPointer<vtkActor> m_axis_x1_cone, m_axis_x2_cone, m_axis_f_cone;
		vtkSmartPointer<vtkBillboardTextActor3D> m_axis_x1_label, m_axis_x2_label, m_axis_f_label;
		vtkSmartPointer<vtkActor> m_boundary;

		vtkSmartPointer<vtkActor> m_optima;
		vtkSmartPointer<vtkPolyDataAlgorithm> m_optima_glyph;
		vtkSmartPointer<vtkFloatArray> m_fnd_scalar;
		vtkSmartPointer<vtkLookupTable> m_fnd_lut;

	public:
		void updateFitnessLandscape(
			const std::vector<std::unique_ptr<ofec::Individual<>>>& samples, 
			const std::vector<std::pair<ofec::Real, ofec::Real>>& boundary, 
			const std::pair<ofec::Real, ofec::Real>& range_fit);
		void addFitnessLandscape(vtkRenderer *renderer);
		void removeFitnessLandscape(vtkRenderer *renderer);	

		void updateOptima(
			const std::vector<std::unique_ptr<ofec::Individual<>>>& optima, 
			const std::vector<std::pair<ofec::Real, ofec::Real>>& boundary, 
			const std::pair<ofec::Real, ofec::Real>& range_fit, bool is_pareto_set);
		void addOptima(vtkRenderer *renderer);
		void removeOptima(vtkRenderer *renderer);
		
		void setOptimaVisible(bool flag);
		void setOptimaFound(bool flag);
		void setOptimaFound(const std::vector<bool> &flag);
		vtkLookupTable* getFitLUT() { return m_fit_lut; }
	};

	class VarSpace3d {
	private:
		vtkSmartPointer<vtkActor> m_axis_x1_line, m_axis_x2_line, m_axis_x3_line;
		vtkSmartPointer<vtkActor> m_axis_x1_cone, m_axis_x2_cone, m_axis_x3_cone;
		vtkSmartPointer<vtkBillboardTextActor3D> m_axis_x1_label, m_axis_x2_label, m_axis_x3_label;
		vtkSmartPointer<vtkActor> m_boundary;

		vtkSmartPointer<vtkActor> m_optima;
		vtkSmartPointer<vtkPolyDataAlgorithm> m_optima_glyph;
		vtkSmartPointer<vtkFloatArray> m_fnd_scalar;
		vtkSmartPointer<vtkLookupTable> m_fnd_lut;

	public:
		void updateBoundary(const std::vector<std::pair<ofec::Real, ofec::Real>> &boundary);
		void addBoundary(vtkRenderer *renderer);
		void removeBoundary(vtkRenderer *renderer);

		void updateOptima(
			const std::vector<std::unique_ptr<ofec::Individual<>>> &optima,
			const std::vector<std::pair<ofec::Real, ofec::Real>> &boundary,
			bool is_pareto_set);
		void addOptima(vtkRenderer *renderer);
		void removeOptima(vtkRenderer *renderer);

		void setOptimaVisible(bool flag);
		void setOptimaFound(bool flag);
		void setOptimaFound(const std::vector<bool> &flag);
	};
}

#endif // !OFEC_DEMO_ITEMS_3D_CONOP_H