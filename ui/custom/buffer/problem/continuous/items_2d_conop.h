#ifndef OFEC_DEMO_ITEMS_2D_CONOP_H
#define OFEC_DEMO_ITEMS_2D_CONOP_H

#include "../../../../core/buffer/geometrics/geom_2d.h"
#include <core/definition.h>
#include <core/algorithm/individual.h>

namespace ofec_demo {
	class FitLndscp2d {
	public:
		static const qreal ms_lenth;  // domain in coordinate of graphic scnene: [-ms_lenth, ms_lenth]

	protected:
		std::unique_ptr<PolyLine2d> m_fit_land;
		std::unique_ptr<Rect2d> m_space_top, m_space_right;
		std::unique_ptr<Arrow2d> m_axis_x, m_axis_f;
		std::unique_ptr<Text2d> m_title_bottom, m_title_left;
		std::vector<std::unique_ptr<Points2d>> m_optima;
		QColor m_color_fnd, m_color_not_fnd, m_color_pareto_set;
		QFont m_font_title;

	public:
		FitLndscp2d();
		void updateFitnessLandscape(
			const std::vector<std::unique_ptr<ofec::Individual<>>> &samples,
			const std::vector<std::pair<ofec::Real, ofec::Real>> &boundary,
			const std::pair<ofec::Real, ofec::Real> &range_fit);
		void addFitnessLandscape(QGraphicsScene *scene);
		void removeFitnessLandscape(QGraphicsScene *scene);

		void updateOptima(
			const std::vector<std::unique_ptr<ofec::Individual<>>> &optima,
			const std::vector<std::pair<ofec::Real, ofec::Real>> &boundary,
			const std::pair<ofec::Real, ofec::Real> &range_fit, bool is_pareto_set);
		void addOptima(QGraphicsScene *scene);
		void removeOptima(QGraphicsScene *scene);

		void setOptimaVisible(bool flag);
		void setOptimaFound(bool flag);
		void setOptimaFound(const std::vector<bool> &flag);
	};

	class VarSpace2d {
	public:
		static const qreal ms_lenth;  // domain in coordinate of graphic scnene: [-ms_lenth, ms_lenth]

	protected:
		std::unique_ptr<Rect2d> m_boundary, m_space_top, m_space_right;
		std::unique_ptr<Arrow2d> m_axis_x1, m_axis_x2;
		std::unique_ptr<Text2d> m_title_bottom, m_title_left;
		std::vector<std::unique_ptr<Points2d>> m_optima;
		QColor m_color_fnd, m_color_not_fnd, m_color_pareto_set;
		QFont m_font_title;

	public:
		VarSpace2d();
		void updateBoundary(const std::vector<std::pair<ofec::Real, ofec::Real>> &range);
		void addBoundary(QGraphicsScene *scene);
		void removeBoundary(QGraphicsScene *scene);

		void updateOptima(const std::vector<std::unique_ptr<ofec::Individual<>>> &optima,
			const std::vector<std::pair<ofec::Real, ofec::Real>> &range, bool is_pareto_set);
		void addOptima(QGraphicsScene *scene);
		void removeOptima(QGraphicsScene *scene);

		void setOptimaVisible(bool flag);
		void setOptimaFound(bool flag);
		void setOptimaFound(const std::vector<bool> &flag);
	};
}

#endif // !OFEC_DEMO_ITEMS_2D_CONOP_H
