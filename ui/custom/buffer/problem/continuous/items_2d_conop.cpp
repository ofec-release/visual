#include "items_2d_conop.h"
#include <QGraphicsScene>

namespace ofec_demo {	
	const qreal FitLndscp2d::ms_lenth(100);

	FitLndscp2d::FitLndscp2d() : 
		m_color_fnd(Qt::black), 
		m_color_not_fnd(Qt::gray),
		m_color_pareto_set(Qt::red),
		m_font_title("Courier", 4, QFont::Bold) {}

	void FitLndscp2d::updateFitnessLandscape(
		const std::vector<std::unique_ptr<ofec::Individual<>>> &samples,
		const std::vector<std::pair<ofec::Real, ofec::Real>> &boundary,
		const std::pair<ofec::Real, ofec::Real> &range_fit) 
	{
		std::vector<QPointF> pts(samples.size());
		for (size_t i = 0; i < samples.size(); i++) {
			pts[i].setX(((samples[i]->variable()[0] - boundary[0].first) / (boundary[0].second - boundary[0].first) * 2 - 1) * ms_lenth);
			pts[i].setY((1 - (samples[i]->fitness() - range_fit.first) / (range_fit.second - range_fit.first) * 2) * ms_lenth / 2);
		}
		m_fit_land.reset(new PolyLine2d(pts, 1, Qt::black));
		m_axis_f.reset(new Arrow2d(-ms_lenth, ms_lenth/2, -ms_lenth, -0.65 * ms_lenth));
		m_axis_x.reset(new Arrow2d(-ms_lenth, ms_lenth / 2, 1.15 * ms_lenth, ms_lenth / 2));
		m_title_bottom.reset(new Text2d(-ms_lenth, ms_lenth / 2, ms_lenth * 2, ms_lenth * 0.2, "x", m_font_title, Qt::black, false));
		m_title_left.reset(new Text2d(-ms_lenth / 2, -1.2 * ms_lenth, ms_lenth, ms_lenth * 0.2, "f(x)", m_font_title, Qt::black, false, -90));
		m_space_top.reset(new Rect2d(-ms_lenth, -0.75 * ms_lenth, ms_lenth * 2, ms_lenth * 0.25, Qt::transparent, Qt::NoPen));
		m_space_right.reset(new Rect2d(ms_lenth, -ms_lenth / 2, ms_lenth * 0.2, ms_lenth, Qt::transparent, Qt::NoPen));
	}

	void FitLndscp2d::addFitnessLandscape(QGraphicsScene *scene) {
		scene->addItem(m_fit_land.get());
		scene->addItem(m_axis_x.get());
		scene->addItem(m_axis_f.get());
		scene->addItem(m_title_bottom.get());
		scene->addItem(m_title_left.get());
		scene->addItem(m_space_top.get());
		scene->addItem(m_space_right.get());
	}

	void FitLndscp2d::removeFitnessLandscape(QGraphicsScene *scene) {
		scene->removeItem(m_fit_land.get());
		scene->removeItem(m_axis_x.get());
		scene->removeItem(m_axis_f.get());
		scene->removeItem(m_title_bottom.get());
		scene->removeItem(m_title_left.get());
		scene->removeItem(m_space_top.get());
		scene->removeItem(m_space_right.get());
	}

	void FitLndscp2d::updateOptima(
		const std::vector<std::unique_ptr<ofec::Individual<>>> &optima,
		const std::vector<std::pair<ofec::Real, ofec::Real>> &boundary,
		const std::pair<ofec::Real, ofec::Real> &range_fit, bool is_pareto_set)
	{
		m_optima.clear();
		if (!is_pareto_set) {
			std::vector<QPointF> tmp_pts(1);
			for (const auto &optimum : optima) {
				tmp_pts[0].setX(((optimum->variable()[0] - boundary[0].first) / (boundary[0].second - boundary[0].first) * 2 - 1) * ms_lenth);
				tmp_pts[0].setY((1 - (optimum->fitness() - range_fit.first) / (range_fit.second - range_fit.first) * 2) * ms_lenth / 2);
				auto new_pts = new Points2d(tmp_pts, 6, m_color_not_fnd);
				m_optima.emplace_back(new_pts);
			}
		}
		else {
			std::vector<QPointF> tmp_pts(optima.size());
			for (size_t i = 0; i < optima.size(); ++i) {
				tmp_pts[i].setX(((optima[i]->variable()[0] - boundary[0].first) / (boundary[0].second - boundary[0].first) * 2 - 1) * ms_lenth);
				tmp_pts[i].setY((1 - (optima[i]->fitness() - range_fit.first) / (range_fit.second - range_fit.first) * 2) * ms_lenth / 2);
			}
			auto new_pts = new Points2d(tmp_pts, 3, m_color_pareto_set);
			m_optima.emplace_back(new_pts);
		}
	}

	void FitLndscp2d::setOptimaVisible(bool flag) {
		for (auto &opt : m_optima)
			opt->setVisible(flag);
	}

	void FitLndscp2d::addOptima(QGraphicsScene *scene) {
		for (auto &opt : m_optima)
			scene->addItem(opt.get());
	}

	void FitLndscp2d::removeOptima(QGraphicsScene *scene) {
		for (auto &opt : m_optima)
			scene->removeItem(opt.get());
	}

	void FitLndscp2d::setOptimaFound(bool flag) {
		for (auto& opt : m_optima)
			opt->setColor(flag ? m_color_fnd : m_color_not_fnd);
	}

	void FitLndscp2d::setOptimaFound(const std::vector<bool> &flag) {
		for (size_t i = 0; i < m_optima.size(); i++) {
			if (flag[i])
				m_optima[i]->setColor(m_color_fnd);
			else
				m_optima[i]->setColor(m_color_not_fnd);
		}
	}

	const qreal VarSpace2d::ms_lenth(100);

	VarSpace2d::VarSpace2d() :
		m_color_fnd(Qt::black),
		m_color_not_fnd(Qt::gray),
		m_color_pareto_set(Qt::red),
		m_font_title("Courier", 4, QFont::Bold) {}

	void VarSpace2d::updateBoundary(const std::vector<std::pair<ofec::Real, ofec::Real>> &range) {
		m_boundary.reset(new Rect2d(-ms_lenth, -ms_lenth, ms_lenth * 2, ms_lenth * 2, Qt::transparent, Qt::DotLine));
		m_space_top.reset(new Rect2d(-ms_lenth, -1.25 * ms_lenth, ms_lenth * 2, ms_lenth * 0.25, Qt::transparent, Qt::NoPen));
		m_space_right.reset(new Rect2d(ms_lenth, -ms_lenth, ms_lenth * 0.2, ms_lenth * 2, Qt::transparent, Qt::NoPen));
		m_axis_x1.reset(new Arrow2d(-ms_lenth, ms_lenth, -ms_lenth, -1.15 * ms_lenth));
		m_axis_x2.reset(new Arrow2d(-ms_lenth, ms_lenth, 1.15 * ms_lenth, ms_lenth));
		m_title_bottom.reset(new Text2d(-ms_lenth, ms_lenth, ms_lenth * 2, ms_lenth * 0.2, "x1", m_font_title, Qt::black, false));
		m_title_left.reset(new Text2d(-ms_lenth, -1.2 * ms_lenth, ms_lenth * 2, ms_lenth * 0.2, "x2", m_font_title, Qt::black, false, -90));
	}

	void VarSpace2d::addBoundary(QGraphicsScene *scene) {
		scene->addItem(m_boundary.get());
		scene->addItem(m_space_top.get());
		scene->addItem(m_space_right.get());
		scene->addItem(m_axis_x1.get());
		scene->addItem(m_axis_x2.get());
		scene->addItem(m_title_bottom.get());
		scene->addItem(m_title_left.get());
	}

	void VarSpace2d::removeBoundary(QGraphicsScene *scene) {
		scene->removeItem(m_boundary.get());
		scene->removeItem(m_space_top.get());
		scene->removeItem(m_space_right.get());
		scene->removeItem(m_axis_x1.get());
		scene->removeItem(m_axis_x2.get());
		scene->removeItem(m_title_bottom.get());
		scene->removeItem(m_title_left.get());
	}

	void VarSpace2d::updateOptima(
		const std::vector<std::unique_ptr<ofec::Individual<>>> &optima,
		const std::vector<std::pair<ofec::Real, ofec::Real>> &range, bool is_pareto_set)
	{
		m_optima.clear();
		if (!is_pareto_set) {
			std::vector<QPointF> tmp_pts(1);
			for (const auto &optimum : optima) {
				tmp_pts[0].setX(((optimum->variable()[0] - range[0].first) / (range[0].second - range[0].first) * 2 - 1) * ms_lenth);
				tmp_pts[0].setY((1 - (optimum->variable()[1] - range[1].first) / (range[1].second - range[1].first) * 2) * ms_lenth);
				auto new_pts = new Points2d(tmp_pts, 6, m_color_not_fnd);
				m_optima.emplace_back(new_pts);
			}
		}
		else {
			std::vector<QPointF> tmp_pts(optima.size());
			for (size_t i = 0; i < optima.size(); i++) {
				tmp_pts[i].setX(((optima[i]->variable()[0] - range[0].first) / (range[0].second - range[0].first) * 2 - 1) * ms_lenth);
				tmp_pts[i].setY((1 - (optima[i]->variable()[1] - range[1].first) / (range[1].second - range[1].first) * 2) * ms_lenth);
			}
			auto new_pts = new Points2d(tmp_pts, 3, m_color_pareto_set);
			m_optima.emplace_back(new_pts);
		}
	}

	void VarSpace2d::setOptimaVisible(bool flag) {
		for (auto &opt : m_optima)
			opt->setVisible(flag);
	}

	void VarSpace2d::addOptima(QGraphicsScene *scene) {
		for (auto &opt : m_optima)
			scene->addItem(opt.get());
	}

	void VarSpace2d::removeOptima(QGraphicsScene *scene) {
		for (auto &opt : m_optima)
			scene->removeItem(opt.get());
	}

	void VarSpace2d::setOptimaFound(bool flag) {
		for (auto& opt : m_optima)
			opt->setColor(flag ? m_color_fnd : m_color_not_fnd);
	}

	void VarSpace2d::setOptimaFound(const std::vector<bool> &flag) {
		for (size_t i = 0; i < m_optima.size(); i++) {
			if (flag[i])
				m_optima[i]->setColor(m_color_fnd);
			else
				m_optima[i]->setColor(m_color_not_fnd);
		}
	}
}