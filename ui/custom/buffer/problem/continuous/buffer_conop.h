//AddCustom BufferConOP "" ConOP

#ifndef OFEC_DEMO_BUFFER_COP_H
#define OFEC_DEMO_BUFFER_COP_H

#include "../../../../core/buffer/buffer_pro.h"
#include "items_2d_conop.h"
#include "items_3d_conop.h"
#include <core/algorithm/individual.h>
#include <core/problem/continuous/continuous.h>
#include <vtkLookupTable.h>

#include "../../../../core/buffer/graphic_view/model_view_3d.h"

namespace ofec_demo {
	#define BUFFER_CONOP dynamic_cast<BufferConOP*>(m_buffer_pro)
	/* Buffer of Continuous Optimization Problem */
	class BufferConOP : public BufferPro {
	protected:
		std::vector<std::unique_ptr<ofec::Individual<>>> m_samples, m_optima;
		std::pair<ofec::Real, ofec::Real> m_range_fit;

	private:
		std::unique_ptr<GraphicView3d> m_fit_lndscp_view_3d;		/* 3D view for fit landscape (2D ConOP)*/
		std::unique_ptr<GraphicView2d> m_fit_lndscp_view_2d;		/* 2D view for fit landscape (1D ConOP) */
		std::unique_ptr<GraphicView2d> m_var_space_view_2d;			/* 2D view for decision space (2D ConOP) */
		std::unique_ptr<GraphicView3d> m_var_space_view_3d;			/* 3D view for decision space (3D ConOP) */

		//std::unique_ptr<ModelView3d> m_pikachu_view_3d;

		struct BufferDatum {
			std::unique_ptr<FitLndscp3d> fit_lndscp_3d;	 	/* 3D items for fit landscape (2D ConOP)*/
			std::unique_ptr<FitLndscp2d> fit_lndscp_2d;	 	/* 2D items for fit landscape (1D ConOP) */
			std::unique_ptr<VarSpace2d> var_space_2d;	 	/* 2D items for decision space (2D ConOP) */
			std::unique_ptr<VarSpace3d> var_space_3d;	 	/* 3D items for decision space (2D ConOP) */
			std::vector<std::pair<ofec::Real, ofec::Real>> range_var;
		};
		std::map<int, std::unique_ptr<BufferDatum>> m_buffer_data;
		BufferDatum *m_cur_datum;

	public:
		BufferConOP(GraphicsArea *graphic_dock_set, int id_pro);
		virtual ~BufferConOP();

		void initializeGraphicViews() override;
		bool graphicViewsInitialized() override;
		void appendBuffer(int max_buffer_id) override;
		void popupBuffer(int buffer_id) override;
		void updateGraphicViews(int buffer_id) override;
		void updateViews() override;
		void setOptimaVisible(bool flag) override;

		const BufferDatum& getDatatu(int id_pro_buffer)const
		{
			return *m_buffer_data.at(id_pro_buffer);
		}
		QGraphicsScene* fitLndscp2dScene() { return m_fit_lndscp_view_2d->scene(); }
		vtkRenderer* fitLndscp3dRenderer() { return m_fit_lndscp_view_3d->renderer(); }
		QGraphicsScene* varSpace2dScene() { return m_var_space_view_2d->scene(); }
		vtkRenderer* varSpace3dRenderer() { return m_var_space_view_3d->renderer(); }

		void setOptVarsFound(int id_buffer, bool flag);
		void setOptVarsFound(int id_buffer, const std::vector<bool> &flag);
		const std::pair<ofec::Real, ofec::Real> &rangeFit() const { return m_range_fit; }
		const std::vector<std::pair<ofec::Real, ofec::Real>>& rangeVar(int id_pro_buffer) const { return m_buffer_data.at(id_pro_buffer)->range_var; }
		vtkLookupTable *fitLndscpColorLUT() { return m_cur_datum->fit_lndscp_3d->getFitLUT(); }
		void removeItemsVarSpaceView2d();
		void addItemsVarSpaceView2d();

	private:
		void updateSample();
		void updateOptima();
		void updateFitLndscap2d(BufferDatum *buffer_datum);
		void updateFitLndscap3d(BufferDatum *buffer_datum);
		void updateVarSpace2d(BufferDatum *buffer_datum);
		void updateVarSpace3d(BufferDatum *buffer_datum);
		void removeCurDatum();
		void addNewDatum(int buffer_id);
		void evaluateSamples(std::vector<int> ids);
	};
}

#endif // !OFEC_DEMO_BUFFER_COP_H