#ifndef OFEC_DEMO_CNSTRCT_CSIWDN_H
#define OFEC_DEMO_CNSTRCT_CSIWDN_H

#include "../../../../../core/parameter/cnstrct.h"

namespace ofec_demo {
	class CnstrctCSIWDN : public Cnstrct {
	public:
		CnstrctCSIWDN(const QString& alg_name, const std::set<ofec::ProTag>& pro_tags);
	protected:
		void updateLayout_() override;
	};
}

#endif // !OFEC_DEMO_CNSTRCT_CSIWDN_H
