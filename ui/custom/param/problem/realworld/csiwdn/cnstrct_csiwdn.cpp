#include "cnstrct_csiwdn.h"
#include <core/global.h>
#include "../../../../../core/parameter/widgets/int_slider.h"
#include "../../../../../core/parameter/widgets/drop_down.h"
#include <list>
#include <string>
#include <QDirIterator>
#include <QStringList>

namespace ofec_demo {
	CnstrctCSIWDN::CnstrctCSIWDN(const QString& pro_name, const std::set<ofec::ProTag>& pro_tags) :
		Cnstrct(pro_name, pro_tags) {}

	void CnstrctCSIWDN::updateLayout_() {
		QString OFEC_path(OFEC_DIR);
		QStringList file_filters;
		QDir::Filters type_filters = QDir::NoDotAndDotDot | QDir::Files;

		std::list<std::string> case_list;
		file_filters << "*.txt";
		{
			QDirIterator it(OFEC_path + "/instance/problem/realworld/csiwdn/data/input", file_filters, type_filters);
			while (it.hasNext()) {
				QFileInfo f(it.next());
				case_list.push_back(f.baseName().toStdString());
			}
		}

		std::list<std::string> map_list;
		file_filters.clear();
		file_filters << "*.inp";
		{
			QDirIterator it(OFEC_path + "/instance/problem/realworld/csiwdn/data/map", file_filters, type_filters);
			while (it.hasNext()) {
				QFileInfo f(it.next());
				map_list.push_back(f.baseName().toStdString());
			}
		}

		m_layout->addWidget(new DropDown(ParamType::pro, "dataFile1", case_list, "Net2_97_case2_dynamic"));
		m_layout->addWidget(new DropDown(ParamType::pro, "dataFile2", map_list, "Net2"));
	}
}