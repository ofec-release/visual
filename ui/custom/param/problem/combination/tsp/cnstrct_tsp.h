#ifndef OFEC_DEMO_CNSTRCT_TSP_H
#define OFEC_DEMO_CNSTRCT_TSP_H

#include "../../../../../core/parameter/cnstrct.h"

namespace ofec_demo {
	class CnstrctTSP : public Cnstrct {
	public:
		CnstrctTSP(const QString &pro_name, const std::set<ofec::ProTag> &pro_tags);
	protected:
		void updateLayout_() override;
	};
}

#endif // !OFEC_DEMO_CNSTRCT_TSP_H
