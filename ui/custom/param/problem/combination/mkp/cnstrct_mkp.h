#ifndef OFEC_DEMO_CNSTRCT_MKP_H
#define OFEC_DEMO_CNSTRCT_MKP_H

#include "../../../../../core/parameter/cnstrct.h"

namespace ofec_demo {
	class CnstrctMKP : public Cnstrct {
	public:
		CnstrctMKP(const QString &pro_name, const std::set<ofec::ProTag> &pro_tags);
	protected:
		void updateLayout_() override;
	};
}

#endif // !OFEC_DEMO_CNSTRCT_MKP_H
