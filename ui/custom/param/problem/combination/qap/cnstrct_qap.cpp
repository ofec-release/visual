#include "cnstrct_qap.h"
#include "../../../../../core/parameter/widgets/drop_down.h"
#include <list>
#include <string>
#include <QDirIterator>
#include <QStringList>

namespace ofec_demo {
	CnstrctQAP::CnstrctQAP(const QString &pro_name, const std::set<ofec::ProTag> &pro_tags) :
		Cnstrct(pro_name, pro_tags) {}

	void CnstrctQAP::updateLayout_() {
		QString OFEC_path(OFEC_DIR);
		QStringList file_filters;
		file_filters << "*.qap";
		QDir::Filters type_filters = QDir::NoDotAndDotDot | QDir::Files;
		QDirIterator it(OFEC_path + "/instance/problem/combination/quadratic_assignment/data", file_filters, type_filters);
		std::list<std::string> map_list;
		while (it.hasNext()) {
			QFileInfo f(it.next());
			map_list.push_back(f.baseName().toStdString());
		}
		m_layout->addWidget(new DropDown(ParamType::pro, "dataFile1", map_list, map_list.front()));
	}
}