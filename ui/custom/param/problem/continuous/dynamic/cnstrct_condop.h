#ifndef OFEC_DEMO_CNSTRCT_DCOP_H
#define OFEC_DEMO_CNSTRCT_DCOP_H

#include "../cnstrct_conop.h"

namespace ofec_demo {
	class CnstrctConDOP : public CnstrctConOP {
	public:
		CnstrctConDOP(const QString &pro_name, const std::set<ofec::ProTag> &pro_tags);
	protected:
		void updateLayout_() override;
	};
}

#endif // !OFEC_DEMO_CNSTRCT_DCOP_H
