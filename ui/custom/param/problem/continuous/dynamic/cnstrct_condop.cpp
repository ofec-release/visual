#include "cnstrct_condop.h"
#include "../../../../../core/parameter/widgets/int_slider.h"
#include "../../../../../core/parameter/widgets/double_slider.h"

namespace ofec_demo {
	CnstrctConDOP::CnstrctConDOP(const QString &pro_name, const std::set<ofec::ProTag> &pro_tags) :
		CnstrctConOP(pro_name, pro_tags) {}
	
	void CnstrctConDOP::updateLayout_() {
		m_layout->addWidget(new IntSlider(ParamType::pro, "changeFre", 500, 100000, 5000, 500));
		m_layout->addWidget(new IntSlider(ParamType::pro, "number of variables", 1, 10, 2, 1));
		if (m_name == "Moving-Peaks") {
			m_layout->addWidget(new IntSlider(ParamType::pro, "numPeak", 1, 200, 10, 1));
			m_layout->addWidget(new DoubleSlider(ParamType::pro, "shiftLength", 0, 20, 1, 0.01));
			m_layout->addWidget(new DoubleSlider(ParamType::pro, "changePeakRatio", 0, 1, 1, 0.01));
		}
	}
}