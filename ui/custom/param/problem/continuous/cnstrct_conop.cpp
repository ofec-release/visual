#include "cnstrct_conop.h"
#include <core/global.h>
#include "../../../../core/parameter/widgets/int_slider.h"

namespace ofec_demo {
	CnstrctConOP::CnstrctConOP(const QString &pro_name, const std::set<ofec::ProTag> &pro_tags) :
		Cnstrct(pro_name, pro_tags) {}

	void CnstrctConOP::updateLayout_() {
		m_layout->addWidget(new IntSlider(ParamType::pro, "number of variables", 1, 20, 2, 1));
		if (m_pro_tags.count(ofec::ProTag::kMOP)) {
			m_layout->addWidget(new IntSlider(ParamType::pro, "number of objectives", 2, 20, 2, 1));
			if (m_name.contains("WFG")) {
				m_layout->addWidget(new IntSlider(ParamType::pro, "k factor", 1, 20, 1, 1));
				m_layout->addWidget(new IntSlider(ParamType::pro, "l factor", 1, 20, 1, 1));
			}
		}
	}
}