#ifndef OFEC_DEMO_PARAM_CONOP_H
#define OFEC_DEMO_PARAM_CONOP_H

#include "../../../../core/parameter/cnstrct.h"

namespace ofec_demo {
	class CnstrctConOP : public Cnstrct {
	public:
		CnstrctConOP(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags);
	protected:
		void updateLayout_() override;
	};
}

#endif // !OFEC_DEMO_PARAM_CONOP_H
