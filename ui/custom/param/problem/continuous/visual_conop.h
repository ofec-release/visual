#ifndef OFEC_DEMO_CONOP_VISUAL_H
#define OFEC_DEMO_CONOP_VISUAL_H

#include "../../../../core/parameter/visual_pro.h"

namespace ofec_demo {
	class VisualConOP : public VisualPro {
		Q_OBJECT
	public:
		static std::atomic<int> ms_num_div;
	public:
		VisualConOP(const QString &name, const std::set<ofec::ProTag> &pro_tags);
	protected:
		void updateLayout_() override;
	private slots:
		void onNumDivChanged(int id);
	};
}

#endif //!OFEC_DEMO_CONOP_VISUAL_H