#include "visual_conop.h"
#include <QLabel>
#include <QSlider>
#include <QSpinBox>
#include <QHBoxLayout>

namespace ofec_demo {
	std::atomic<int> VisualConOP::ms_num_div = 50;

	VisualConOP::VisualConOP(const QString &name, const std::set<ofec::ProTag> &pro_tags) : 
		VisualPro(name, pro_tags) {}

	void VisualConOP::updateLayout_() {
		VisualPro::updateLayout_();

		int min = 10, max = 200, step = 1;
		if (ms_num_div < min || ms_num_div > max)
			ms_num_div = min;

		auto slider = new QSlider(Qt::Horizontal);
		slider->setRange(min, max);
		slider->setValue(ms_num_div);
		slider->setSingleStep(step);

		auto spin_box = new QSpinBox;
		spin_box->setRange(min, max);
		spin_box->setValue(ms_num_div);
		spin_box->setSingleStep(step);
		spin_box->setFocusPolicy(Qt::ClickFocus);

		connect(slider, &QSlider::valueChanged, spin_box, &QSpinBox::setValue);
		connect(spin_box, QOverload<int>::of(&QSpinBox::valueChanged), slider, &QSlider::setValue);
		connect(slider, &QSlider::valueChanged, this, &VisualConOP::onNumDivChanged);

		auto HBox = new QHBoxLayout;
		HBox->addWidget(slider);
		HBox->addWidget(spin_box);
#ifdef Q_OS_MACOS
		HBox->setSpacing(0);
#else
		HBox->setSpacing(5);
#endif
		HBox->setContentsMargins(0, 0, 0, 0);

		auto VBox = new QVBoxLayout;
		VBox->addWidget(new QLabel("Number of divisions"));
		VBox->addLayout(HBox);
#ifdef Q_OS_MACOS
		VBox->setSpacing(0);
		VBox->setContentsMargins(0, 0, 0, 0);
#else
		VBox->setSpacing(2);
		VBox->setContentsMargins(2, 2, 2, 2);
#endif

		m_layout->addLayout(VBox);
	}

	void VisualConOP::onNumDivChanged(int val) {
		ms_num_div = val;
	}
}