#include "../../core/global_ui.h"
#include "../../core/main_window.h"
#include <core/global.h>

#include "../../core/parameter/visual_pro.h"
#include "problem/continuous/cnstrct_conop.h"
#include "problem/continuous/visual_conop.h"
#include "problem/continuous/dynamic/cnstrct_condop.h"
#include "problem/combination/tsp/cnstrct_tsp.h"
#include "problem/combination/qap/cnstrct_qap.h"
#include "problem/combination/mkp/cnstrct_mkp.h"
#include "problem/realworld/csiwdn/cnstrct_csiwdn.h"

namespace ofec_demo {
	void MainWindow::updateProParamWidget() {
		auto name = QString::fromStdString(std::get<std::string>(g_params[ParamType::pro].at("problem name")));
        const auto &pro_tags = ofec::g_reg_problem.get().at(name.toStdString()).second;
		Cnstrct *cnstrct = nullptr;
		VisualPro *visual = nullptr;

		if (pro_tags.count(ofec::ProTag::kConOP) > 0 && pro_tags.count(ofec::ProTag::kDOP) > 0)
			cnstrct = new CnstrctConDOP(name, pro_tags);
		else if (pro_tags.count(ofec::ProTag::kConOP) > 0)
			cnstrct = new CnstrctConOP(name, pro_tags);	
		else if (pro_tags.count(ofec::ProTag::kTSP) > 0)
			cnstrct = new CnstrctTSP(name, pro_tags);
		else if (pro_tags.count(ofec::ProTag::kQAP) > 0)
			cnstrct = new CnstrctQAP(name, pro_tags);
		else if (pro_tags.count(ofec::ProTag::kMKP) > 0)
			cnstrct = new CnstrctMKP(name, pro_tags);
		else if (pro_tags.count(ofec::ProTag::kOneMax) > 0)
			cnstrct = new CnstrctConOP(name, pro_tags);
		else if (pro_tags.count(ofec::ProTag::kCSIWDN) > 0)
			cnstrct = new CnstrctCSIWDN(name, pro_tags);
		else
			cnstrct = new Cnstrct(name, pro_tags);

		if (pro_tags.count(ofec::ProTag::kConOP) > 0)
			visual = new VisualConOP(name, pro_tags);
		else 
			visual = new VisualPro(name, pro_tags);

		cnstrct->updateLayout();
		visual->updateLayout();
		m_pro_param_dock->updateConstruction(cnstrct);
		m_pro_param_dock->updateVisualization(visual);
		m_pro_param_dock->switchConstruction();
    }
}