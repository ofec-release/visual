#include "../../core/global_ui.h"
#include "../../core/main_window.h"
#include <core/global.h>

#include "../../core/parameter/visual_alg.h"
#include "algorithm/cnstrct_alg.h"
#include "algorithm/continuous/spae/cnstrct_spae.h"
#include "algorithm/continuous/spae/visual_spae.h"
#include "algorithm/combination/construction_aco.h"
#include "algorithm/continuous/spso/cnstrct_spso.h"
#include "algorithm/continuous/samo/cnstrct_samo.h"
#include "algorithm/continuous/cpso/cnstrct_cpso.h"
#include "algorithm/realworld/csiwdn/cnstrct_csiwdn_alg.h"

namespace ofec_demo {
    void MainWindow::updateAlgParamWidget() {
        auto name = QString::fromStdString(std::get<std::string>(g_params[ParamType::alg].at("algorithm name")));
        const auto &pro_tags = ofec::g_reg_algorithm.get().at(name.toStdString()).second;
        Cnstrct *cnstrct = nullptr;
        VisualAlg *visual = nullptr;

        if (pro_tags.count(ofec::ProTag::kConOP) > 0 && name.contains("SPAE"))
            cnstrct = new CnstrctSPAE(name, pro_tags);
        else if (pro_tags.count(ofec::ProTag::kConOP) > 0 && name.contains("SAMO"))
            cnstrct = new CnstrctSAMO(name, pro_tags);
        else if (pro_tags.count(ofec::ProTag::kConOP) > 0 && name.contains("SPSO"))
            cnstrct = new CnstrctSPSO(name, pro_tags);
        else if (pro_tags.count(ofec::ProTag::kConOP) > 0 && name.contains("CPSO")/* || name.contains("CPSOR")*/)
            cnstrct = new CnstrctCPSO(name, pro_tags);
        else if (pro_tags.count(ofec::ProTag::kConOP) > 0)
            cnstrct = new CnstrctConOEA(name, pro_tags);
        else if (name=="AS_TSP"||name=="ACS_TSP"||name=="MMAS_TSP"||name=="PACO_TSP")
            cnstrct = new CnstrctACO(name, pro_tags);

        else
            cnstrct = new CnstrctAlg(name, pro_tags);

        if (pro_tags.count(ofec::ProTag::kConOP) > 0 && name.contains("SPAE"))
            visual = new VisualSPAE(name, pro_tags);
        else
            visual = new VisualAlg(name, pro_tags);

        cnstrct->updateLayout();    
        visual->updateLayout();    
        m_alg_param_dock->updateConstruction(cnstrct);
        m_alg_param_dock->updateVisualization(visual);
        m_alg_param_dock->switchConstruction();
    }
}