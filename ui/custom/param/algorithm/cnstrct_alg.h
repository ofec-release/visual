#ifndef OFEC_DEMO_CNSTRCT_ALG_H
#define OFEC_DEMO_CNSTRCT_ALG_H

#include "../../../core/parameter/cnstrct.h"

namespace ofec_demo {
	class CnstrctAlg : public Cnstrct {
	public:
		CnstrctAlg(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags);
	protected:
		void updateLayout_() override;
	};
}

#endif // !OFEC_DEMO_CNSTRCT_ALG_H
