#include "cnstrct_csiwdn_alg.h"
#include "../../../../../core/parameter/widgets/int_slider.h"
#include <QLabel>

namespace ofec_demo {
	CnstrctAlgCSIWDN::CnstrctAlgCSIWDN(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags) :
		CnstrctAlg(alg_name, pro_tags) {}

	void CnstrctAlgCSIWDN::updateLayout_() {
		CnstrctAlg::updateLayout_();
		if (m_name.contains("AMP"))
			addParamsAMP();
	}

	void CnstrctAlgCSIWDN::addParamsAMP() {
		m_layout->addWidget(new QLabel("[AMP]"), 0, Qt::AlignHCenter);
		m_layout->addWidget(new IntSlider(ParamType::alg, "number of SubPopulations", 1, 100, 5, 1));
		m_layout->addWidget(new IntSlider(ParamType::alg, "subpopulation size", 5, 200, 10, 5));
		m_layout->addWidget(new IntSlider(ParamType::alg, "alpha", 1, 10, 2, 1));
		m_layout->addWidget(new IntSlider(ParamType::alg, "beta", 1, 10, 2, 1));
		m_layout->addWidget(new IntSlider(ParamType::alg, "maximum evaluations", 5000, 200000, 100000, 500));
	}
}