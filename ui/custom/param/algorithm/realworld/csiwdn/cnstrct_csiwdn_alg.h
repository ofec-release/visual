#ifndef OFEC_DEMO_CNSTRCT_CSIWDN_ALG_H
#define OFEC_DEMO_CNSTRCT_CSIWDN_ALG_H

#include "../../cnstrct_alg.h"

namespace ofec_demo {
	class CnstrctAlgCSIWDN : public CnstrctAlg {
	public:
		CnstrctAlgCSIWDN(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags);
	protected:
		void updateLayout_() override;
		void addParamsAMP();
	};
}

#endif // !OFEC_DEMO_CNSTRCT_CSIWDN_ALG_H
