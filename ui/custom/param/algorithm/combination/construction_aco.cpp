#include "construction_aco.h"
#include <QLabel.h>
#include "../../../../core/parameter/widgets/int_slider.h"

ofec_demo::CnstrctACO::CnstrctACO(const QString& alg_name, const std::set<ofec::ProTag>& pro_tags)
	:CnstrctAlg(alg_name, pro_tags)
{
}

void ofec_demo::CnstrctACO::updateLayout_()
{
	
	CnstrctAlg::updateLayout_();
	m_layout->addWidget(new QLabel("[ACO]"), 0, Qt::AlignHCenter);
	m_layout->addWidget(new IntSlider(ParamType::alg, "population size", 2, 2000, 100, 1));
}
