#ifndef OFEC_DEMO_CNSTRCT_ACO_H
#define OFEC_DEMO_CNSTRCT_ACO_H


#include "../cnstrct_alg.h"
namespace ofec_demo {
	class CnstrctACO : public CnstrctAlg {
	public:
		CnstrctACO(const QString& alg_name, const std::set<ofec::ProTag>& pro_tags);
	protected:
		void updateLayout_() override;
	};
}

#endif // !OFEC_DEMO_CNSTRCT_ALG_H
