#include "cnstrct_alg.h"
#include "../../../core/parameter/widgets/fraction_number.h"
#include "../../../core/parameter/widgets/check_box.h"
#include <QLabel>

namespace ofec_demo {
	CnstrctAlg::CnstrctAlg(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags) :
		Cnstrct(alg_name, pro_tags) {}

	void CnstrctAlg::updateLayout_() {
		m_layout->addWidget(new QLabel("[Algorithm]"), 0, Qt::AlignHCenter);
		m_layout->addWidget(new FractionNumber(ParamType::alg, "random seed", 25, 50, 200));
		m_layout->addWidget(new CheckBox(ParamType::alg, "keep candidates updated", true));
	}
}