#ifndef OFEC_DEMO_CNSTRCT_CONOEA_H
#define OFEC_DEMO_CNSTRCT_CONOEA_H

#include "../cnstrct_alg.h"

namespace ofec_demo {
	class CnstrctConOEA : public CnstrctAlg {
	public:
		CnstrctConOEA(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags);
	protected:
		void updateLayout_() override;
		void addParamsDE();
		void addParamsPSO();
	};
}

#endif // !OFEC_DEMO_CNSTRCT_CONOEA_H
