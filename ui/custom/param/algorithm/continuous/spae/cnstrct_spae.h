#ifndef OFEC_DEMO_CNSTRCT_SPAE_H
#define OFEC_DEMO_CNSTRCT_SPAE_H

#include "../cnstrct_conoea.h"

namespace ofec_demo {
	class CnstrctSPAE : public CnstrctConOEA {
	public:
		CnstrctSPAE(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags);
	protected:
		void updateLayout_() override;
	};
}

#endif // !OFEC_DEMO_CNSTRCT_SPAE_H
