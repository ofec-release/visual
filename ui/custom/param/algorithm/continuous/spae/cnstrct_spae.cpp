#include "cnstrct_spae.h"
#include "../../../../../core/parameter/widgets/int_slider.h"
#include "../../../../../core/parameter/widgets/double_slider.h"
#include "../../../../../core/parameter/widgets/check_box.h"
#include <QLabel>

namespace ofec_demo {
	CnstrctSPAE::CnstrctSPAE(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags) :
		CnstrctConOEA(alg_name, pro_tags) {}

	void CnstrctSPAE::updateLayout_() {
		CnstrctAlg::updateLayout_();
		m_layout->addWidget(new QLabel("[SPAE]"), 0, Qt::AlignHCenter);
		m_layout->addWidget(new IntSlider(ParamType::alg, "subpopulation size", 5, 100, 15, 1));
		m_layout->addWidget(new IntSlider(ParamType::alg, "initial number of subspaces", 1, 5000, 250, 1));
		addParamsDE();
		addParamsPSO();
	}
}