#ifndef OFEC_DEMO_SPAE_SSP_INFO_H
#define OFEC_DEMO_SPAE_SSP_INFO_H

#include "../../../../../core/parameter/visual_alg.h"
#include <QButtonGroup>

namespace ofec_demo {
	class VisualSPAE : public VisualAlg {
		Q_OBJECT
	public:
		static std::atomic<int> ms_show_color_ssp; // -1:none, 0:affiliated attractor, 1:fit
		static std::atomic<int> ms_show_label_ssp;  // -1:none, 0:index, 1:fit, 2:freq, 3: diff
		static std::atomic<int> ms_show_label_bsn;  // -1:none, 0:index, 1:potential, 2:fit, 3:freq, 4:diff, 5:div
		static std::atomic<bool> ms_show_typ_inds;

	public:
		VisualSPAE(const QString &name, const std::set<ofec::ProTag> &pro_tags);

	protected:
		void updateLayout_() override;

	private:
		QButtonGroup *m_color_ssp_btn_group;
		void addColorSubspace();

		QButtonGroup *m_label_ssp_btn_group;
		void addLabelSubspace();

		QButtonGroup *m_label_bsn_btn_group;
		void addLabelBsnAtrctr();

		void addShowTypInds();

	private slots:
		void onShowColorSubspaceCheckBoxChanged(bool checked);
		void onShowColorSubspaceButtonGroupChanged(int id, bool checked);

		void onShowLabelSubspaceCheckBoxChanged(bool checked);
		void onShowLabelSubspaceButtonGroupChanged(int id, bool checked);

		void onShowLabelBsnAtrctrCheckBoxChanged(bool checked);
		void onShowLabelBsnAtrctrButtonGroupChanged(int id, bool checked);

		void onShowTypIndsCheckBoxChanged(bool checked);
	
	};
}

#endif //!OFEC_DEMO_SPAE_SSP_INFO_H