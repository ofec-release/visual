#include "visual_spae.h"
#include <QGroupBox>
#include <QRadioButton>
#include <QGridLayout>
#include <QCheckBox>
#include <QLabel>
#include "../../../../../core/global_ui.h"

namespace ofec_demo {
	std::atomic<int> VisualSPAE::ms_show_color_ssp = 0; // -1:none, 0:affiliated attractor, 1:fit
	std::atomic<int> VisualSPAE::ms_show_label_ssp = -1;  // -1:none, 0:index, 1:fit, 2:freq, 3: diff
	std::atomic<int> VisualSPAE::ms_show_label_bsn = -1;  // -1:none, 0:index, 1:potential, 2:fit, 3:freq, 4:number of modals 
	std::atomic<bool> VisualSPAE::ms_show_typ_inds = true;

	VisualSPAE::VisualSPAE(const QString &name, const std::set<ofec::ProTag> &pro_tags) :
		VisualAlg(name, pro_tags),
		m_color_ssp_btn_group(nullptr),
		m_label_ssp_btn_group(nullptr),
		m_label_bsn_btn_group(nullptr) {}

	void VisualSPAE::updateLayout_() {
		VisualAlg::updateLayout_();
		addColorSubspace();
		addLabelSubspace();
		addLabelBsnAtrctr();
		addShowTypInds();
	}

	void VisualSPAE::addColorSubspace() {
		auto check_box = new QCheckBox("show color");
		check_box->setChecked(ms_show_color_ssp > -1);
		connect(check_box, &QCheckBox::stateChanged, this, &VisualSPAE::onShowColorSubspaceCheckBoxChanged);
		
		QStringList contents;
		contents << "affiliated attractor" << "fitness";
		QList<QRadioButton*> radios;
		for (size_t i = 0; i < contents.size(); ++i)
			radios << new QRadioButton(contents.at(i));
		m_color_ssp_btn_group = new QButtonGroup;
		for (size_t i = 0; i < radios.size(); i++)
			m_color_ssp_btn_group->addButton(radios.at(i), i);
		m_color_ssp_btn_group->setExclusive(true);
		m_color_ssp_btn_group->checkedId();
		connect(m_color_ssp_btn_group, &QButtonGroup::idToggled, this, &VisualSPAE::onShowColorSubspaceButtonGroupChanged);
		auto button_layout = new QGridLayout;
		for (size_t i = 0; i < radios.size(); ++i) {
			radios.at(i)->setEnabled(ms_show_color_ssp > -1);
			radios.at(i)->setChecked(ms_show_color_ssp == i);
			button_layout->addWidget(radios.at(i), i / 2, i % 2);
			connect(check_box, &QCheckBox::stateChanged, radios.at(i), &QRadioButton::setEnabled);
		}

		auto layout = new QVBoxLayout;		
		layout->addWidget(check_box);
		layout->addLayout(button_layout);
#ifdef Q_OS_MACOS
		layout->setSpacing(0);
		layout->setMargin(0);
#else
		layout->setSpacing(2);
		layout->setContentsMargins(2, 2, 2, 2);
#endif

		auto group = new QGroupBox("color of subspace");
		group->setLayout(layout);

		m_layout->addWidget(group);
	}

	void VisualSPAE::onShowColorSubspaceCheckBoxChanged(bool checked) {
		if (checked)
			ms_show_color_ssp = m_color_ssp_btn_group->checkedId();
		else
			ms_show_color_ssp = -1;
		g_buffer->updateViews();
	}

	void VisualSPAE::onShowColorSubspaceButtonGroupChanged(int id, bool checked) {
		if (checked) {
			ms_show_color_ssp = id;
			g_buffer->updateViews();
		}
	}

	void VisualSPAE::addLabelSubspace() {
		auto check_box = new QCheckBox("show label");
		check_box->setChecked(ms_show_label_ssp > -1);
		connect(check_box, &QCheckBox::stateChanged, this, &VisualSPAE::onShowLabelSubspaceCheckBoxChanged);

		QStringList contents;
		contents << "index" << "fitness" << "frequency" << "diffculty";
		QList<QRadioButton *> radios;
		for (size_t i = 0; i < contents.size(); ++i)
			radios << new QRadioButton(contents.at(i));
		m_label_ssp_btn_group = new QButtonGroup;
		for (size_t i = 0; i < radios.size(); i++)
			m_label_ssp_btn_group->addButton(radios.at(i), i);
		m_label_ssp_btn_group->setExclusive(true);
		connect(m_label_ssp_btn_group, &QButtonGroup::idToggled, this, &VisualSPAE::onShowLabelSubspaceButtonGroupChanged);
		auto button_layout = new QGridLayout;
		for (size_t i = 0; i < radios.size(); ++i) {
			radios.at(i)->setEnabled(ms_show_label_ssp > -1);
			radios.at(i)->setChecked(ms_show_label_ssp == i);
			button_layout->addWidget(radios.at(i), i / 2, i % 2);
			connect(check_box, &QCheckBox::stateChanged, radios.at(i), &QRadioButton::setEnabled);
		}

		auto layout = new QVBoxLayout;
		layout->addWidget(check_box);
		layout->addLayout(button_layout);
#ifdef Q_OS_MACOS
		layout->setSpacing(0);
		layout->setContentsMargins(0, 0, 0, 0);
#else
		layout->setSpacing(2);
		layout->setContentsMargins(2, 2, 2, 2);
#endif

		auto group = new QGroupBox("label of subspace");
		group->setLayout(layout);

		m_layout->addWidget(group);
	}

	void VisualSPAE::onShowLabelSubspaceCheckBoxChanged(bool checked) {
		if (checked)
			ms_show_label_ssp = m_label_ssp_btn_group->checkedId();
		else
			ms_show_label_ssp = -1;
		g_buffer->updateViews();
	}

	void VisualSPAE::onShowLabelSubspaceButtonGroupChanged(int id, bool checked) {
		if (checked) {
			ms_show_label_ssp = id;
			g_buffer->updateViews();
		}
	}

	void VisualSPAE::addLabelBsnAtrctr() {
		auto check_box = new QCheckBox("show label");
		check_box->setChecked(ms_show_label_bsn > -1);
		connect(check_box, &QCheckBox::stateChanged, this, &VisualSPAE::onShowLabelBsnAtrctrCheckBoxChanged);

		QStringList contents;
		contents << "index" << "potential" << "fitness" << "frequency" << "difficulty" << "diversity";
		QList<QRadioButton *> radios;
		for (size_t i = 0; i < contents.size(); ++i)
			radios << new QRadioButton(contents.at(i));
		m_label_bsn_btn_group = new QButtonGroup;
		for (size_t i = 0; i < radios.size(); i++)
			m_label_bsn_btn_group->addButton(radios.at(i), i);
		m_label_bsn_btn_group->setExclusive(true);
		connect(m_label_bsn_btn_group, &QButtonGroup::idToggled, this, &VisualSPAE::onShowLabelBsnAtrctrButtonGroupChanged);
		auto button_layout = new QGridLayout;
		for (size_t i = 0; i < radios.size(); ++i) {
			radios.at(i)->setEnabled(ms_show_label_bsn > -1);
			radios.at(i)->setChecked(ms_show_label_bsn == i);
			button_layout->addWidget(radios.at(i), i / 2, i % 2);
			connect(check_box, &QCheckBox::stateChanged, radios.at(i), &QRadioButton::setEnabled);
		}

		auto layout = new QVBoxLayout;
		layout->addWidget(check_box);
		layout->addLayout(button_layout);
#ifdef Q_OS_MACOS
		layout->setSpacing(0);
		layout->setContentsMargins(0, 0, 0, 0);
#else
		layout->setSpacing(2);
		layout->setContentsMargins(2, 2, 2, 2);
#endif

		auto group = new QGroupBox("label of basin of attraction");
		group->setLayout(layout);

		m_layout->addWidget(group);
	}

	void VisualSPAE::addShowTypInds() {
		auto check_box = new QCheckBox("show history individuals");
		check_box->setChecked(ms_show_typ_inds);
		connect(check_box, &QCheckBox::stateChanged, this, &VisualSPAE::onShowTypIndsCheckBoxChanged);

		auto VBox = new QVBoxLayout;
		VBox->addWidget(check_box);
#ifdef Q_OS_MACOS
		VBox->setSpacing(0);
		VBox->setContentsMargins(0, 0, 0, 0);
#else
		VBox->setSpacing(2);
		VBox->setContentsMargins(2, 2, 2, 2);
#endif

		m_layout->addLayout(VBox);
	}

	void VisualSPAE::onShowLabelBsnAtrctrCheckBoxChanged(bool checked) {
		if (checked)
			ms_show_label_bsn = m_label_bsn_btn_group->checkedId();
		else
			ms_show_label_bsn = -1;
		g_buffer->updateViews();
	}

	void VisualSPAE::onShowLabelBsnAtrctrButtonGroupChanged(int id, bool checked) {
		if (checked) {
			ms_show_label_bsn = id;
			g_buffer->updateViews();
		}
	}

	void VisualSPAE::onShowTypIndsCheckBoxChanged(bool checked) {
		if (ms_show_typ_inds != checked) {
			ms_show_typ_inds = checked;
			g_buffer->updateViews();
		}
	}

}