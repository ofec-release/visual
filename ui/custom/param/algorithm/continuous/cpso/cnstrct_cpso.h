#ifndef OFEC_DEMO_CNSTRCT_CPSO_H
#define OFEC_DEMO_CNSTRCT_CPSO_H

#include "../cnstrct_conoea.h"

namespace ofec_demo {
	class CnstrctCPSO : public CnstrctConOEA {
	public:
		CnstrctCPSO(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags);
	protected:
		void updateLayout_() override;
	protected:
		QString m_alg_name;
	};
}

#endif // !OFEC_DEMO_CNSTRCT_CPSO_H
