#include "cnstrct_cpso.h"
#include "../../../../../core/parameter/widgets/int_slider.h"
#include "../../../../../core/parameter/widgets/double_slider.h"
#include "../../../../../core/parameter/widgets/check_box.h"
#include <QLabel>

namespace ofec_demo {
	CnstrctCPSO::CnstrctCPSO(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags) :
		CnstrctConOEA(alg_name, pro_tags){
		m_alg_name = alg_name;
	}

	void CnstrctCPSO::updateLayout_() {
		CnstrctAlg::updateLayout_();
		if(m_alg_name == "CPSO"){
			m_layout->addWidget(new QLabel("[CPSO]"), 0, Qt::AlignHCenter);
			m_layout->addWidget(new IntSlider(ParamType::alg, "population size", 2, 2000, 70, 1));
			m_layout->addWidget(new IntSlider(ParamType::alg, "max subpop size", 2, 20, 3, 1));
		}
		else if(m_alg_name == "CPSOR"){
			m_layout->addWidget(new QLabel("[CPSOR]"), 0, Qt::AlignHCenter);
			m_layout->addWidget(new IntSlider(ParamType::alg, "population size", 2, 2000, 70, 1));
			m_layout->addWidget(new IntSlider(ParamType::alg, "max subpop size", 2, 20, 3, 1));
			m_layout->addWidget(new DoubleSlider(ParamType::alg, "diversity degree", 0, 1, 0.3, 0.01));
		}
		addParamsDE();
		addParamsPSO();
	}
}