#include "cnstrct_conoea.h"
#include "../../../../core/parameter/widgets/double_slider.h"
#include "../../../../core/parameter/widgets/int_slider.h"
#include "../../../../core/parameter/widgets/drop_down.h"
#include "../../../../core/parameter/widgets/exclusive_group.h"
#include <QLabel>

namespace ofec_demo {
	CnstrctConOEA::CnstrctConOEA(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags) :
		CnstrctAlg(alg_name, pro_tags) {}

	void CnstrctConOEA::updateLayout_() {
		CnstrctAlg::updateLayout_();
		m_layout->addWidget(new QLabel("[EA]"), 0, Qt::AlignHCenter);
		m_layout->addWidget(new IntSlider(ParamType::alg, "population size", 2, 2000, 100, 1));
		addParamsDE();
		addParamsPSO();
	}

	void CnstrctConOEA::addParamsDE() {
		if (m_name.contains("DE-") ||
			m_name.contains("-DE"))
		{
			m_layout->addWidget(new QLabel("[DE]"), 0, Qt::AlignHCenter);
			m_layout->addWidget(new DoubleSlider(ParamType::alg, "scaling factor", 0, 2, 0.5, 0.01));
			m_layout->addWidget(new DoubleSlider(ParamType::alg, "crossover rate", 0, 2, 0.6, 0.01));
			std::list<std::string> mutation_stategies = {
				"rand/1", "best/1", "current-to-best/1", "best/2", "rand/2",
				"rand-to-best/1", "current-to-rand/1"
			};
			m_layout->addWidget(new DropDown(ParamType::alg, "mutation strategy", mutation_stategies, 0));
			std::list<std::string> recombine_stategies = { "binomial", "exponential" };
			m_layout->addWidget(new ExclusiveGroup(ParamType::alg, "recombine stategie", recombine_stategies, 0));
		}
	}

	void CnstrctConOEA::addParamsPSO() {
		if (m_name.contains("PSO-") ||
			m_name.contains("-PSO") ||
			m_name == "SPSO07" ||
			m_name == "SPSO11")
		{
			m_layout->addWidget(new QLabel("[PSO]"), 0, Qt::AlignHCenter);
			m_layout->addWidget(new DoubleSlider(ParamType::alg, "weight", 0, 2, 0.721, 0.001));
			m_layout->addWidget(new DoubleSlider(ParamType::alg, "accelerator1", 0, 3, 1.193, 0.001));
			m_layout->addWidget(new DoubleSlider(ParamType::alg, "accelerator2", 0, 3, 1.193, 0.001));
		}
		if (m_name.contains("Ring")) {
			std::list<std::string> topologies = { "R2","R3","LHC-R2","LHC-R3" };
			m_layout->addWidget(new DropDown(ParamType::alg, "ring topology", topologies, 2));
		}
	}
}