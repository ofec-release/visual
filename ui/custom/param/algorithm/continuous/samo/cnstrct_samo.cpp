#include "cnstrct_samo.h"
#include "../../../../../core/parameter/widgets/int_slider.h"
#include "../../../../../core/parameter/widgets/double_slider.h"
#include "../../../../../core/parameter/widgets/check_box.h"
#include <QLabel>

namespace ofec_demo {
	CnstrctSAMO::CnstrctSAMO(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags) :
		CnstrctConOEA(alg_name, pro_tags) {}

	void CnstrctSAMO::updateLayout_() {
		CnstrctAlg::updateLayout_();
		m_layout->addWidget(new QLabel("[SAMO]"), 0, Qt::AlignHCenter);
		m_layout->addWidget(new IntSlider(ParamType::alg, "number of free populations",1,10000,1,1));
		addParamsDE();
		addParamsPSO();
	}
}