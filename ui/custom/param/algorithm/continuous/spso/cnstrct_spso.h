#ifndef OFEC_DEMO_CNSTRCT_SPSO_H
#define OFEC_DEMO_CNSTRCT_SPSO_H

#include "../cnstrct_conoea.h"

namespace ofec_demo {
	class CnstrctSPSO : public CnstrctConOEA {
	public:
		CnstrctSPSO(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags);
	protected:
		void updateLayout_() override;
	};
}

#endif // !OFEC_DEMO_CNSTRCT_SPAE_H
