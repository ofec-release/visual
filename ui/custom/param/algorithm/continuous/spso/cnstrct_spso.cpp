#include "cnstrct_spso.h"
#include "../../../../../core/parameter/widgets/int_slider.h"
#include "../../../../../core/parameter/widgets/double_slider.h"
#include "../../../../../core/parameter/widgets/check_box.h"
#include <QLabel>

namespace ofec_demo {
	CnstrctSPSO::CnstrctSPSO(const QString &alg_name, const std::set<ofec::ProTag> &pro_tags) :
		CnstrctConOEA(alg_name, pro_tags) {}

	void CnstrctSPSO::updateLayout_() {
		CnstrctAlg::updateLayout_();
		m_layout->addWidget(new QLabel("[SPSO]"), 0, Qt::AlignHCenter);
		m_layout->addWidget(new IntSlider(ParamType::alg, "population size", 20, 200, 100, 1));
		m_layout->addWidget(new DoubleSlider(ParamType::alg, "Separate Radius",0.,500.,100.,0.01));
		addParamsDE();
		addParamsPSO();
	}
}