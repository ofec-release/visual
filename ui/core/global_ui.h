#ifndef OFEC_DEMO_GLOBAL_H
#define OFEC_DEMO_GLOBAL_H

#include <memory>
#include "buffer/buffer.h"
#include <utility/typevar/typevar.h>
#include <atomic>
#include <QString>
#include <map>
#include <vtkLookupTable.h>
#include <utility/random/newran.h>

namespace ofec_demo {
	enum class ParamType { alg, pro };

	extern std::atomic<bool> g_term_alg;
	extern std::atomic<bool> g_pause;
	extern std::unique_ptr<Buffer> g_buffer;
	extern ofec::Random g_random;
	extern size_t g_num_rand_colors;
	extern std::vector<std::vector<double>> g_rand_colors;
	extern vtkSmartPointer<vtkLookupTable> g_rand_color_lut;
	extern std::map<ParamType, ofec::ParamMap> g_params;

#ifdef Q_OS_MACOS
	extern QString g_tool_button_style;
#endif
	extern QString g_icon_button_style;
}

#endif // !OFEC_DEMO_GLOBAL_H