#include <QtWidgets/QApplication>
#include "main_window.h"
#include <core/instance_manager.h>
#include <run/custom_method.h>
#include <run/prime_method.h>
#ifdef Q_OS_MACOS
#include <QSurfaceFormat>
#include <QVTKOpenGLNativeWidget.h>
#endif

int main(int argc, char *argv[]) {
	QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
	QApplication app(argc, argv);

	ofec::InstanceManager::ms_instance_manager.reset(new ofec::InstanceManager);
	ofec::registerProblem();
	ofec::registerAlgorithm();
	ofec::setAlgForPro();

	ofec_demo::MainWindow mainWindow;
	mainWindow.show();

	return app.exec();
}