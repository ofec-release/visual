#include "main_window.h"

#include <QGuiApplication>
#include <QScreen>
#include <QApplication>
#include <QStatusBar>
#include <QMenuBar>
#include <QCloseEvent>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTime>
#include <QSplitter>

#include <core/global.h>
#include <core/instance_manager.h>
#include <run/prime_method.h>

#include "global_ui.h"
#include "thread/plot_problem.h"
#include "thread/run_algorithm.h"
#include "thread/play_animation.h"

namespace ofec_demo {
	MainWindow::MainWindow(QWidget* parent) :
		QMainWindow(parent),
		m_alg_running(false),
		m_sig_close(false)
	{
		using namespace ofec;
		initRandomColors();
		g_buffer.reset(new Buffer);
		
		createAboutDialog();
		createWarningMsgBox();
		createOutputDock();
		createProParamDock();
		createAlgParamDock();
		createProToolBar();
		createAlgToolBar();
		createCentralWidget();		
		createMenus();

		setCentralWidget(m_central_widget);
		addDockWidget(Qt::RightDockWidgetArea, m_pro_param_dock);
		addDockWidget(Qt::RightDockWidgetArea, m_alg_param_dock);
		addDockWidget(Qt::RightDockWidgetArea, m_output_dock);
		addToolBar(m_pro_tool_bar);
		addToolBar(m_alg_tool_bar);
		auto other_tool_bar = addToolBar("Other Tools");
		other_tool_bar->addAction(m_tile_subwindows_act);

		setWindowTitle("Visual OFEC");
		setWindowIcon(QIcon(FIG_DIR "/ofec.png"));
		statusBar()->showMessage("No algorithm running.");
		QSize available_size = QGuiApplication::screens().front()->availableGeometry().size();
#ifndef Q_OS_MACOS
		setMinimumSize(available_size.width() / 3, available_size.height() / 2);
		resize(available_size.width() * 2 / 3, available_size.height() * 2 / 3);
#else
		setMinimumSize(available_size.width() * 3 / 4, available_size.height() * 3 / 4);
#endif
		QRect r = geometry();
		r.moveCenter(QGuiApplication::screens().front()->availableGeometry().center());
		setGeometry(r);
	}

	MainWindow::~MainWindow() {}

	void MainWindow::initRandomColors() {
		using namespace ofec;
		g_num_rand_colors = 2000;
		g_rand_colors.assign(g_num_rand_colors, std::vector<double>(3));
		g_rand_color_lut = vtkLookupTable::New();
		g_rand_color_lut->SetNumberOfColors(g_num_rand_colors);
		g_rand_color_lut->SetTableRange(0, g_num_rand_colors - 1);
		for (size_t i = 0; i < g_num_rand_colors; ++i) {
			for (auto &val : g_rand_colors[i])
				val = g_random.uniform.nextNonStd<double>(0, 0.7);
			g_rand_color_lut->SetTableValue(i, g_rand_colors[i][0], g_rand_colors[i][1], g_rand_colors[i][2]);
		}
		g_rand_color_lut->Build();
	}

	void MainWindow::createAboutDialog() {
		m_about_dialog = new AboutDialog(this);
		m_show_about_dialog_act = new QAction("About", this);
		connect(m_show_about_dialog_act, &QAction::triggered, m_about_dialog, &AboutDialog::show);
	}

	void MainWindow::createWarningMsgBox() {
		m_warning_msg_box = new QMessageBox(this);
		m_warning_msg_box->setWindowTitle("Warning");
	}

	void MainWindow::createOutputDock() {
		m_output_dock = new OutputDock;
		m_output_dock->setVisible(true);

		m_show_log_dock_act = new QAction("Output");
		m_show_log_dock_act->setCheckable(true);
		m_show_log_dock_act->setChecked(m_output_dock->isVisible());
		m_show_log_dock_act->setShortcut(QKeySequence("Ctrl+o"));
		connect(m_show_log_dock_act, &QAction::triggered, this, &MainWindow::onShowLogDockActTriggered);
		connect(m_output_dock, &OutputDock::visibilityChanged, m_show_log_dock_act, &QAction::setChecked);
	}

	void MainWindow::createProParamDock() {
		m_pro_param_dock = new ParamDock("Problem");
		m_pro_param_dock->setVisible(false);

		m_show_pro_param_dock_act = new QAction("Problem Parameter");
		m_show_pro_param_dock_act->setCheckable(true);
		m_show_pro_param_dock_act->setEnabled(false);
		m_show_pro_param_dock_act->setChecked(m_pro_param_dock->isVisible());
		m_show_pro_param_dock_act->setShortcut(QKeySequence("Ctrl+p"));
		connect(m_show_pro_param_dock_act, &QAction::triggered, this, &MainWindow::onShowProParamDockActTriggered);
		connect(m_pro_param_dock, &ParamDock::visibilityChanged, m_show_pro_param_dock_act, &QAction::setChecked);
	}

	void MainWindow::createAlgParamDock() {
		m_alg_param_dock = new ParamDock("Algorithm");
		m_alg_param_dock->setVisible(false);

		m_show_alg_param_dock_act = new QAction("Algorithm Parameter");
		m_show_alg_param_dock_act->setCheckable(true);
		m_show_alg_param_dock_act->setEnabled(false);
		m_show_alg_param_dock_act->setChecked(m_pro_param_dock->isVisible());
		m_show_alg_param_dock_act->setShortcut(QKeySequence("Ctrl+a"));
		connect(m_show_alg_param_dock_act, &QAction::triggered, this, &MainWindow::onShowAlgParamDockActTriggered);
		connect(m_alg_param_dock, &ParamDock::visibilityChanged, m_show_alg_param_dock_act, &QAction::setChecked);
	}

	void MainWindow::createProToolBar() {
		m_pro_select_box = new QComboBox;
		m_pro_select_box->setPlaceholderText("Select Problem");
		for (const auto &entry : ofec::g_reg_problem.get())
			m_pro_select_box->addItem(QString::fromStdString(entry.first));
		m_pro_select_box->setMaxVisibleItems(50);
		connect(m_pro_select_box, &QComboBox::currentTextChanged, this, &MainWindow::onProNameChanged);

		m_display_pro_act = new QAction(QIcon(FIG_DIR "/display_pro.png"), "Display Problem");
		m_display_pro_act->setEnabled(false);
		connect(m_display_pro_act, &QAction::triggered, this, &MainWindow::onDisplayProActTriggered);

		m_clear_pro_act = new QAction(QIcon(FIG_DIR "/clear_pro.png"), "Clear Problem");
		m_clear_pro_act->setEnabled(false);
		connect(m_clear_pro_act, &QAction::triggered, this, &MainWindow::onClearProActTriggered);

		m_pro_tool_bar = new QToolBar("Problems tools");
		m_pro_tool_bar->addWidget(new QLabel("Problem:"));
		m_pro_tool_bar->addWidget(m_pro_select_box);
		m_pro_tool_bar->addAction(m_display_pro_act);
		m_pro_tool_bar->addAction(m_clear_pro_act);
		m_pro_tool_bar->setIconSize(QSize(20, 20));
#ifndef Q_OS_MACOS
		m_pro_tool_bar->layout()->setSpacing(3);
#endif
	}

	void MainWindow::createAlgToolBar() {
		m_alg_select_box = new QComboBox;
		m_alg_select_box->setPlaceholderText("Select Algorithm");
		for (const auto& entry : ofec::g_reg_algorithm.get())
			m_alg_select_box->addItem(QString::fromStdString(entry.first));
		m_alg_select_box->setEnabled(false);
		m_alg_select_box->setMaxVisibleItems(50);
		connect(m_alg_select_box, &QComboBox::currentTextChanged, this, &MainWindow::onAlgNameChanged);

		m_play_alg_act = new QAction(QIcon(FIG_DIR "/play.png"), "Start/Resume Algorithm");
		m_play_alg_act->setEnabled(false);
		m_play_alg_act->setShortcut(QKeySequence("Space"));
		connect(m_play_alg_act, &QAction::triggered, this, &MainWindow::onPlayAlgActTriggered);

		m_stop_alg_act = new QAction(QIcon(FIG_DIR "/stop.png"), "Terminate Algorithm");
		m_stop_alg_act->setEnabled(false);
		m_stop_alg_act->setShortcut(QKeySequence("Esc"));
		connect(m_stop_alg_act, &QAction::triggered, this, &MainWindow::onStopAlgActTriggered);

		m_alg_tool_bar = new QToolBar("Algorithm Tools");
		m_alg_tool_bar->addWidget(new QLabel("Algorithm:"));
		m_alg_tool_bar->addWidget(m_alg_select_box);
		m_alg_tool_bar->addAction(m_play_alg_act);
		m_alg_tool_bar->addAction(m_stop_alg_act);
		m_alg_tool_bar->setIconSize(QSize(20, 20));
#ifndef Q_OS_MACOS
		m_alg_tool_bar->layout()->setSpacing(3);
#endif
	}

	void MainWindow::createGraphicsArea() {
		m_graphics_area = new GraphicsArea;
		m_tile_subwindows_act = new QAction(QIcon(FIG_DIR "/tile.png"), "Tile Graphics");
		connect(m_tile_subwindows_act, &QAction::triggered, m_graphics_area, &QMdiArea::tileSubWindows);
	}

	void MainWindow::createProgressBar() {
		m_progress_bar = new ProgressBar;
		m_progress_bar->setVisible(false);
		connect(m_progress_bar, &ProgressBar::paused, this, &MainWindow::onAlgorithmPaused);
		connect(m_progress_bar, &ProgressBar::playThrdExited, qApp, &QApplication::quit);
		connect(g_buffer.get(), &Buffer::maxFrameIdIncreased, m_progress_bar, &ProgressBar::onMaxFrameIdIncreased);
		connect(m_progress_bar, &ProgressBar::curFrameIdModified, g_buffer.get(), &Buffer::onCurFrameIdModified);
		connect(m_progress_bar, &ProgressBar::minFrameIdIncreased, g_buffer.get(), &Buffer::onMinFrameIdIncreased);
	}

	void MainWindow::createCentralWidget() {
		createGraphicsArea();
		m_central_widget = new QMainWindow;
		m_central_widget->setCentralWidget(m_graphics_area);
		createProgressBar();
		m_central_widget->addToolBar(Qt::BottomToolBarArea, m_progress_bar);
	}

	void MainWindow::createMenus() {
		auto view_menu = menuBar()->addMenu("View");
		view_menu->addAction(m_show_pro_param_dock_act);
		view_menu->addAction(m_show_alg_param_dock_act);
		view_menu->addAction(m_show_log_dock_act);
		m_graphics_area->setViewMenu(view_menu);

		auto opr_menu = menuBar()->addMenu("Operation");
		opr_menu->addAction(m_display_pro_act);
		opr_menu->addAction(m_clear_pro_act);
		opr_menu->addAction(m_play_alg_act);
		opr_menu->addAction(m_stop_alg_act);
		opr_menu->addAction(m_tile_subwindows_act);

		auto help_menu = menuBar()->addMenu("Help");
		help_menu->addAction(m_show_about_dialog_act);
	}

	void MainWindow::closeEvent(QCloseEvent *event) {
		if (m_progress_bar->isPlayThrdFinished())
			event->accept();
		else {
			event->ignore();
			m_pro_tool_bar->setEnabled(false);
			m_alg_tool_bar->setEnabled(false);
			m_progress_bar->setVisible(false);
			if (m_alg_running) {
				m_sig_close = true;
				onStopAlgActTriggered();
			}
			else {
				using namespace ofec;
				statusBar()->showMessage("Exiting...");
				g_buffer->releaseBufferPro();
				g_buffer.reset();
				m_progress_bar->finishPlayThrd();
			}
		}
	}

	void MainWindow::showMessage(const QString& text) {
		statusBar()->showMessage(text);
	}

	void MainWindow::onShowLogDockActTriggered(bool checked) {
		m_output_dock->setVisible(checked);
		if (checked) {
			if (m_alg_param_dock->isVisible())
				tabifyDockWidget(m_alg_param_dock, m_output_dock);
			if (m_pro_param_dock->isVisible())
				tabifyDockWidget(m_pro_param_dock, m_output_dock);
		}
	}

	void MainWindow::onShowProParamDockActTriggered(bool checked) {
		m_pro_param_dock->setVisible(checked);
		if (checked) {
			m_pro_param_dock->setVisible(true);
			if (m_output_dock->isVisible())
				tabifyDockWidget(m_output_dock, m_pro_param_dock);
			if (m_alg_param_dock->isVisible())
				tabifyDockWidget(m_alg_param_dock, m_pro_param_dock);
		}
	}

	void MainWindow::onShowAlgParamDockActTriggered(bool checked) {
		m_alg_param_dock->setVisible(checked);
		if (checked) {
			if (m_output_dock->isVisible())
				tabifyDockWidget(m_output_dock, m_alg_param_dock);
			if (m_pro_param_dock->isVisible())
				tabifyDockWidget(m_pro_param_dock, m_alg_param_dock);
		}
	}

	void MainWindow::onProNameChanged(const QString& name) {
		if (name == "Select Problem") {
			m_pro_param_dock->setWidget(nullptr);
			m_pro_param_dock->setVisible(false);
			m_display_pro_act->setEnabled(false);
			m_show_pro_param_dock_act->setEnabled(false);
			m_show_pro_param_dock_act->setChecked(false);
		}
		else {
			g_params[ParamType::pro]["problem name"] = name.toStdString();
			updateProParamWidget();
			onShowProParamDockActTriggered(true);
			m_show_pro_param_dock_act->setEnabled(true);
			m_show_pro_param_dock_act->setChecked(true);
			m_display_pro_act->setEnabled(true);
		}
	}

	void MainWindow::onAlgNameChanged(const QString& name) {
		if (name == "Select Algorithm") {
			m_alg_param_dock->setWidget(nullptr);
			m_alg_param_dock->setVisible(false);
			m_play_alg_act->setEnabled(false);
			m_show_alg_param_dock_act->setEnabled(false);
			m_show_alg_param_dock_act->setChecked(false);
		}
		else {
			g_params[ParamType::alg]["algorithm name"] = name.toStdString();
			updateAlgParamWidget();
			onShowAlgParamDockActTriggered(true);
			m_show_alg_param_dock_act->setEnabled(true);
			m_show_alg_param_dock_act->setChecked(true);
			m_play_alg_act->setEnabled(true);
		}
	}

	void MainWindow::setProChangable(bool flag) {
		m_pro_select_box->setEnabled(flag);
		if (flag && m_pro_select_box->currentIndex() > -1)
			m_display_pro_act->setEnabled(true);
		else
			m_display_pro_act->setEnabled(false);
	}

	void MainWindow::setAlgChangable(bool flag) {
		m_alg_select_box->setEnabled(flag);
		if (flag && m_alg_select_box->currentIndex() > -1)
			m_play_alg_act->setEnabled(true);
		else
			m_play_alg_act->setEnabled(false);
	}

	void MainWindow::onDisplayProActTriggered() {
		setProChangable(false);
		plotProblem();
	}

	void MainWindow::onClearProActTriggered() {
		g_buffer->releaseBufferPro();
		setProChangable(true);
		setAlgChangable(false);
		m_clear_pro_act->setEnabled(false);
		m_pro_param_dock->switchConstruction();
	}

	void MainWindow::plotProblem() {
		using namespace ofec;
		int id_param(ADD_PARAM(g_params[ParamType::pro]));
		Real seed = g_params[ParamType::pro].count("random seed") > 0 ? 
			std::get<Real>(g_params[ParamType::pro].at("random seed")) : 0.5;
		int id_pro = ADD_PRO(id_param, seed);
		g_buffer->setupBufferPro(m_graphics_area, id_pro);
		auto thrd_plot = new ThreadPlotProblem;
		connect(thrd_plot, &ThreadPlotProblem::bufferAppended, this, &MainWindow::onProBufferAppended);
		connect(thrd_plot, &ThreadPlotProblem::showMessage, this, &MainWindow::showMessage);
		connect(thrd_plot, &ThreadPlotProblem::appendLog, m_output_dock, &OutputDock::appendLog);
		connect(thrd_plot, &ThreadPlotProblem::exceptionMessage, this, &MainWindow::onProExceptionMessage);
		thrd_plot->start();
	}

	void MainWindow::onProBufferAppended() {
		g_buffer->updateGraphicViews(0);
		m_clear_pro_act->setEnabled(true);
		setAlgChangable(true);
		m_pro_param_dock->switchVisualization();
	}

	void MainWindow::onPlayAlgActTriggered() {
		if (m_alg_running) { // resume the running algorithm
			m_play_alg_act->setEnabled(false);
			m_progress_bar->play();
			statusBar()->showMessage("Algorithm running...");
			return;
		} // match checking
		auto alg_name = std::get<std::string>(g_params[ParamType::alg].at("algorithm name"));
		auto pro_name = std::get<std::string>(g_params[ParamType::pro].at("problem name"));
		if (!ofec::checkValidation(alg_name, pro_name)) {
        	m_warning_msg_box->setText(QString::fromStdString(alg_name) + " is not applicable to solve " + QString::fromStdString(pro_name));
			m_warning_msg_box->exec();
		}
		else {  		      // start running a new algorithm
			startAlgorithm();
			m_clear_pro_act->setEnabled(false);
			setAlgChangable(false);
			m_stop_alg_act->setEnabled(true);
			m_progress_bar->setVisible(true);
		}
	}

	void MainWindow::startAlgorithm() {
		using namespace ofec;
		int id_param(ADD_PARAM(g_params[ParamType::alg]));
		Real seed = g_params[ParamType::alg].count("random seed") > 0 ?
			std::get<Real>(g_params[ParamType::alg].at("random seed")) : 0.5;
		int id_alg = ADD_ALG(id_param, g_buffer->idPro(), seed, -1);
		g_buffer->setupBufferAlg(m_graphics_area, id_alg);
		auto thrd_run = new ThreadRunAlgorithm;
		connect(thrd_run, &ThreadRunAlgorithm::algorithmTerminated, this, &MainWindow::onAlgorithmTerminated);
		connect(thrd_run, &ThreadRunAlgorithm::showMessage, this, &MainWindow::showMessage);
		connect(thrd_run, &ThreadRunAlgorithm::appendLog, m_output_dock, &OutputDock::appendLog);
		connect(thrd_run, &ThreadRunAlgorithm::exceptionMessage, this, &MainWindow::onAlgExceptionMessage);
		m_alg_running = true;
		g_term_alg = false;
		m_progress_bar->play();
		m_alg_param_dock->switchVisualization();
		thrd_run->start();
	}

	void MainWindow::onStopAlgActTriggered() {
		statusBar()->showMessage("Terminating algorithm...");
		g_term_alg = true;
		if (m_play_alg_act->isEnabled()) 			// in pausing state
			m_play_alg_act->setEnabled(false);
		m_stop_alg_act->setEnabled(false);
		m_progress_bar->setVisible(false);
	}

	void MainWindow::onAlgorithmTerminated() {
		m_progress_bar->stop();
		g_buffer->releaseBufferAlg();
		m_alg_running = false;
		g_term_alg = false;
		if (m_sig_close) {
			statusBar()->showMessage("Exiting...");
			g_buffer->releaseBufferPro();
			g_buffer.reset();
			m_progress_bar->finishPlayThrd();
		}
		else {
			setAlgChangable(true);
			m_clear_pro_act->setEnabled(true);
			m_alg_param_dock->switchConstruction();
		}
	}
	
	void MainWindow::onAlgorithmPaused() {
		m_play_alg_act->setEnabled(true);
		statusBar()->showMessage("");
	}
	
	void MainWindow::onProExceptionMessage(const QString &txt) {
		onClearProActTriggered();
		m_warning_msg_box->setText(txt);
		m_warning_msg_box->exec();
	}

	void MainWindow::onAlgExceptionMessage(const QString &txt) {
		m_progress_bar->setVisible(false);
		m_stop_alg_act->setEnabled(false);
		setAlgChangable(true);
		m_clear_pro_act->setEnabled(true);
		m_alg_param_dock->switchConstruction();
		m_progress_bar->stop();
		g_term_alg = true;
		m_alg_running = false;
		g_buffer->releaseBufferAlg();
		m_warning_msg_box->setText(txt);
		m_warning_msg_box->exec();
	}
}