#include "run_algorithm.h"
#include "../global_ui.h"
#include <core/instance_manager.h>
#include <QTime>

namespace ofec_demo {
	ThreadRunAlgorithm::ThreadRunAlgorithm() {}

	ThreadRunAlgorithm::~ThreadRunAlgorithm() {}

	void ThreadRunAlgorithm::run() {
		using namespace ofec;

		emit appendLog("[" + QTime::currentTime().toString("hh:mm:ss")
			+ "] Start initializing algorithm...");
		emit showMessage("Algorithm initializing...");
		try {
			GET_ALG(g_buffer->idAlg()).initialize();
		}
		catch (MyExcept e) {
			emit exceptionMessage(QString(e.what()));
			emit algorithmTerminated();
			return;
		}
		if (std::get<bool>(g_params[ParamType::alg].at("keep candidates updated")))
			GET_ALG(g_buffer->idAlg()).setKeepCandidatesUpdated(true);
		emit showMessage("");
		emit appendLog("[" + QTime::currentTime().toString("hh:mm:ss")
			+ "] Algorithm " + QString::fromStdString(GET_ALG(g_buffer->idAlg()).name()) + " initialized.");

		emit appendLog("[" + QTime::currentTime().toString("hh:mm:ss") 
			+ "] Start running algorithm " + QString::fromStdString(GET_ALG(g_buffer->idAlg()).name()) + ".");
		emit showMessage("Algorithm running...");
		GET_ALG(g_buffer->idAlg()).run();
		emit showMessage("");
		emit appendLog("[" + QTime::currentTime().toString("hh:mm:ss") 
			+ "] Algorithm " + QString::fromStdString(GET_ALG(g_buffer->idAlg()).name()) + " terminated.");
		emit algorithmTerminated();
	}
}