#include "plot_problem.h"
#include "../global_ui.h"
#include <core/instance_manager.h>
#include <QTime>

namespace ofec_demo {
	ThreadPlotProblem::ThreadPlotProblem() {}

	ThreadPlotProblem::~ThreadPlotProblem()	{}

	void ThreadPlotProblem::run() {
		using namespace ofec;

		emit appendLog("[" + QTime::currentTime().toString("hh:mm:ss")
			+ "] Start initializing problem...");
		emit showMessage("Problem initializing...");

		try {
			GET_PRO(g_buffer->idPro()).initialize();
		}
		catch (MyExcept e) {
			emit exceptionMessage(QString(e.what()));
			return;
		}

	
		emit showMessage("");
		emit appendLog("[" + QTime::currentTime().toString("hh:mm:ss")
			+ "] Problem " + QString::fromStdString(GET_PRO(g_buffer->idPro()).name()) + " initialized.");

		emit appendLog("[" + QTime::currentTime().toString("hh:mm:ss")
			+ "] Start sampling problem " + QString::fromStdString(GET_PRO(g_buffer->idPro()).name()) + "...");
		emit showMessage("Problem sampling...");
		g_buffer->appendProBuffer(g_buffer->idPro());
		emit showMessage("");
		emit appendLog("[" + QTime::currentTime().toString("hh:mm:ss")
			+ "] Problem " + QString::fromStdString(GET_PRO(g_buffer->idPro()).name()) + " sampled.");
		emit bufferAppended();
	}
}