#ifndef OFEC_DEMO_RUN_ALG_H
#define OFEC_DEMO_RUN_ALG_H

#include <QThread>

namespace ofec_demo {
	class ThreadRunAlgorithm : public QThread {
		Q_OBJECT
	public:
		ThreadRunAlgorithm();
		~ThreadRunAlgorithm();
	private:
		void run() override;
	signals:
		void algorithmTerminated();
		void showMessage(const QString &text);
		void appendLog(const QString &text);
		void exceptionMessage(const QString &txt);
	};
}

#endif // !OFEC_DEMO_RUN_ALG_H
