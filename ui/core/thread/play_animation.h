#ifndef OFEC_DEMO_PLAY_ANI_H
#define OFEC_DEMO_PLAY_ANI_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QReadWriteLock>

namespace ofec_demo {
	class ThreadPlayAnimation : public QThread {
		Q_OBJECT
	private:
		QMutex m_wait_mutex;
		QWaitCondition m_wait_cond;
		int m_play_speed;
		QReadWriteLock m_play_speed_lock;
		bool m_terminate;

	public:
		ThreadPlayAnimation(int play_speed);
		~ThreadPlayAnimation() {}
		void speedChanged(int play_speed);
		void resume();
		void terminate();

	private:
		void run() override;

	signals:
		void oneLapsePassed();
	};
}

#endif // !OFEC_DEMO_PLAY_ANI_H
