#include "play_animation.h"

namespace ofec_demo {
	ThreadPlayAnimation::ThreadPlayAnimation(int play_speed) : 
		m_play_speed(play_speed), m_terminate(false) {}

	void ThreadPlayAnimation::run() {
		int time_sleep;
		while (!m_terminate) {
			m_wait_mutex.lock();
			m_wait_cond.wait(&m_wait_mutex);
			m_wait_mutex.unlock();
			if (m_terminate)
				break;
			m_play_speed_lock.lockForRead();
			time_sleep = 1000 / m_play_speed;
			m_play_speed_lock.unlock();
			msleep(time_sleep);
			if (!m_terminate)
				emit oneLapsePassed();
		}
	}

	void ThreadPlayAnimation::speedChanged(int play_speed) {
		m_play_speed_lock.lockForWrite();
		m_play_speed = play_speed;
		m_play_speed_lock.unlock();
	}

	void ThreadPlayAnimation::resume() {
		m_wait_cond.wakeOne();
	}

	void ThreadPlayAnimation::terminate() {
		m_terminate = true;
		m_wait_cond.wakeOne();
	}
}