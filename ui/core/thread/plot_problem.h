#ifndef OFEC_DEMO_PLOT_PRO_H
#define OFEC_DEMO_PLOT_PRO_H

#include <QThread>

namespace ofec_demo {
	class ThreadPlotProblem : public QThread {
		Q_OBJECT
	public:
		ThreadPlotProblem();
		~ThreadPlotProblem();
	private:
		void run() override;
	signals:
		void bufferAppended();
		void showMessage(const QString &text);
		void appendLog(const QString &text);
		void exceptionMessage(const QString & txt);
	};
}

#endif // !OFEC_DEMO_PLOT_PRO_H
