#include "buffer_alg.h"

namespace ofec_demo {
	BufferAlg::BufferAlg(GraphicsArea *graphic_dock_set, int id_alg, BufferPro *buffer_pro) :
		m_graphics_area(graphic_dock_set),
		m_id_alg(id_alg),
		m_buffer_pro(buffer_pro) {}
	
	bool BufferAlg::graphicViewsInitialized() { 
		return m_buffer_pro->graphicViewsInitialized(); 
	}
		
	void BufferAlg::updateViews() {
		m_buffer_pro->updateViews();
	}
}
