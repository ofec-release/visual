#include "items_2d_pro.h"
#include <QGraphicsScene>

namespace ofec_demo {	
	const qreal ObjSpace2d::ms_lenth(100);

	ObjSpace2d::ObjSpace2d() :
		m_font_title("Courier", 4, QFont::Bold) {}

	void ObjSpace2d::updateBoundary(const std::vector<std::pair<ofec::Real, ofec::Real>> &range) {
		m_boundary.reset(new Rect2d(-ms_lenth, -ms_lenth, ms_lenth * 2, ms_lenth * 2, Qt::transparent, Qt::DotLine));
		m_space_top.reset(new Rect2d(-ms_lenth, -1.25 * ms_lenth, ms_lenth * 2, ms_lenth * 0.25, Qt::transparent, Qt::NoPen));
		m_space_right.reset(new Rect2d(ms_lenth, -ms_lenth, ms_lenth * 0.2, ms_lenth * 2, Qt::transparent, Qt::NoPen));
		m_axis_f1.reset(new Arrow2d(-ms_lenth, ms_lenth, 1.15 * ms_lenth, ms_lenth));
		m_axis_f2.reset(new Arrow2d(-ms_lenth, ms_lenth, -ms_lenth, -1.15 * ms_lenth));
		m_title_bottom.reset(new Text2d(-ms_lenth, ms_lenth, ms_lenth * 2, ms_lenth * 0.2, "f1", m_font_title, Qt::black, false));
		m_title_left.reset(new Text2d(-ms_lenth, -1.2 * ms_lenth, ms_lenth * 2, ms_lenth * 0.2, "f2", m_font_title, Qt::black, false, -90));
	}

	void ObjSpace2d::addBoundary(QGraphicsScene *scene) {
		scene->addItem(m_boundary.get());
		scene->addItem(m_space_top.get());
		scene->addItem(m_space_right.get());
		scene->addItem(m_axis_f1.get());
		scene->addItem(m_axis_f2.get());
		scene->addItem(m_title_bottom.get());
		scene->addItem(m_title_left.get());
	}

	void ObjSpace2d::removeBoundary(QGraphicsScene *scene) {
		scene->removeItem(m_boundary.get());
		scene->removeItem(m_space_top.get());
		scene->removeItem(m_space_right.get());
		scene->removeItem(m_axis_f1.get());
		scene->removeItem(m_axis_f2.get());
		scene->removeItem(m_title_bottom.get());
		scene->removeItem(m_title_left.get());
	}

	void ObjSpace2d::updateOptima(
		const std::vector<std::vector<ofec::Real>> &optima,
		const std::vector<std::pair<ofec::Real, ofec::Real>> &range)
	{
		std::vector<QPointF> tmp_pts(optima.size());
		for (size_t i = 0; i < optima.size(); ++i) {
			tmp_pts[i].setX(((optima[i][0] - range[0].first) / (range[0].second - range[0].first) * 2 - 1) * ms_lenth);
			tmp_pts[i].setY((1 - (optima[i][1] - range[1].first) / (range[1].second - range[1].first) * 2) * ms_lenth);
		}
		m_optima.reset(new Points2d(tmp_pts, 3, Qt::red));
	}

	void ObjSpace2d::addOptima(QGraphicsScene *scene) {
		scene->addItem(m_optima.get());
	}

	void ObjSpace2d::removeOptima(QGraphicsScene *scene) {
		scene->removeItem(m_optima.get());
	}

	void ObjSpace2d::setOptimaVisible(bool flag) {
		m_optima->setVisible(flag);
	}
}