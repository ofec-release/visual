#ifndef OFEC_DEMO_ITEMS_2D_EA_H
#define OFEC_DEMO_ITEMS_2D_EA_H

#include "geometrics/geom_2d.h"
#include <core/definition.h>

namespace ofec_demo {
	class PopObjSpace2d {
	private:
		std::list<std::unique_ptr<Points2d>> m_multi_pop;
	public:
		void updateMultiPops(
			const std::vector<std::vector<ofec::Real>> &mp,
			const std::vector<int> &pi, int np, 
			const std::vector<std::pair<ofec::Real, ofec::Real>> &range);
		void addMultiPops(QGraphicsScene *scene);
		void removeMultiPops(QGraphicsScene *scene);
		void setMultiPopsVisible(bool flag);
	};
}

#endif // !OFEC_DEMO_ITEMS_2D_EA_H
