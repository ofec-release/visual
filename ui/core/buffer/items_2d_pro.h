#ifndef OFEC_DEMO_ITEMS_2D_PRO_H
#define OFEC_DEMO_ITEMS_2D_PRO_H

#include "geometrics/geom_2d.h"
#include <core/definition.h>

namespace ofec_demo {
	class ObjSpace2d {
	public:
		static const qreal ms_lenth;  // domain in coordinate of graphic scnene: [-ms_lenth, ms_lenth]

	protected:
		std::unique_ptr<Rect2d> m_boundary, m_space_top, m_space_right;
		std::unique_ptr<Arrow2d> m_axis_f1, m_axis_f2;
		std::unique_ptr<Text2d> m_title_bottom, m_title_left;
		std::unique_ptr<Points2d> m_optima;
		QFont m_font_title;

	public:
		ObjSpace2d();
		void updateBoundary(const std::vector<std::pair<ofec::Real, ofec::Real>> &range);
		void addBoundary(QGraphicsScene *scene);
		void removeBoundary(QGraphicsScene *scene);

		void updateOptima(
			const std::vector<std::vector<ofec::Real>> &optima,
			const std::vector<std::pair<ofec::Real, ofec::Real>> &range);
		void addOptima(QGraphicsScene *scene);
		void removeOptima(QGraphicsScene *scene);

		void setOptimaVisible(bool flag);
	};
}

#endif // !OFEC_DEMO_ITEMS_2D_PRO_H
