#ifndef OFEC_DEMO_BUFFER_EA_H
#define OFEC_DEMO_BUFFER_EA_H

#include "buffer_alg.h"
#include "items_2d_ea.h"
#include "items_3d_ea.h"
#include "../global_ui.h"
#include "../parameter/visual_alg.h"
#include <core/instance_manager.h>

namespace ofec_demo {
	/* Buffer of Evolutionary Algorithm */
	template<typename TInd>
	class BufferEA : public BufferAlg {
	private:
		std::vector<std::vector<ofec::Real>> m_pop_obj;
		struct BufferDatum {
			std::unique_ptr<PopObjSpace2d> pop_obj_space_2d;
			std::unique_ptr<PopObjSpace3d> pop_obj_space_3d;
		};
		std::map<int, std::unique_ptr<BufferDatum>> m_buffer_data;
		BufferDatum *m_cur_datum;

	protected:
		struct PopInfo {
			std::vector<std::unique_ptr<TInd>> inds;
			std::vector<int> popIDs_of_inds;
			int num_pops;
		};
		std::map<int, std::unique_ptr<PopInfo>> m_buffer_pop_info;

	public:
		BufferEA(GraphicsArea *graphic_dock_set, int id_alg, BufferPro *buffer_pro) :
			BufferAlg(graphic_dock_set, id_alg, buffer_pro), m_cur_datum(nullptr) {}
		virtual ~BufferEA() {}
		void appendBuffer(int max_buffer_id) override;
		void popupBuffer(int buffer_id) override;
		void updateGraphicViews(int buffer_id) override;
		void setSolutionVisible(bool flag) override;

	private:
		void updatePopInfo(PopInfo *pop_info);
		void updatePopObjSpace2d(PopInfo *pop_info, BufferDatum *pg);
		void updatePopObjSpace3d(PopInfo *pop_info, BufferDatum *pg);
		void removeCurDatum();
		void addNewDatum(int buffer_id);
	};

	template<typename TInd>
	void BufferEA<TInd>::appendBuffer(int max_buffer_id) {
		using namespace ofec;
		auto pop_info = new PopInfo;
		updatePopInfo(pop_info);
		auto buffer_datum = new BufferDatum;
		if (GET_PRO(m_buffer_pro->idPro()).numObjectives() == 2)
			updatePopObjSpace2d(pop_info, buffer_datum);
		else if (GET_PRO(m_buffer_pro->idPro()).numObjectives() == 3)
			updatePopObjSpace3d(pop_info, buffer_datum);
		m_buffer_pop_info[max_buffer_id + 1].reset(pop_info);
		m_buffer_data[max_buffer_id + 1].reset(buffer_datum);
	}

	template<typename TInd>
	void BufferEA<TInd>::popupBuffer(int buffer_id) {
		m_buffer_pop_info.erase(buffer_id);
		m_buffer_data.erase(buffer_id);
	}

	template<typename TInd>
	void BufferEA<TInd>::updateGraphicViews(int buffer_id) {
		if (m_cur_datum)
			removeCurDatum();
		if (m_buffer_data.count(buffer_id) > 0)
			addNewDatum(buffer_id);
	}

	template<typename TInd>
	void BufferEA<TInd>::setSolutionVisible(bool flag) {
		using namespace ofec;
		if (GET_PRO(m_buffer_pro->idPro()).numObjectives() == 2)
			m_cur_datum->pop_obj_space_2d->setMultiPopsVisible(flag);
		else if (GET_PRO(m_buffer_pro->idPro()).numObjectives() == 3)
			m_cur_datum->pop_obj_space_3d->setMultiPopsVisible(flag);
	}

	template<typename TInd>
	void BufferEA<TInd>::updatePopInfo(PopInfo *pop_info) {
		using namespace ofec;
		auto &pops = GET_ALG(m_id_alg).getSolution();
		int size(0);
		for (auto i = pops.begin(); i != pops.end(); ++i)
			size += i->size();
		pop_info->inds.resize(size);
		pop_info->popIDs_of_inds.resize(size);
		m_pop_obj.resize(size);
		int j = 0;
		int id_pop = 0;
		for (auto &pop : pops) {
			if (pop.empty())
				continue;
			else {
				for (auto k = 0; k < pop.size(); ++k, ++j) {
					pop_info->inds[j].reset(new TInd(*dynamic_cast<const typename TInd::SolType *>(pop[k])));
					pop_info->popIDs_of_inds[j] = id_pop;
					m_pop_obj[j] = pop_info->inds[j]->objective();
				}
				id_pop++;
			}
		}
		pop_info->num_pops = id_pop;
		if (!pop_info->inds.empty()) {
			size_t num_objs = GET_PRO(m_buffer_pro->idPro()).numObjectives();
			if (m_cur_obj_range.size() == 0) m_cur_obj_range.resize(num_objs);
			for (size_t j = 0; j < num_objs; ++j) {
				m_cur_obj_range[j].first = pop_info->inds.front()->objective(j);
				m_cur_obj_range[j].second = pop_info->inds.front()->objective(j);
			}
			for (auto &i : pop_info->inds) {
				for (auto j = 0; j < num_objs; ++j) {
					if (m_cur_obj_range[j].first > i->objective(j)) m_cur_obj_range[j].first = i->objective(j);
					if (m_cur_obj_range[j].second < i->objective(j)) m_cur_obj_range[j].second = i->objective(j);
				}
			}
		}
	}

	template<typename TInd>
	void BufferEA<TInd>::updatePopObjSpace2d(PopInfo *pop_info, BufferDatum *buffer_datum) {
		using namespace ofec;
		const auto &range_obj = m_buffer_pro->rangeObj();
		buffer_datum->pop_obj_space_2d.reset(new PopObjSpace2d);
		buffer_datum->pop_obj_space_2d->updateMultiPops(m_pop_obj, pop_info->popIDs_of_inds, pop_info->num_pops, range_obj);
	}

	template<typename TInd>
	void BufferEA<TInd>::updatePopObjSpace3d(PopInfo *pop_info, BufferDatum *buffer_datum) {
		using namespace ofec;
		const auto &range_obj = m_buffer_pro->rangeObj();
		buffer_datum->pop_obj_space_3d.reset(new PopObjSpace3d);
		buffer_datum->pop_obj_space_3d->updateMultiPops(m_pop_obj, pop_info->popIDs_of_inds, pop_info->num_pops, range_obj);
	}

	template<typename TInd>
	void BufferEA<TInd>::removeCurDatum() {
		using namespace ofec;
		if (GET_PRO(m_buffer_pro->idPro()).numObjectives() == 2) {
			auto scene = m_buffer_pro->objSpace2dScene();
			m_cur_datum->pop_obj_space_2d->removeMultiPops(scene);
		}
		else if (GET_PRO(m_buffer_pro->idPro()).numObjectives() == 3) {
			auto renderer = m_buffer_pro->objSpace3dRenderer();
			m_cur_datum->pop_obj_space_3d->removeMultiPops(renderer);
		}
		m_cur_datum = nullptr;
	}

	template<typename TInd>
	void BufferEA<TInd>::addNewDatum(int buffer_id) {
		using namespace ofec;
		auto &new_graph = m_buffer_data.at(buffer_id);
		if (GET_PRO(m_buffer_pro->idPro()).numObjectives() == 2) {
			auto scene = m_buffer_pro->objSpace2dScene();
			new_graph->pop_obj_space_2d->addMultiPops(scene);
			new_graph->pop_obj_space_2d->setMultiPopsVisible(VisualAlg::ms_show_solution);
		}
		else if (GET_PRO(m_buffer_pro->idPro()).numObjectives() == 3) {
			auto renderer = m_buffer_pro->objSpace3dRenderer();
			new_graph->pop_obj_space_3d->addMultiPops(renderer);
			new_graph->pop_obj_space_3d->setMultiPopsVisible(VisualAlg::ms_show_solution);
		}
		m_cur_datum = new_graph.get();
	}
}

#endif // !OFEC_DEMO_BUFFER_EA_H