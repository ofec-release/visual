#include "items_3d_pro.h"
#include "geometrics/geom_3d.h"
#include <vtkMapper.h>
#include <vtkProperty.h>
#include <array>
#include <vtkTextProperty.h>

namespace ofec_demo {
	void ObjSpace3d::updateBoundary(const std::vector<std::pair<ofec::Real, ofec::Real>> &range) {
		std::vector<std::array<ofec::Real, 3>> pts(2);
		std::array<ofec::Real, 3> drc;
		pts[0] = { -5, -5, -5 };
		pts[1] = { 5 * 1.15, -5, -5 };
		drc = { 1, 0, 0 };
		m_axis_f1_line = Geom3d::createPolyLine(pts, 0, 0, 0);
		m_axis_f1_cone = Geom3d::createCone(pts[1], drc, 0.45, 0.15, 0, 0, 0);
		m_axis_f1_label = Geom3d::createText(pts[1], "f1", 20, 0, 0, 0);
		pts[1] = { -5, 5 * 1.15, -5 };
		drc = { 0, 1, 0 };
		m_axis_f2_line = Geom3d::createPolyLine(pts, 0, 0, 0);
		m_axis_f2_cone = Geom3d::createCone(pts[1], drc, 0.45, 0.15, 0, 0, 0);
		m_axis_f2_label = Geom3d::createText(pts[1], "f2", 20,  0, 0, 0);
		pts[1] = { -5, -5, 5 * 1.15 };
		drc = { 0, 0, 1 };
		m_axis_f3_line = Geom3d::createPolyLine(pts, 0, 0, 0);
		m_axis_f3_cone = Geom3d::createCone(pts[1], drc, 0.45, 0.15, 0, 0, 0);
		m_axis_f3_label = Geom3d::createText(pts[1], "f3", 20, 0, 0, 0);

		std::vector<std::array<ofec::Real, 3>> pts2(1);
		pts2[0] = { 0, 0, 0 };
		m_center = Geom3d::createPoints(pts2, 5, 0, 0, 0, 0.2);

		std::array<ofec::Real, 3> pts3, size;
		pts3 = { -5, -5, -5 };
		size = { 10, 10, 10 };
		m_boundary = Geom3d::createCuboidFrame(pts3, size, 0, 0, 0, 0.2);
	}

	void ObjSpace3d::addBoundary(vtkRenderer *renderer) {
		renderer->AddActor(m_axis_f1_line);
		renderer->AddActor(m_axis_f1_cone);
		renderer->AddActor(m_axis_f1_label);
		renderer->AddActor(m_axis_f2_line);
		renderer->AddActor(m_axis_f2_cone);
		renderer->AddActor(m_axis_f2_label);
		renderer->AddActor(m_axis_f3_line);
		renderer->AddActor(m_axis_f3_cone);
		renderer->AddActor(m_axis_f3_label);
		renderer->AddActor(m_center);
		renderer->AddActor(m_boundary);
	}

	void ObjSpace3d::removeBoundary(vtkRenderer *renderer) {
		renderer->RemoveActor(m_axis_f1_line);
		renderer->RemoveActor(m_axis_f1_cone);
		renderer->RemoveActor(m_axis_f1_label);
		renderer->RemoveActor(m_axis_f2_line);
		renderer->RemoveActor(m_axis_f2_cone);
		renderer->RemoveActor(m_axis_f2_label);
		renderer->RemoveActor(m_axis_f3_line);
		renderer->RemoveActor(m_axis_f3_cone);
		renderer->RemoveActor(m_axis_f3_label);
		renderer->RemoveActor(m_center);
		renderer->RemoveActor(m_boundary);
	}

	void ObjSpace3d::updateOptima(
		const std::vector<std::vector<ofec::Real>> &optima,
		const std::vector<std::pair<ofec::Real, ofec::Real>> &range)
	{
		std::vector<std::array<ofec::Real, 3>> pts(optima.size());
		for (size_t i = 0; i < optima.size(); ++i) {
			pts[i][0] = (optima[i][0] - range[0].first) / (range[0].second - range[0].first) * 10 - 5;
			pts[i][1] = (optima[i][1] - range[1].first) / (range[1].second - range[1].first) * 10 - 5;
			pts[i][2] = (optima[i][2] - range[2].first) / (range[2].second - range[2].first) * 10 - 5;
		}
		m_optima = Geom3d::createPoints(pts, 4, 1.0, 0.0, 0.0);
	}

	void ObjSpace3d::addOptima(vtkRenderer *scene) {
		scene->AddActor(m_optima);
	}

	void ObjSpace3d::removeOptima(vtkRenderer *scene) {
		scene->RemoveActor(m_optima);
	}

	void ObjSpace3d::setOptimaVisible(bool flag) {
		m_optima->SetVisibility(flag);
	}
}