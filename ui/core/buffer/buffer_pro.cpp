#include "buffer_pro.h"
#include <core/instance_manager.h>
#include "../parameter/visual_pro.h"
#include "../global_ui.h"

namespace ofec_demo {
	BufferPro::BufferPro(GraphicsArea *graphic_dock_set, int id_pro) :
		m_graphics_area(graphic_dock_set),
		m_id_pro(id_pro),
		m_cur_datum(nullptr) {}

	BufferPro::~BufferPro() {
		using namespace ofec;
		if (m_cur_datum)
			removeCurDatum();
		if (GET_PRO(m_id_pro).numObjectives() == 2) {
			m_graphics_area->removeGraphic("Objective Space");
		}
		else if (GET_PRO(m_id_pro).numObjectives() == 3) {
			m_graphics_area->removeGraphic("Objective Space");
		}
		//m_graphics_area->tileSubWindows();
	}

	void BufferPro::initializeGraphicViews() {
		using namespace ofec;
		if (GET_PRO(m_id_pro).numObjectives() == 2) {
			m_obj_space_view_2d.reset(new GraphicView2d);
			m_graphics_area->addGraphic("Objective Space", m_obj_space_view_2d.get());
		}
		else if (GET_PRO(m_id_pro).numObjectives() == 3) {
			m_obj_space_view_3d.reset(new GraphicView3d);
			m_graphics_area->addGraphic("Objective Space", m_obj_space_view_3d.get());
		}
	}

	bool BufferPro::graphicViewsInitialized() {
		using namespace ofec;
		bool flag = true;
		if (GET_PRO(m_id_pro).numObjectives() == 2)
			flag = flag && m_obj_space_view_2d;
		else if (GET_PRO(m_id_pro).numObjectives() == 3)
			flag = flag && m_obj_space_view_3d;
		return flag;
	}

	void BufferPro::appendBuffer(int max_buffer_id) {
		using namespace ofec;
		auto buffer_datum = new BufferDatum;
		if (GET_PRO(m_id_pro).numObjectives() == 2)
			updateObjSpace2d(buffer_datum);
		else if (GET_PRO(m_id_pro).numObjectives() == 3)
			updateObjSpace3d(buffer_datum);
		m_buffer_data[max_buffer_id + 1].reset(buffer_datum);
	}
	
	void BufferPro::popupBuffer(int buffer_id) {
		m_buffer_data.erase(buffer_id);
	}

	void BufferPro::updateGraphicViews(int buffer_id) {
		if (m_cur_datum)
			removeCurDatum();
		if (m_buffer_data.count(buffer_id) > 0)
			addNewDatum(buffer_id);
	}

	void BufferPro::updateViews() {
		using namespace ofec;
		if (GET_PRO(m_id_pro).numObjectives() == 2)
			m_obj_space_view_2d->updateView();
		else if (GET_PRO(m_id_pro).numObjectives() == 3)
			m_obj_space_view_3d->updateView();
	}

	void BufferPro::setOptimaVisible(bool flag) {
		using namespace ofec;
		if (m_cur_datum && GET_PRO(m_id_pro).isOptimaObjGiven()) {
			if (GET_PRO(m_id_pro).numObjectives() == 2)
				m_cur_datum->obj_space_2d->setOptimaVisible(flag);
			else if (GET_PRO(m_id_pro).numObjectives() == 3)
				m_cur_datum->obj_space_3d->setOptimaVisible(flag);
		}
	}

	void BufferPro::updateObjSpace2d(BufferDatum *buffer_datum) {
		using namespace ofec;
		buffer_datum->obj_space_2d.reset(new ObjSpace2d);
		buffer_datum->obj_space_2d->updateBoundary(m_range_obj);
		if (GET_PRO(m_id_pro).isOptimaObjGiven())
			buffer_datum->obj_space_2d->updateOptima(m_optima_obj, m_range_obj);
	}

	void BufferPro::updateObjSpace3d(BufferDatum *buffer_datum) {
		using namespace ofec;
		buffer_datum->obj_space_3d.reset(new ObjSpace3d);
		buffer_datum->obj_space_3d->updateBoundary(m_range_obj);
		if (GET_PRO(m_id_pro).isOptimaObjGiven())
			buffer_datum->obj_space_3d->updateOptima(m_optima_obj, m_range_obj);
	}

	void BufferPro::removeCurDatum() {
		using namespace ofec;
		if (GET_PRO(m_id_pro).numObjectives() == 2) {
			m_cur_datum->obj_space_2d->removeBoundary(m_obj_space_view_2d->scene());
			if (GET_PRO(m_id_pro).isOptimaObjGiven())
				m_cur_datum->obj_space_2d->removeOptima(m_obj_space_view_2d->scene());
		}
		else if (GET_PRO(m_id_pro).numObjectives() == 3) {
			m_cur_datum->obj_space_3d->removeBoundary(m_obj_space_view_3d->renderer());
			if (GET_PRO(m_id_pro).isOptimaObjGiven())
				m_cur_datum->obj_space_3d->removeOptima(m_obj_space_view_3d->renderer());
		}
		m_cur_datum = nullptr;
	}

	void BufferPro::addNewDatum(int buffer_id) {
		using namespace ofec;
		auto &new_datum = m_buffer_data.at(buffer_id);
		if (GET_PRO(m_id_pro).numObjectives() == 2) {
			new_datum->obj_space_2d->addBoundary(m_obj_space_view_2d->scene());
			if (GET_PRO(m_id_pro).isOptimaObjGiven()) {
				new_datum->obj_space_2d->addOptima(m_obj_space_view_2d->scene());
				new_datum->obj_space_2d->setOptimaVisible(VisualPro::ms_show_optima);
			}
		}
		else if (GET_PRO(m_id_pro).numObjectives() == 3) {
			new_datum->obj_space_3d->addBoundary(m_obj_space_view_3d->renderer());
			if (GET_PRO(m_id_pro).isOptimaObjGiven()) {
				new_datum->obj_space_3d->addOptima(m_obj_space_view_3d->renderer());
				new_datum->obj_space_3d->setOptimaVisible(VisualPro::ms_show_optima);
			}
			m_obj_space_view_3d->resetCamera();
		}
		m_cur_datum = new_datum.get();
	}
}