#include "chart_2d.h"
#include <vtkTable.h>
#include <vtkIntArray.h>
#include <vtkFloatArray.h>
#include <vtkStringArray.h>
#include <vtkPlot.h>
#include <vtkPlotPoints.h>
#include <vtkPlotArea.h>
#include <vtkBrush.h>
#include <vtkPlotPie.h>
#include <vtkAxis.h>

namespace ofec_demo {
	namespace Chart2d {
		void addLine(
			vtkChart* chart, 
			const std::vector<ofec::Real>& x, 
			const std::vector<ofec::Real>& y, 
			const std::string& label, 
			double* color)
		{
			if (x.size() != y.size() || x.size() < 5)
				return;
			auto table = vtkSmartPointer<vtkTable>::New();
			auto arr_x = vtkSmartPointer<vtkFloatArray>::New();
			auto arr_y = vtkSmartPointer<vtkFloatArray>::New();
			auto line = chart->AddPlot(vtkChart::LINE);
			arr_x->SetName("X Array");
			arr_y->SetName(label.c_str());
			table->AddColumn(arr_x);
			table->AddColumn(arr_y);
			table->SetNumberOfRows(x.size());
			for (size_t i = 0; i < x.size(); i++) {
				table->SetValue(i, 0, x[i]);
				table->SetValue(i, 1, y[i]);
			}
			line->SetInputData(table, 0, 1);
			line->SetColor(color[0], color[1], color[2]);
		}
		
		void addScatter(
			vtkChart* chart, 
			const std::vector<ofec::Real>& x, 
			const std::vector<ofec::Real>& y, 
			const std::string& label, 
			double* color)
		{
			auto table = vtkSmartPointer<vtkTable>::New();
			auto arr_x = vtkSmartPointer<vtkFloatArray>::New();
			auto arr_y = vtkSmartPointer<vtkFloatArray>::New();
			auto scatter = chart->AddPlot(vtkChart::POINTS);
			arr_x->SetName("X Array");
			arr_y->SetName(label.c_str());
			table->AddColumn(arr_x);
			table->AddColumn(arr_y);
			table->SetNumberOfRows(x.size());
			for (size_t i = 0; i < x.size(); i++) {
				table->SetValue(i, 0, x[i]);
				table->SetValue(i, 1, y[i]);
			}
			scatter->SetInputData(table, 0, 1);
			scatter->SetColor(color[0], color[1], color[2]);
			vtkPlotPoints::SafeDownCast(scatter)->SetMarkerStyle(vtkPlotPoints::CIRCLE);
			vtkPlotPoints::SafeDownCast(scatter)->SetMarkerSize(5);
		}
		
		void addArea(
			vtkChart *chart, 
			const std::vector<ofec::Real> &x, 
			const std::vector<ofec::Real> &y1, 
			const std::vector<ofec::Real> &y2, 
			double *color)
		{
			auto table = vtkSmartPointer<vtkTable>::New();
			auto arr_x = vtkSmartPointer<vtkFloatArray>::New();
			auto arr_y1 = vtkSmartPointer<vtkFloatArray>::New();
			auto arr_y2 = vtkSmartPointer<vtkFloatArray>::New();
			auto area = dynamic_cast<vtkPlotArea *>(chart->AddPlot(vtkChart::AREA));
			arr_x->SetName("X Array");
			arr_y1->SetName("Y Max");
			arr_y2->SetName("Y Min");
			table->AddColumn(arr_x);
			table->AddColumn(arr_y1);
			table->AddColumn(arr_y2);
			table->SetNumberOfRows(x.size());
			for (size_t i = 0; i < x.size(); i++) {
				table->SetValue(i, 0, x[i]);
				table->SetValue(i, 1, y1[i]);
				table->SetValue(i, 2, y2[i]);
			}
			area->SetInputData(table);
			area->SetInputArray(0, "X Array");
			area->SetInputArray(1, "Y Max");
			area->SetInputArray(2, "Y Min");
			area->GetBrush()->SetColorF(color[0], color[1], color[2], 0.6);
		}
		
		void addPieChart(
			vtkChartPie *chart, 
			const std::vector<ofec::Real> &data, 
			const std::vector<std::string> &labels,
			int color_scheme)
		{
			auto table = vtkSmartPointer<vtkTable>::New();
			auto arr_data = vtkSmartPointer<vtkFloatArray>::New();
			auto arr_label = vtkSmartPointer<vtkStringArray>::New();
			auto color_series = vtkSmartPointer<vtkColorSeries>::New();
			auto pie = dynamic_cast<vtkPlotPie *>(chart->AddPlot(0));
			arr_data->SetName("Data");
			for (int i = 0; i < data.size(); i++) {
				arr_data->InsertNextValue(data[i]);
				arr_label->InsertNextValue(labels[i].c_str());
			}
			table->AddColumn(arr_data);
			pie->SetInputData(table);
			pie->SetInputArray(0, "Data");
			pie->SetLabels(arr_label);
			color_series->SetColorScheme(color_scheme);
			pie->SetColorSeries(color_series);
		}
		
		void addBars(
			vtkChart *chart, 
			const std::vector<ofec::Real> &data, 
			const std::vector<std::string> &labels,
			int color_scheme)
		{
			auto table = vtkSmartPointer<vtkTable>::New();
			auto arr_x = vtkSmartPointer<vtkIntArray>::New();
			auto color_series = vtkSmartPointer<vtkColorSeries>::New();
			arr_x->SetName("seq");
			table->AddColumn(arr_x);
			for (size_t i = 0; i < data.size(); i++) {
				auto arr_data = vtkSmartPointer<vtkFloatArray>::New();
				arr_data->SetName(labels[i].c_str());
				table->AddColumn(arr_data);
			}
			table->SetNumberOfRows(1);
			table->SetValue(0, 0, 1);
			color_series->SetColorScheme(color_scheme);
			for (size_t i = 0; i < data.size(); i++) {
				table->SetValue(0, i + 1, data[i]);
				auto bars = chart->AddPlot(vtkChart::BAR);
				bars->SetInputData(table, 0, i + 1);
				bars->SetColor(
					color_series->GetColor(i).GetRed() / 255.0,
					color_series->GetColor(i).GetGreen() / 255.0,
					color_series->GetColor(i).GetBlue() / 255.0);
			}
			chart->SetShowLegend(true);
			chart->GetAxis(vtkAxis::BOTTOM)->SetGridVisible(false);
			chart->GetAxis(vtkAxis::BOTTOM)->SetTicksVisible(false);
			chart->GetAxis(vtkAxis::BOTTOM)->SetLabelsVisible(false);
			chart->GetAxis(vtkAxis::BOTTOM)->SetTitleVisible(false);
			chart->GetAxis(vtkAxis::LEFT)->SetGridVisible(false);
		}
	}
}