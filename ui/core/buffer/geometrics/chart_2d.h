#ifndef OFEC_DEMO_CHART_H
#define OFEC_DEMO_CHART_H

#include <vtkChart.h>
#include <vtkChartPie.h>
#include <vtkColorSeries.h>
#include <string>
#include <vector>
#include <core/definition.h>

namespace ofec_demo {
	namespace Chart2d {
		void addLine(
			vtkChart *chart,
			const std::vector<ofec::Real> &x,
			const std::vector<ofec::Real> &y,
			const std::string &label,
			double *color);

		void addScatter(
			vtkChart *chart,
			const std::vector<ofec::Real> &x,
			const std::vector<ofec::Real> &y,
			const std::string &label,
			double *color);

		void addArea(
			vtkChart *chart,
			const std::vector<ofec::Real> &x,
			const std::vector<ofec::Real> &y1,
			const std::vector<ofec::Real> &y2,
			double *color);

		void addPieChart(
			vtkChartPie *chart,
			const std::vector<ofec::Real> &data,
			const std::vector<std::string> &labels,
			int color_scheme = vtkColorSeries::BREWER_QUALITATIVE_SET3);

		void addBars(
			vtkChart *chart,
			const std::vector<ofec::Real> &data,
			const std::vector<std::string> &labels,
			int color_scheme = vtkColorSeries::BREWER_QUALITATIVE_SET3);
	}
}

#endif // !OFEC_DEMO_CHART_H
