#include "geom_3d.h"
#include <utility/functional.h>
#include <vtkPolyData.h>
#include <vtkDelaunay2D.h>
#include <vtkElevationFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkLineSource.h>
#include <vtkTextProperty.h>
#include <vtkOutlineFilter.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkConeSource.h>

namespace ofec_demo::Geom3d {
	vtkActor* createSurface(
		const std::vector<std::array<ofec::Real, 3>> &pts, 
		vtkLookupTable* lut,
		double opacity)
	{
		auto points = vtkSmartPointer<vtkPoints>::New();
		auto poly_data = vtkSmartPointer<vtkPolyData>::New();
		auto delaunay = vtkSmartPointer<vtkDelaunay2D>::New();
		auto elevation_filter= vtkSmartPointer<vtkElevationFilter>::New();
		auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		auto actor = vtkActor::New();
		points->SetNumberOfPoints(pts.size());
		for (size_t i = 0; i < pts.size(); i++)
			points->SetPoint(i, pts[i][0], pts[i][1], pts[i][2]);
		double bounds[6];
		points->GetBounds(bounds);
		poly_data->SetPoints(points);
		delaunay->SetInputData(poly_data);
		elevation_filter->SetInputConnection(delaunay->GetOutputPort());
		elevation_filter->SetLowPoint(0.0, 0.0, bounds[4]);
		elevation_filter->SetHighPoint(0.0, 0.0, bounds[5]);
		mapper->SetInputConnection(elevation_filter->GetOutputPort());
		mapper->SetLookupTable(lut);
		actor->SetMapper(mapper);
		actor->GetProperty()->LightingOff();
		actor->GetProperty()->SetOpacity(opacity);
		return actor;
	}
	
	vtkActor* createPoints(
		const std::vector<std::array<ofec::Real, 3>> &pts, 
		float size, 
		double r, double g, double b, 
		double opacity) 
	{
		auto points = vtkSmartPointer<vtkPoints>::New();
		auto poly_data = vtkSmartPointer<vtkPolyData>::New();
		auto vertex_filter = vtkSmartPointer<vtkVertexGlyphFilter>::New();
		auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		auto actor = vtkActor::New();
		points->SetNumberOfPoints(pts.size());
		for (size_t i = 0; i < pts.size(); i++)
			points->SetPoint(i, pts[i][0], pts[i][1], pts[i][2]);
		poly_data->SetPoints(points);
		vertex_filter->SetInputData(poly_data);
		mapper->SetInputConnection(vertex_filter->GetOutputPort());
		actor->SetMapper(mapper);
		actor->GetProperty()->SetPointSize(size);
		actor->GetProperty()->SetColor(r, g, b);
		actor->GetProperty()->SetOpacity(opacity);
		return actor;
	}

	vtkActor *createGlyphs(
		const std::vector<std::array<ofec::Real, 3>> &pts, 
		vtkPolyDataAlgorithm *glyph, 
		double r, double g, double b, 
		double opacity)
	{
		auto points = vtkSmartPointer<vtkPoints>::New();
		auto poly_data = vtkSmartPointer<vtkPolyData>::New();
		auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		auto actor = vtkActor::New();
		points->SetNumberOfPoints(pts.size());
		for (size_t i = 0; i < pts.size(); i++)
			points->SetPoint(i, pts[i][0], pts[i][1], pts[i][2]);
		poly_data->SetPoints(points);
		glyph->SetInputData(poly_data);
		mapper->SetInputConnection(glyph->GetOutputPort());
		actor->SetMapper(mapper);
		actor->GetProperty()->SetColor(r, g, b);
		actor->GetProperty()->SetOpacity(opacity);
		return actor;
	}

	vtkActor* createPolyLine(
		const std::vector<std::array<ofec::Real, 3>> &pts, 
		double r, double g, double b, 
		double opacity) 
	{
		auto points = vtkSmartPointer<vtkPoints>::New();
		auto source = vtkSmartPointer<vtkLineSource>::New();
		auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		auto actor = vtkActor::New();
		points->SetNumberOfPoints(pts.size());
		for (size_t i = 0; i < pts.size(); i++)
			points->SetPoint(i, pts[i][0], pts[i][1], pts[i][2]);
		source->SetPoints(points);
		mapper->SetInputConnection(source->GetOutputPort());
		actor->SetMapper(mapper);
		actor->GetProperty()->SetColor(r, g, b);
		actor->GetProperty()->SetOpacity(opacity);
		return actor;
	}

	vtkActor* createCone(
		const std::array<ofec::Real, 3> &center, 
		const std::array<ofec::Real, 3> &direction,
		ofec::Real height, 
		ofec::Real radius,
		double r, double g, double b, 
		double opacity) 
	{
		auto source = vtkSmartPointer<vtkConeSource>::New();
		auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		auto actor = vtkActor::New();
		source->SetDirection(direction[0], direction[1], direction[2]);
		source->SetCenter(center[0], center[1], center[2]);
		source->SetResolution(20);
		source->SetHeight(height);
		source->SetRadius(radius);
		mapper->SetInputConnection(source->GetOutputPort());
		actor->SetMapper(mapper);
		actor->GetProperty()->SetDiffuseColor(r, g, b);
		actor->GetProperty()->SetOpacity(opacity);
		return actor;
	}

	vtkActor* createCuboidFrame(
		std::array<ofec::Real, 3> pts, 
		std::array<ofec::Real, 3> size, 
		double r, double g, double b, 
		double opacity) 
	{
		auto points = vtkSmartPointer<vtkPoints>::New();
		auto poly_data = vtkSmartPointer<vtkPolyData>::New();
		auto filter = vtkSmartPointer<vtkOutlineFilter>::New();
		auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		auto actor = vtkActor::New();
		points->SetNumberOfPoints(2);
		points->SetPoint(0, pts[0], pts[1], pts[2]);
		points->SetPoint(1, pts[0] + size[0], pts[1] + size[1], pts[2] + size[2]);
		poly_data->SetPoints(points);
		filter->SetInputData(poly_data);
		mapper->SetInputConnection(filter->GetOutputPort());
		actor->SetMapper(mapper);
		actor->GetProperty()->SetColor(r, g, b);
		actor->GetProperty()->SetOpacity(opacity);
		return actor;
	}

	vtkBillboardTextActor3D* createText(
		const std::array<ofec::Real, 3> &pos, 
		const std::string &text, 
		int size, 
		double r, double g, double b, 
		double opacity)
	{
		auto actor = vtkBillboardTextActor3D::New();
		actor->SetInput(text.c_str());
		actor->SetPosition(pos[0], pos[1], pos[2]);
		actor->GetTextProperty()->SetColor(r, g, b);
		actor->GetTextProperty()->SetFontSize(size);
		actor->GetTextProperty()->SetOpacity(opacity);
		return actor;
	}

	vtkActor *createColoredPoints(
		const std::vector<std::array<ofec::Real, 3>> &pts,
		vtkFloatArray *scalars,
		vtkLookupTable *lut,
		float size,
		double opacity)
	{
		auto points = vtkSmartPointer<vtkPoints>::New();
		auto poly_data = vtkSmartPointer<vtkPolyData>::New();
		auto vertex_filter = vtkSmartPointer<vtkVertexGlyphFilter>::New();
		auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		auto actor = vtkActor::New();
		points->SetNumberOfPoints(pts.size());
		for (size_t i = 0; i < pts.size(); i++)
			points->SetPoint(i, pts[i][0], pts[i][1], pts[i][2]);
		poly_data->SetPoints(points);
		poly_data->GetPointData()->SetScalars(scalars);
		vertex_filter->SetInputData(poly_data);
		mapper->SetInputConnection(vertex_filter->GetOutputPort());
		mapper->SetLookupTable(lut);
		mapper->UseLookupTableScalarRangeOn();
		actor->SetMapper(mapper);
		actor->GetProperty()->SetPointSize(size);
		actor->GetProperty()->SetOpacity(opacity);
		return actor;
	}

	vtkActor* createColoredGlyphs(
		const std::vector<std::array<ofec::Real, 3>> &pts, 
		vtkFloatArray *scalars, 
		vtkLookupTable *lut, 
		vtkPolyDataAlgorithm *glyph,
		double opacity)
	{
		auto points = vtkSmartPointer<vtkPoints>::New();
		auto poly_data = vtkSmartPointer<vtkPolyData>::New();
		auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		auto actor = vtkActor::New();
		points->SetNumberOfPoints(pts.size());
		for (size_t i = 0; i < pts.size(); i++)
			points->SetPoint(i, pts[i][0], pts[i][1], pts[i][2]);
		poly_data->SetPoints(points);
		poly_data->GetPointData()->SetScalars(scalars);
		glyph->SetInputData(poly_data);
		mapper->SetInputConnection(glyph->GetOutputPort());
		mapper->SetLookupTable(lut);
		mapper->UseLookupTableScalarRangeOn();
		actor->SetMapper(mapper);
		actor->GetProperty()->SetOpacity(opacity);
		return actor;
	}

	vtkLookupTable* createRainbowLuT(const std::vector<double> &values) {
		std::vector<int> sequence(values.size());
		ofec::mergeSort(values, values.size(), sequence, false);
		ofec::Real min_val = values[sequence.back()];
		ofec::Real max_val = values[sequence.front()];

		auto lut = vtkLookupTable::New();
		lut->SetTableRange(min_val, max_val);
		auto rainbow_colormap = vtkSmartPointer<vtkLookupTable>::New();
		rainbow_colormap->SetNumberOfTableValues(9);
		rainbow_colormap->SetTableRange(0, 8);
		rainbow_colormap->Build();
		size_t smpl_size = values.size();
		ofec::Real val, alpha = 1;
		size_t pre_pos = 0, cur_pos;
		std::vector<double> init_col = { 0.7,0,0,alpha }, col = { 0,0,0,alpha };
		double *pre_col = init_col.data(), *cur_col;
		int num_colors = 20000;
		lut->SetNumberOfColors(num_colors);
		for (size_t i = 0; i < 9; ++i) {
			val = values[sequence[(i + 1) * smpl_size / 9 - 1]];
			cur_pos = (max_val - val) / (max_val - min_val) * num_colors;
			cur_col = rainbow_colormap->GetTableValue(i);
			for (size_t indx = pre_pos; indx < cur_pos; ++indx) {
				for (size_t j = 0; j < 3; ++j)
					col[j] = pre_col[j] + (indx - pre_pos) / double(cur_pos - pre_pos) * (cur_col[j] - pre_col[j]);
				lut->SetTableValue(num_colors - indx - 1, col[0], col[1], col[2]);
			}
			pre_pos = cur_pos;
			for (size_t j = 0; j < 3; ++j)
				pre_col[j] = cur_col[j];
		}
		lut->Build();
		return lut;
	}
}