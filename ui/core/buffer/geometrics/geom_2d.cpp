#include "geom_2d.h"
#include <QPainter>

namespace ofec_demo {
	Points2d::Points2d(const std::vector<QPointF> &pts, qreal size, QColor color) :
		m_pen(color), m_pts(pts), m_bnd_rect(pts.front().x(), pts.front().y(), 0, 0)
	{
		m_pen.setWidthF(size);
		for (size_t i = 1; i < pts.size(); i++) {
			if (m_bnd_rect.x() > pts[i].x())
				m_bnd_rect.setX(pts[i].x());
			if (m_bnd_rect.y() > pts[i].y())
				m_bnd_rect.setY(pts[i].y());
			if (m_bnd_rect.x() + m_bnd_rect.width() < pts[i].x())
				m_bnd_rect.setWidth(pts[i].x() - m_bnd_rect.x());
			if (m_bnd_rect.y() + m_bnd_rect.height() < pts[i].y())
				m_bnd_rect.setHeight(pts[i].y() - m_bnd_rect.y());
		}
		m_bnd_rect.setWidth(m_bnd_rect.width() + size);
		m_bnd_rect.setHeight(m_bnd_rect.height() + size);
	}

	void Points2d::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
		auto scaleFactor = painter->transform().m11();
		auto pen = m_pen;
		pen.setWidthF(pen.widthF() / scaleFactor);
		painter->setPen(pen);
		painter->drawPoints(m_pts.data(), m_pts.size());
	}

	Rect2d::Rect2d(
		qreal x, qreal y, qreal w, qreal h,
		QColor fill_color, Qt::PenStyle frame_style, QColor frame_color, qreal frame_width) :
		QGraphicsRectItem(x, y, w, h)
	{
		auto pen = this->pen();
		pen.setColor(frame_color);
		pen.setStyle(frame_style);
		pen.setWidthF(frame_width);
		setPen(pen);
		auto brush = this->brush();
		brush.setStyle(Qt::SolidPattern);
		brush.setColor(fill_color);
		setBrush(brush);
	}

	void Rect2d::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
		auto scaleFactor = painter->transform().m11();
		auto pen = this->pen();
		pen.setWidthF(pen.widthF() / scaleFactor);
		painter->setPen(pen);
		painter->setBrush(this->brush());
		painter->drawRect(this->rect());
	}

	void Rect2d::setFillColor(QColor color) {
		auto brush = this->brush();
		brush.setColor(color);
		setBrush(brush);
	}

	Text2d::Text2d(
		qreal x, qreal y, qreal w, qreal h,
		const QString &text, QFont font, QColor color,
		bool keep_size, qreal angle, int flags) :
		QGraphicsRectItem(x, y, w, h),
		m_font(font), m_text(text), m_pen(color), m_flags(flags), m_keep_size(keep_size)
	{
		setRotation(angle);
	}

	void Text2d::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
		auto scaleFactor = painter->transform().m11();
		if (m_keep_size) {
			auto font = m_font;
			font.setPointSizeF(font.pointSizeF() / scaleFactor);
			painter->setFont(font);
		}
		else
			painter->setFont(m_font);
		painter->setPen(m_pen);
		painter->drawText(this->rect(), m_flags, m_text);
	}

	PolyLine2d::PolyLine2d(const std::vector<QPointF> &pts, qreal size, QColor color) :
		m_pen(color), m_pts(pts), m_bnd_rect(pts.front().x(), pts.front().y(), 0, 0)
	{
		m_pen.setWidthF(size);
		for (size_t i = 1; i < pts.size(); i++) {
			if (m_bnd_rect.x() > pts[i].x())
				m_bnd_rect.setX(pts[i].x());
			if (m_bnd_rect.y() > pts[i].y())
				m_bnd_rect.setY(pts[i].y());
			if (m_bnd_rect.x() + m_bnd_rect.width() < pts[i].x())
				m_bnd_rect.setWidth(pts[i].x() - m_bnd_rect.x());
			if (m_bnd_rect.y() + m_bnd_rect.height() < pts[i].y())
				m_bnd_rect.setHeight(pts[i].y() - m_bnd_rect.y());
		}
		m_bnd_rect.setWidth(m_bnd_rect.width() + size);
		m_bnd_rect.setHeight(m_bnd_rect.height() + size);
	}

	void PolyLine2d::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
		painter->setRenderHint(QPainter::Antialiasing);
		auto scaleFactor = painter->transform().m11();
		auto pen = m_pen;
		pen.setWidthF(pen.widthF() / scaleFactor);
		painter->setPen(pen);
		painter->drawPolyline(m_pts.data(), m_pts.size());
	}

	const double Arrow2d::ms_pi(std::acos(-1.0));

	Arrow2d::Arrow2d(
		qreal x1, qreal y1, qreal x2, qreal y2,
		bool tail, bool head, Qt::PenStyle style, QColor color, qreal width) :
		QGraphicsLineItem(x1, y1, x2, y2), m_brush(color), m_tail(tail), m_head(head)
	{
		auto pen = this->pen();
		pen.setWidth(width);
		pen.setColor(color);
		pen.setStyle(style);
	}

	void Arrow2d::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
		painter->setRenderHint(QPainter::Antialiasing);
		auto scaleFactor = painter->transform().m11();
		auto pen = this->pen();
		pen.setWidthF(pen.widthF() / scaleFactor);
		painter->setPen(pen);
		painter->drawLine(this->line());
		if (m_tail || m_head) {
			painter->setBrush(m_brush);
			auto angle = -std::atan2(line().dx(), line().dy());
			const double sz = 10.0 / scaleFactor; //pixels
			if (m_tail) {
				const QPointF p0 = this->line().p1();
				const QPointF p1 = p0 + QPointF(std::sin(angle + ms_pi + (ms_pi * 0.1)) * sz,
					-std::cos(angle + ms_pi + (ms_pi * 0.1)) * sz);
				const QPointF p2 = p0 + QPointF(std::sin(angle + ms_pi - (ms_pi * 0.1)) * sz,
					-std::cos(angle + ms_pi - (ms_pi * 0.1)) * sz);
				painter->drawPolygon(QPolygonF() << p0 << p1 << p2);
			}
			if (m_head) {
				const QPointF p0 = this->line().p2();
				const QPointF p1 = p0 + QPointF(std::sin(angle + 0.0 + (ms_pi * 0.1)) * sz,
					-std::cos(angle + 0.0 + (ms_pi * 0.1)) * sz);
				const QPointF p2 = p0 + QPointF(std::sin(angle + 0.0 - (ms_pi * 0.1)) * sz,
					-std::cos(angle + 0.0 - (ms_pi * 0.1)) * sz);
				painter->drawPolygon(QPolygonF() << p0 << p1 << p2);
			}
		}
	}

	Matrix2d::Matrix2d(Qt::BrushStyle brush_style, Qt::PenStyle frame_style, qreal frame_width) :
		QGraphicsRectItem(-5, -5, 10, 10)
	{
		auto pen = this->pen();
		pen.setStyle(frame_style);
		pen.setWidthF(frame_width);
		setPen(pen);
		auto brush = this->brush();
		brush.setStyle(brush_style);
		setBrush(brush);
	}

	void Matrix2d::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
		if (m_fill_color.empty() || m_fill_color.front().empty())
			return;

		int max_x(m_fill_color.size()), max_y(0);
		for (auto &it : m_fill_color) {
			max_y = std::max<int>(max_y, it.size());
		}
		qreal span = this->rect().width() / std::max(max_x, max_y);
		qreal from_x = -span * max_x / 2.0;
		qreal from_y = -span * max_y / 2.0;
		auto scaleFactor = painter->transform().m11();
		auto pen = this->pen();
		pen.setWidthF(pen.widthF() / scaleFactor);
		painter->setPen(pen);
		auto brush = this->brush();
		painter->setBrush(brush);
		painter->drawRect(this->rect());

		for (int x(0); x < max_x; ++x) {
			for (int y(0); y < max_y; ++y) {
				pen.setColor(m_frame_color[x][y]);
				brush.setColor(m_fill_color[x][y]);
				painter->setPen(pen);
				painter->setBrush(brush);
				painter->drawRect(QRectF(x * span + from_x, y * span + from_y, span, span));
			}
		}
	}

	Circle2d::Circle2d(qreal x, qreal y, qreal r,
		QColor fill_color, Qt::PenStyle frame_style, QColor frame_color, qreal frame_width) :
		QGraphicsEllipseItem(x, y, r, r)
	{
		auto pen = this->pen();
		pen.setColor(frame_color);
		pen.setStyle(frame_style);
		pen.setWidthF(frame_width);
		setPen(pen);
		auto brush = this->brush();
		brush.setStyle(Qt::SolidPattern);
		brush.setColor(fill_color);
		setBrush(brush);
	}

	void Circle2d::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
		painter->setRenderHint(QPainter::Antialiasing);
		auto scaleFactor = painter->transform().m11();
		auto pen = this->pen();
		pen.setWidthF(pen.widthF() / scaleFactor);
		painter->setPen(pen);
		painter->setBrush(this->brush());
		painter->drawEllipse(this->rect());
	}
}