#ifndef OFEC_DEMO_GEOM_3D_H
#define OFEC_DEMO_GEOM_3D_H

#include <vtkActor.h>
#include <vtkLookupTable.h>
#include <vtkFloatArray.h>
#include <vtkBillboardTextActor3D.h>
#include <vtkPolyDataAlgorithm.h>
#include <core/definition.h>
#include <vector>
#include <array>
#include <string>

#include <vtkGlyph3D.h>

namespace ofec_demo::Geom3d {
	vtkActor* createSurface(
		const std::vector<std::array<ofec::Real, 3>> &pts, 
		vtkLookupTable* lut, 
		double opacity = 1);

	vtkActor* createPoints(
		const std::vector<std::array<ofec::Real, 3>> &pts, 
		float size, 
		double r, double g, double b, 
		double opacity = 1);

	vtkActor* createGlyphs(
		const std::vector<std::array<ofec::Real, 3>> &pts,
		vtkPolyDataAlgorithm *glyph,
		double r, double g, double b,
		double opacity = 1);

	vtkActor* createPolyLine(
		const std::vector<std::array<ofec::Real, 3>> &pts, 
		double r, double g, double b, 
		double opacity = 1);

	vtkActor* createCone(
		const std::array<ofec::Real, 3> &center,
		const std::array<ofec::Real, 3> &direction,
		ofec::Real height, 
		ofec::Real radius,
		double r, double g, double b, 
		double opacity = 1);

	vtkActor* createCuboidFrame(
		std::array<ofec::Real, 3> pts, 
		std::array<ofec::Real, 3> size, 
		double r, double g, double b, 
		double opacity = 1);

	vtkBillboardTextActor3D* createText(
		const std::array<ofec::Real, 3> &pos, 
		const std::string &text, 
		int size, 
		double r, double g, double b, 
		double opacity = 1);

	vtkActor* createColoredPoints(
		const std::vector<std::array<ofec::Real, 3>> &pts,
		vtkFloatArray *scalars,
		vtkLookupTable *lut,
		float size,
		double opacity = 1);

	vtkActor* createColoredGlyphs(
		const std::vector<std::array<ofec::Real, 3>> &pts,
		vtkFloatArray *scalars,
		vtkLookupTable *lut,
		vtkPolyDataAlgorithm *glyph,
		double opacity = 1);

	vtkLookupTable* createRainbowLuT(const std::vector<double> &values);
}

#endif // !OFEC_DEMO_GEOM_3D_H
