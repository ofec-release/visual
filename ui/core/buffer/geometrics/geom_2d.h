#ifndef OFEC_DEMO_GEOME_2D_H
#define OFEC_DEMO_GEOME_2D_H

#include <vector>
#include <QGraphicsRectItem>
#include <QGraphicsLineItem>
#include <QFont>
#include <QPen>
#include <QBrush>

namespace ofec_demo {
	class Points2d : public QGraphicsItem {
	private:
		std::vector<QPointF> m_pts;
		QPen m_pen;
		QRectF m_bnd_rect;
	public:
		Points2d(const std::vector<QPointF> &pts, qreal size, QColor color);
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
		QRectF boundingRect() const override { return m_bnd_rect; }
		void setColor(QColor color) { m_pen.setColor(color); }
		size_t numPoints() const { return m_pts.size(); }
		const QPointF &getPoint(size_t id) const { return m_pts[id]; }
		void setPoint(size_t id, const QPointF &pt) { m_pts[id] = pt; }
	};

	class Text2d : public QGraphicsRectItem {
	private:
		QFont m_font;
		QString m_text;
		QPen m_pen;
		int m_flags;
		bool m_keep_size;
	public:
		Text2d(
			qreal x, qreal y, qreal w, qreal h,
			const QString &text, QFont font, QColor color = Qt::black,
			bool keep_size = true, qreal angle = 0, int flags = Qt::AlignCenter);
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
		void setText(const QString &text) { m_text = text; }
	};

	class Rect2d : public QGraphicsRectItem {
	public:
		Rect2d(
			qreal x, qreal y, qreal w, qreal h,
			QColor fill_color = Qt::transparent, Qt::PenStyle frame_style = Qt::SolidLine, QColor frame_color = Qt::black, qreal frame_width = 1);
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
		void setFillColor(QColor color);
	};

	class PolyLine2d : public QGraphicsItem {
	private:
		std::vector<QPointF> m_pts;
		QPen m_pen;
		QRectF m_bnd_rect;
	public:
		PolyLine2d(const std::vector<QPointF> &pts, qreal size, QColor color);
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
		QRectF boundingRect() const override { return m_bnd_rect; }
	};

	class Arrow2d : public QGraphicsLineItem {
	private:
		QBrush m_brush;
		bool m_tail, m_head;
		static const double ms_pi;
	public:
		Arrow2d(
			qreal x1, qreal y1, qreal x2, qreal y2,
			bool tail = false, bool head = true,
			Qt::PenStyle style = Qt::SolidLine, QColor color = Qt::black, qreal width = 1);
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
	};

	class Matrix2d : public QGraphicsRectItem {
	private:
		std::vector<std::vector<QColor>> m_fill_color;
		std::vector<std::vector<QColor>> m_frame_color;
	public:
		Matrix2d(Qt::BrushStyle brush_style = Qt::SolidPattern,
			Qt::PenStyle frame_style = Qt::SolidLine, qreal frame_width = 1);
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
		void setFillColor(const std::vector<std::vector<QColor>> &fillColor) { m_fill_color = fillColor; update(); }
		void setFrameColor(const std::vector<std::vector<QColor>> &frameColor) { m_frame_color = frameColor; update(); }
	};

	class Circle2d : public QGraphicsEllipseItem {
	public:
		Circle2d(
			qreal x, qreal y, qreal r,
			QColor fill_color = Qt::transparent, Qt::PenStyle frame_style = Qt::SolidLine, QColor frame_color = Qt::black, qreal frame_width = 1);
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
	};
}

#endif // !OFEC_DEMO_GEOME_2D_H
