#ifndef OFEC_DEMO_ITEMS_3D_PRO_H
#define OFEC_DEMO_ITEMS_3D_PRO_H

#include <vtkActor.h>
#include <vtkSmartPointer.h>
#include <vtkBillboardTextActor3D.h>
#include <vtkLookupTable.h>
#include <vtkRenderer.h>
#include <core/definition.h>
#include <vector>
#include <utility>

namespace ofec_demo {
	class ObjSpace3d {
	private:
		vtkSmartPointer<vtkActor> m_axis_f1_line, m_axis_f2_line, m_axis_f3_line;
		vtkSmartPointer<vtkActor> m_axis_f1_cone, m_axis_f2_cone, m_axis_f3_cone;
		vtkSmartPointer<vtkBillboardTextActor3D> m_axis_f1_label, m_axis_f2_label, m_axis_f3_label;
		vtkSmartPointer<vtkActor> m_boundary, m_center;
		vtkSmartPointer<vtkActor> m_optima;

	public:
		void updateBoundary(const std::vector<std::pair<ofec::Real, ofec::Real>> &range);
		void addBoundary(vtkRenderer *renderer);
		void removeBoundary(vtkRenderer *renderer);

		void updateOptima(
			const std::vector<std::vector<ofec::Real>> &optima,
			const std::vector<std::pair<ofec::Real, ofec::Real>> &range);
		void addOptima(vtkRenderer *renderer);
		void removeOptima(vtkRenderer *renderer);

		void setOptimaVisible(bool flag);
	};
}

#endif // !OFEC_DEMO_ITEMS_3D_PRO_H