#ifndef OFEC_DEMO_BUFFER_PRO_H
#define OFEC_DEMO_BUFFER_PRO_H

#include <core/definition.h>
#include "../../core/window/graphics_area.h"
#include "graphic_view/graphic_view_2d.h"
#include "graphic_view/graphic_view_3d.h"
#include "items_2d_pro.h"
#include "items_3d_pro.h"

namespace ofec_demo {
	/* Buffer of Problem */
	class BufferPro {
	protected:
		int m_id_pro;
		GraphicsArea *m_graphics_area;

		std::vector<std::vector<ofec::Real>> m_optima_obj;
		std::vector<std::pair<ofec::Real, ofec::Real>> m_range_obj;

	private:
		std::unique_ptr<GraphicView2d> m_obj_space_view_2d;			/* 2D view for objective space (2D obj problem)*/
		std::unique_ptr<GraphicView3d> m_obj_space_view_3d;			/* 3D view for objective space (3D obj problem)*/
		struct BufferDatum {
			std::unique_ptr<ObjSpace2d> obj_space_2d;	 	/* 2D items for objective space (2D obj problem)*/
			std::unique_ptr<ObjSpace3d> obj_space_3d;	 	/* 3D items for objective space (3D obj problem)*/
		};
		std::map<int, std::unique_ptr<BufferDatum>> m_buffer_data;
		BufferDatum *m_cur_datum;

	public:
		BufferPro(GraphicsArea *graphic_dock_set, int id_pro);
		virtual ~BufferPro();
		int idPro()const { return m_id_pro; }

		virtual void initializeGraphicViews();
		virtual bool graphicViewsInitialized();
		virtual void appendBuffer(int max_buffer_id); 
		virtual void popupBuffer(int buffer_id);	
		virtual void updateGraphicViews(int buffer_id);

		virtual void updateViews();
		virtual void setOptimaVisible(bool flag);

		QGraphicsScene *objSpace2dScene() { return m_obj_space_view_2d->scene(); }
		vtkRenderer *objSpace3dRenderer() { return m_obj_space_view_3d->renderer(); }

		const std::vector<std::pair<ofec::Real, ofec::Real>>& rangeObj() { return m_range_obj; }

	private:
		void updateObjSpace2d(BufferDatum *buffer_datum);
		void updateObjSpace3d(BufferDatum *buffer_datum);
		void removeCurDatum();
		void addNewDatum(int buffer_id);
	};
}

#endif // !OFEC_DEMO_BUFFER_PRO_H