#include "items_2d_ea.h"
#include <QGraphicsScene>
#include "../global_ui.h"
#include "items_2d_pro.h"

namespace ofec_demo {
	void PopObjSpace2d::updateMultiPops(
		const std::vector<std::vector<ofec::Real>> &mp,
		const std::vector<int> &pi, int np, 
		const std::vector<std::pair<ofec::Real, ofec::Real>> &range)
	{
		QPointF point;
		std::vector<std::vector<QPointF>> pops_points(np);
		for (size_t i = 0; i < mp.size(); ++i) {
			point.setX(((mp[i][0] - range[0].first) / (range[0].second - range[0].first) * 2 - 1) * ObjSpace2d::ms_lenth);
			point.setY((1 - (mp[i][1] - range[1].first) / (range[1].second - range[1].first) * 2) * ObjSpace2d::ms_lenth);
			pops_points[pi[i]].push_back(point);
		}
		for (size_t i = 0; i < np; ++i) {
			auto &c = g_rand_colors[i % g_num_rand_colors];
			m_multi_pop.emplace_back(new Points2d(pops_points[i], 4, QColor(c[0] * 255, c[1] * 255, c[2] * 255)));
		}
	}

	void PopObjSpace2d::addMultiPops(QGraphicsScene *scene) {
		for (auto &pop : m_multi_pop)
			scene->addItem(pop.get());
	}

	void PopObjSpace2d::removeMultiPops(QGraphicsScene *scene) {
		for (auto &pop : m_multi_pop)
			scene->removeItem(pop.get());
	}

	void PopObjSpace2d::setMultiPopsVisible(bool flag) {
		for (auto &pop : m_multi_pop)
			pop->setVisible(flag);
	}
}