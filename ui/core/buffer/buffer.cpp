#include "buffer.h"
#include "../../core/global_ui.h"
#include <core/instance_manager.h>
#include <thread>
#include <chrono>

namespace ofec_demo {
	Buffer::Buffer() :
		m_max_frame_id(-1),
		m_cur_frame_id(-1),
		m_max_num_later_frames(10),
		m_max_alg_buffer_id(-1),
		m_max_pro_buffer_id(-1),
		m_cur_alg_buffer_id(-1),
		m_cur_pro_buffer_id(-1) {}

	void Buffer::releaseBufferAlg() {
		using namespace ofec;
		if (!m_buffer_alg)
			return;

		GET_PRO(m_buffer_pro->idPro()).initialize();
		for (int pro_buffer_id = 1; pro_buffer_id <= m_max_pro_buffer_id; pro_buffer_id++)
			m_buffer_pro->popupBuffer(pro_buffer_id);
		m_max_frame_id = 0;
		m_max_pro_buffer_id = 0;
		m_frame_id_to_pro_buffer_id.clear();
		m_frame_id_to_pro_buffer_id[m_max_frame_id] = m_max_pro_buffer_id;

		int id_alg = m_buffer_alg->idAlg();
		int id_param = GET_ALG(id_alg).idParamMap();
		m_buffer_alg.reset();
		DEL_ALG(id_alg);
		DEL_PARAM(id_param);
		m_max_alg_buffer_id = -1;
		m_frame_id_to_alg_buffer_id.clear();
		m_frame_id_to_alg_buffer_id[m_max_frame_id] = m_max_alg_buffer_id;
	}

	void Buffer::releaseBufferPro() {
		using namespace ofec;
		if (!m_buffer_pro)
			return;
		if (m_buffer_alg)
			releaseBufferAlg();
		int id_pro = m_buffer_pro->idPro();
		int id_param = GET_PRO(id_pro).idParamMap();
		m_buffer_pro.reset();
		DEL_PRO(id_pro);
		DEL_PARAM(id_param);
		m_max_frame_id = -1;
		m_cur_frame_id = -1;
		m_max_pro_buffer_id = -1;
		m_cur_pro_buffer_id = -1;
		m_frame_id_to_pro_buffer_id.clear();
		m_frame_id_to_alg_buffer_id.clear();
		m_frame_id_to_num_evals.clear();
	}

	void Buffer::appendAlgBuffer(int id_alg) {
		using namespace std::chrono_literals;
		if (id_alg != m_buffer_alg->idAlg()) 
			return;
		while (!g_term_alg && (g_pause || m_cur_frame_id + m_max_num_later_frames < m_max_frame_id))
			std::this_thread::sleep_for(10ms);
		m_buffer_alg2pro_id[m_max_alg_buffer_id+1] = m_max_pro_buffer_id;
		m_buffer_alg->appendBuffer(m_max_alg_buffer_id);
		m_max_alg_buffer_id++;
		m_frame_id_alg_pro_change[m_max_frame_id] = true;

		increaseMaxFrameId();
	}
	
	void Buffer::appendProBuffer(int id_pro) {
		using namespace std::chrono_literals;
		if (id_pro != m_buffer_pro->idPro()) 
			return;
		while (!g_term_alg && (g_pause || m_cur_frame_id + m_max_num_later_frames < m_max_frame_id))
			std::this_thread::sleep_for(10ms);
		m_buffer_pro->appendBuffer(m_max_pro_buffer_id);
		m_max_pro_buffer_id++;
		m_frame_id_alg_pro_change[m_max_frame_id] = false;
		increaseMaxFrameId();
	}

	void Buffer::updateGraphicViews(int frame_id) {
		if (m_cur_frame_id == frame_id)
			return;
		int pro_buffer_id = m_frame_id_to_pro_buffer_id[frame_id];
		if (pro_buffer_id != m_cur_pro_buffer_id) {
			if (!m_buffer_pro->graphicViewsInitialized())
				m_buffer_pro->initializeGraphicViews();
			m_buffer_pro->updateGraphicViews(pro_buffer_id);
			m_buffer_pro->updateViews();
			m_cur_pro_buffer_id = pro_buffer_id;
		}
		int alg_buffer_id = m_frame_id_to_alg_buffer_id[frame_id];
		if (alg_buffer_id != m_cur_alg_buffer_id) {
			if (!m_buffer_alg->graphicViewsInitialized())
				m_buffer_alg->initializeGraphicViews();
			m_buffer_alg->updateGraphicViews(alg_buffer_id);
			m_buffer_alg->updateViews();
			m_cur_alg_buffer_id = alg_buffer_id;
		}
		m_cur_frame_id = frame_id;
	}

	void Buffer::updateViews() {
		if (m_buffer_alg)
			m_buffer_alg->updateViews();
		else if (m_buffer_pro)
			m_buffer_pro->updateViews();
	}

	void Buffer::setSolutionVisible(bool flag) {
		if (m_buffer_alg) {
			m_buffer_alg->setSolutionVisible(flag);
			m_buffer_alg->updateViews();
		}
	}

	void Buffer::setOptimaVisible(bool flag) {
		if (m_buffer_pro) {
			m_buffer_pro->setOptimaVisible(flag);
			m_buffer_pro->updateViews();
		}
	}
	
	void Buffer::increaseMaxFrameId() {
		using namespace ofec;
		m_max_frame_id++;
		m_frame_id_to_alg_buffer_id[m_max_frame_id] = m_max_alg_buffer_id;
		m_frame_id_to_pro_buffer_id[m_max_frame_id] = m_max_pro_buffer_id;
		if (m_buffer_alg)
			m_frame_id_to_num_evals[m_max_frame_id] = GET_ALG(m_buffer_alg->idAlg()).numEffectiveEvals();
		else
			m_frame_id_to_num_evals[m_max_frame_id] = 0;
		emit maxFrameIdIncreased(m_max_frame_id);
	}
	
	void Buffer::onMinFrameIdIncreased(int frame_id) {
		if (frame_id == 0)
			return;
		int alg_buffer_id = m_frame_id_to_alg_buffer_id[frame_id - 1];
		if (alg_buffer_id > -1 && m_frame_id_alg_pro_change[frame_id])
		{
			m_buffer_alg->popupBuffer(alg_buffer_id);
			m_buffer_alg2pro_id.erase(alg_buffer_id);
		}
			

		int pro_buffer_id = m_frame_id_to_pro_buffer_id[frame_id - 1];
		if (pro_buffer_id > 0 && !m_frame_id_alg_pro_change[frame_id])
			m_buffer_pro->popupBuffer(pro_buffer_id);

		if (frame_id > 1) {
			m_frame_id_to_alg_buffer_id.erase(frame_id - 1);
			m_frame_id_to_pro_buffer_id.erase(frame_id - 1);
			m_frame_id_to_num_evals.erase(frame_id - 1);
			m_frame_id_alg_pro_change.erase(frame_id);
		}
	}

	void Buffer::onCurFrameIdModified(int frame_id) {
		updateGraphicViews(frame_id);
	}
}
