#ifndef OFEC_DEMO_GRAPHIC_VIEW_2D_H
#define OFEC_DEMO_GRAPHIC_VIEW_2D_H

#include "graphic_view.h"
#include <QGraphicsScene>
#include <QGraphicsView>

namespace ofec_demo {
    class ZoomableGraphicsView : public QGraphicsView {
    public:
        ZoomableGraphicsView();
    protected:
        void wheelEvent(QWheelEvent *event) override;
    };

    class GraphicView2d : public graphicView {
    public:
        GraphicView2d();
        void saveFigure() override;
        void resetCamera() override;
        void updateView() override;

        QGraphicsScene *scene() { return m_scene; }  

    protected:
        QGraphicsScene *m_scene;
    };
}

#endif // !OFEC_DEMO_GRAPHIC_VIEW_2D_H
