#include "graphic_view_3d.h"
#include <QVTKOpenGLStereoWidget.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkGL2PSExporter.h>
#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>
#include <vtkCamera.h>
#include <QFileDialog>

namespace ofec_demo {
	GraphicView3d::GraphicView3d() {
		m_renderer = vtkSmartPointer<vtkRenderer>::New();
		m_renderer->SetBackground(1, 1, 1);
		//m_renderer->GetActiveCamera()->SetParallelProjection(true);    // uncomment this line to enable parallel projection
		auto window = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
		window->AddRenderer(m_renderer);
		m_graphic_widget = new QVTKOpenGLStereoWidget;
		//m_graphic_widget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
		dynamic_cast<QVTKOpenGLStereoWidget *>(m_graphic_widget)->setRenderWindow(window);
		m_bounds = { -5, 5, -5, 5,-5, 5 };
	}

	void GraphicView3d::saveFigure() {
		auto file_name = QFileDialog::getSaveFileName(this, "Save Image", "", "PDF files (*.pdf);; PNG files (*.png)");
		if (file_name.contains(".pdf")) {
			auto exporter = vtkSmartPointer<vtkGL2PSExporter>::New();
			exporter->SetFileFormatToPDF();
			exporter->SetRenderWindow(dynamic_cast<QVTKOpenGLStereoWidget*>(m_graphic_widget)->renderWindow());
			exporter->SetFilePrefix(file_name.left(file_name.size()-4).toStdString().c_str());
			exporter->Write();
		}
		else if (file_name.contains(".png")) {
			auto filter = vtkSmartPointer<vtkWindowToImageFilter>::New();
			filter->SetInput(dynamic_cast<QVTKOpenGLStereoWidget *>(m_graphic_widget)->renderWindow());
			filter->SetScale(2); // image quality
			filter->SetInputBufferTypeToRGBA(); // also record the alpha (transparency) channel
			filter->ReadFrontBufferOff();       // read from the back buffer
			filter->Update();
			auto writer = vtkSmartPointer<vtkPNGWriter>::New();
			writer->SetFileName(file_name.toStdString().c_str());
			writer->SetInputConnection(filter->GetOutputPort());
			writer->Write();
			dynamic_cast<QVTKOpenGLStereoWidget*>(m_graphic_widget)->renderWindow()->Render();
		}
	}

	void GraphicView3d::resetCamera() {
		m_renderer->ResetCamera(m_bounds.data());
		dynamic_cast<QVTKOpenGLStereoWidget*>(m_graphic_widget)->renderWindow()->Render();
	}

	void GraphicView3d::updateView() {
		dynamic_cast<QVTKOpenGLStereoWidget*>(m_graphic_widget)->renderWindow()->Render();
	}
}