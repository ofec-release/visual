#ifndef OFEC_DEMO_CHART_VIEW_2D_H
#define OFEC_DEMO_CHART_VIEW_2D_H

#include <vtkContextView.h>
#include <vtkContextScene.h>
#include "graphic_view.h"

namespace ofec_demo {
	class ChartView2d : public graphicView {
	protected:
		vtkSmartPointer<vtkContextView> m_context_view;
	public:
		ChartView2d();
		vtkContextScene* scene() { return m_context_view->GetScene(); }
		void saveFigure() override;
		void resetCamera() override;
		void updateView() override;
	};
}

#endif // !OFEC_DEMO_CHART_VIEW_2D_H
