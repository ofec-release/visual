#ifndef OFEC_DEMO_MODEL_VIEW_3D_H
#define OFEC_DEMO_MODEL_VIEW_3D_H

#include <string>
#include "graphic_view.h"

namespace ofec_demo {
	class ModelView3d : public graphicView {
	public:
		ModelView3d();
		void importObjModel(const std::string &path, const std::string &name);
		void saveFigure() override {}
		void resetCamera() override {}
		void updateView() override {}
	};
}

#endif // !OFEC_DEMO_MODEL_VIEW_3D_H
