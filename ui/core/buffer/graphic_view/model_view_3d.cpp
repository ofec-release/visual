#include "model_view_3d.h"
#include <vtkRenderer.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkNamedColors.h>
#include <vtkOBJImporter.h>
#include <QVTKOpenGLStereoWidget.h>

namespace ofec_demo {
	ModelView3d::ModelView3d() {
		auto colors = vtkSmartPointer<vtkNamedColors>::New();
		auto renderer = vtkSmartPointer<vtkRenderer>::New();
		renderer->SetBackground2(colors->GetColor3d("Silver").GetData());
		renderer->SetBackground(colors->GetColor3d("Gold").GetData());
		renderer->GradientBackgroundOn();
		auto window = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
		window->AddRenderer(renderer);
		m_graphic_widget = new QVTKOpenGLStereoWidget;
		dynamic_cast<QVTKOpenGLStereoWidget*>(m_graphic_widget)->setRenderWindow(window);
	}
	
	void ModelView3d::importObjModel(const std::string &path, const std::string &name) {
		auto importer = vtkSmartPointer<vtkOBJImporter>::New();
		importer->SetFileName((path + "/" + name + ".obj").c_str());
		importer->SetFileNameMTL((path + "/" + name + ".mtl").c_str());
		importer->SetTexturePath(path.c_str());
		importer->SetRenderWindow(dynamic_cast<QVTKOpenGLStereoWidget *>(m_graphic_widget)->renderWindow());
		importer->Update();
	}
}