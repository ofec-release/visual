#include "chart_view_2d.h"
#include <vtkGL2PSExporter.h>
#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>
#include <vtkRenderWindow.h>
#include <QVTKOpenGLStereoWidget.h>
#include <QFileDialog>

namespace ofec_demo {
	ChartView2d::ChartView2d() {
		m_context_view = vtkSmartPointer<vtkContextView>::New();
		m_graphic_widget = new QVTKOpenGLStereoWidget;
		m_context_view->SetRenderWindow(dynamic_cast<QVTKOpenGLStereoWidget *>(m_graphic_widget)->renderWindow());
	}
	
	void ChartView2d::saveFigure() {
		auto file_name = QFileDialog::getSaveFileName(this, "Save Image", "", "PDF files (*.pdf);; PNG files (*.png)");
		if (file_name.contains(".pdf")) {
			auto exporter = vtkSmartPointer<vtkGL2PSExporter>::New();
			exporter->SetFileFormatToPDF();
			exporter->SetRenderWindow(dynamic_cast<QVTKOpenGLStereoWidget *>(m_graphic_widget)->renderWindow());
			exporter->SetFilePrefix(file_name.left(file_name.size() - 4).toStdString().c_str());
			exporter->Write();
		}
		else if (file_name.contains(".png")) {
			auto filter = vtkSmartPointer<vtkWindowToImageFilter>::New();
			filter->SetInput(dynamic_cast<QVTKOpenGLStereoWidget *>(m_graphic_widget)->renderWindow());
			filter->SetScale(2); // image quality
			filter->SetInputBufferTypeToRGBA(); // also record the alpha (transparency) channel
			filter->ReadFrontBufferOff();       // read from the back buffer
			filter->Update();
			auto writer = vtkSmartPointer<vtkPNGWriter>::New();
			writer->SetFileName(file_name.toStdString().c_str());
			writer->SetInputConnection(filter->GetOutputPort());
			writer->Write();
			dynamic_cast<QVTKOpenGLStereoWidget *>(m_graphic_widget)->renderWindow()->Render();
		}
	}

	void ChartView2d::resetCamera() {
		dynamic_cast<QVTKOpenGLStereoWidget *>(m_graphic_widget)->renderWindow()->Render();
	}

	void ChartView2d::updateView() {
		dynamic_cast<QVTKOpenGLStereoWidget *>(m_graphic_widget)->renderWindow()->Render();
	}
}