#ifndef OFEC_DEMO_GRAPHIC_VIEW_3D_H
#define OFEC_DEMO_GRAPHIC_VIEW_3D_H

#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vector>
#include "graphic_view.h"

namespace ofec_demo {
	class GraphicView3d : public graphicView {
	protected:
		vtkSmartPointer<vtkRenderer> m_renderer;
		std::vector<double> m_bounds;
	public:
		GraphicView3d();
		void saveFigure() override;
		void resetCamera() override;
		void updateView() override;
		vtkRenderer* renderer() { return m_renderer; }
	};
}

#endif // !OFEC_DEMO_GRAPHIC_VIEW_3D_H
