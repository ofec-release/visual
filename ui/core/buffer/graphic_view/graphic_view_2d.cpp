#include "graphic_view_2d.h"
#include <QWheelEvent>
#include <QPdfWriter>
#include <QImage>
#include <QGuiApplication>
#include <QScreen>
#include <QFileDialog>

namespace ofec_demo {
    ZoomableGraphicsView::ZoomableGraphicsView() {
        setDragMode(QGraphicsView::ScrollHandDrag);
    }

    void ZoomableGraphicsView::wheelEvent(QWheelEvent *event) {
        if (event->angleDelta().y() > 0)
            scale(1.1, 1.1);
        else
            scale(1 / 1.1, 1 / 1.1);
        event->accept();
    }

    GraphicView2d::GraphicView2d() {
        m_graphic_widget = new ZoomableGraphicsView;
        m_scene = new QGraphicsScene;
        dynamic_cast<QGraphicsView*>(m_graphic_widget)->setScene(m_scene);
    }

    void GraphicView2d::saveFigure() {
        auto file_name = QFileDialog::getSaveFileName(this, "Save Image", "", "PDF files (*.pdf);; PNG files (*.png)");
        if (file_name.contains(".pdf")) {
            QPdfWriter pdfwriter(file_name);
            auto screen_size = QGuiApplication::screens().front()->size();
            auto screen_physical_size = QGuiApplication::screens().front()->physicalSize();
            auto resolution = screen_size.width() / (screen_physical_size.width() / 25.4);
            pdfwriter.setResolution(resolution);
            pdfwriter.setPageSize(QPageSize(rect().size()));
            QPainter painter(&pdfwriter);
            render(&painter);
        }
        else if (file_name.contains(".png")) {
            QImage image(rect().size(), QImage::Format_ARGB32);
            image.fill(Qt::transparent);
            QPainter painter(&image);
            render(&painter);
            image.save(file_name);
        }
    }

    void GraphicView2d::resetCamera() {
        auto rect = dynamic_cast<QGraphicsView *>(m_graphic_widget)->scene()->itemsBoundingRect();
        dynamic_cast<QGraphicsView *>(m_graphic_widget)->scene()->setSceneRect(rect);
        dynamic_cast<QGraphicsView *>(m_graphic_widget)->fitInView(dynamic_cast<QGraphicsView *>(m_graphic_widget)->sceneRect(), Qt::KeepAspectRatio);
    }

    void GraphicView2d::updateView() {}
}