#ifndef OFEC_DEMO_GRAPHIC_VIEW_H
#define OFEC_DEMO_GRAPHIC_VIEW_H

#include <QString>
#include <QWidget>

namespace ofec_demo {
	class graphicView : public QWidget {
	protected:
		QWidget *m_graphic_widget;
	public:
		virtual ~graphicView() {}
		virtual void saveFigure() = 0;
		virtual void resetCamera() = 0;
		virtual void updateView() = 0;
		QWidget* graphicWidget() { return m_graphic_widget; }
	};
}

#endif // !OFEC_DEMO_GRAPHIC_VIEW_H
