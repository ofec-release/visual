#ifndef OFEC_DEMO_BUFFER_ALG_H
#define OFEC_DEMO_BUFFER_ALG_H

#include <core/definition.h>
#include "../../core/window/graphics_area.h"
#include "buffer_pro.h"

namespace ofec_demo {
	/* Buffer of Algorithm */
	class BufferAlg {
	protected:	
		int m_id_alg;
		BufferPro *m_buffer_pro;
		GraphicsArea *m_graphics_area;

		std::vector<std::pair<ofec::Real, ofec::Real>> m_cur_obj_range;

	public:
		BufferAlg(GraphicsArea *graphic_dock_set, int id_alg, BufferPro *buffer_pro);
		virtual ~BufferAlg() {}
		int idAlg() const { return m_id_alg; }

		virtual void initializeGraphicViews() {}
		virtual bool graphicViewsInitialized();
		virtual void appendBuffer(int max_buffer_id) {}
		virtual void popupBuffer(int buffer_id) {}
		virtual void updateGraphicViews(int buffer_id) {}

		virtual void updateViews();
		virtual void setSolutionVisible(bool flag) {}
	};
}

#endif // !OFEC_DEMO_BUFFER_ALG_H
