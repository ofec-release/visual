#ifndef OFEC_DEMO_ITEMS_3D_EA_H
#define OFEC_DEMO_ITEMS_3D_EA_H

#include <vtkActor.h>
#include <vtkSmartPointer.h>
#include <vtkRenderer.h>
#include <core/definition.h>

namespace ofec_demo {
	class PopObjSpace3d {
	private:
		std::vector<vtkSmartPointer<vtkActor>> m_multi_pop;
	public:
		void updateMultiPops(
			const std::vector<std::vector<ofec::Real>> &mp,
			const std::vector<int> &pi, int np, 
			const std::vector<std::pair<ofec::Real, ofec::Real>> &range);
		void addMultiPops(vtkRenderer *renderer);
		void removeMultiPops(vtkRenderer *renderer);
		void setMultiPopsVisible(bool flag);
	};
}

#endif // !OFEC_DEMO_ITEMS_3D_EA_H
