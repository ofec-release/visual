#ifndef OFEC_DEMO_BUFFER_H
#define OFEC_DEMO_BUFFER_H

#include "buffer_alg.h"
#include "buffer_pro.h"
#include <QObject>
#include <map>
#include <memory>

namespace ofec_demo {
	class Buffer final : public QObject {
		Q_OBJECT
	private:
		int m_max_frame_id;
		int m_cur_frame_id;
		const int m_max_num_later_frames;
		int m_max_alg_buffer_id, m_max_pro_buffer_id;
		int m_cur_alg_buffer_id, m_cur_pro_buffer_id;
		std::map<int, int> m_frame_id_to_alg_buffer_id, m_frame_id_to_pro_buffer_id;
		std::map<int, int> m_buffer_alg2pro_id;
		std::map<int, bool> m_frame_id_alg_pro_change;
		std::unique_ptr<BufferAlg> m_buffer_alg;
		std::unique_ptr<BufferPro> m_buffer_pro;
		std::map<int, int> m_frame_id_to_num_evals;

	public:
		Buffer();
		void releaseBufferAlg();
		void releaseBufferPro();
		void setupBufferAlg(GraphicsArea *graphics_area, int id_alg);
		void setupBufferPro(GraphicsArea *graphics_area, int id_pro);
		int idPro() const { return m_buffer_pro->idPro(); }
		int idAlg() const { return m_buffer_alg->idAlg(); }
		int numEvals(int frame_id) const { return m_frame_id_to_num_evals.at(frame_id); }

		int idBufferAlg2Pro(int buffer_alg_id)const { return m_buffer_alg2pro_id.at(buffer_alg_id); }

		void appendAlgBuffer(int alg_id);
		void appendProBuffer(int pro_id);
		void updateGraphicViews(int frame_id);

		void updateViews();
		void setSolutionVisible(bool flag);
		void setOptimaVisible(bool flag);

	private:
		void increaseMaxFrameId();

	public slots:
		void onMinFrameIdIncreased(int frame_id);
		void onCurFrameIdModified(int frame_id);

	signals:
		void maxFrameIdIncreased(int max_frame_id);
	};
}

#endif //!OFEC_DEMO_BUFFER_H