#include "items_3d_ea.h"
#include "geometrics/geom_3d.h"
#include "../global_ui.h"

namespace ofec_demo {
	void PopObjSpace3d::updateMultiPops(
		const std::vector<std::vector<ofec::Real>> &mp,
		const std::vector<int> &pi, int np,
		const std::vector<std::pair<ofec::Real, ofec::Real>> &range)
	{
		std::vector<std::vector<std::array<ofec::Real, 3>>> pops_point(np);
		std::array<ofec::Real, 3> ind;
		for (size_t i = 0; i < mp.size(); ++i) {
			ind[0] = (mp[i][0] - range[0].first) / (range[0].second - range[0].first) * 10 - 5;
			ind[1] = (mp[i][1] - range[1].first) / (range[1].second - range[1].first) * 10 - 5;
			ind[2] = (mp[i][2] - range[2].first) / (range[2].second - range[2].first) * 10 - 5;
			pops_point[pi[i]].push_back(ind);
		}
		m_multi_pop.resize(np);
		for (size_t i = 0; i < np; ++i) {
			auto &c = g_rand_colors[i % g_num_rand_colors];
			m_multi_pop[i] = Geom3d::createPoints(pops_point[i], 4, c[0], c[1], c[2]);
		}
	}

	void PopObjSpace3d::addMultiPops(vtkRenderer *renderer) {
		for (auto &pop : m_multi_pop)
			renderer->AddActor(pop);
	}

	void PopObjSpace3d::removeMultiPops(vtkRenderer *renderer) {
		for (auto &pop : m_multi_pop)
			renderer->RemoveActor(pop);
	}

	void PopObjSpace3d::setMultiPopsVisible(bool flag) {
		for (auto &pop : m_multi_pop)
			pop->SetVisibility(flag);
	}
}