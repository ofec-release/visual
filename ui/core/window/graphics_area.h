#ifndef OFEC_DEMO_GRAPHICS_AREA_H
#define OFEC_DEMO_GRAPHICS_AREA_H

#include <QDockWidget>
#include <QComboBox>
#include <QToolButton>
#include <QAction>
#include <QSplitter>
#include <map>
#include <array>

#include <QMdiArea>
#include <QMdiSubWindow>
#include <QMap>
#include <QMenu>
#include <QAction>

#include "../buffer/graphic_view/graphic_view.h"

namespace ofec_demo {
    class GraphicsArea : public QMdiArea {
        Q_OBJECT
    private:
        QMenu *m_view_menu;
        QMap<QString, QAction*> m_actions;
        QMap<QString, QMdiSubWindow*> m_sub_windows;
        QWidget* addNewWidget(const QString &title, graphicView *view);

    public:
        GraphicsArea();
        void setViewMenu(QMenu *view_menu);
        void addGraphic(const QString &title, graphicView *view);
        void removeGraphic(const QString &title);
        void setVisible(const QString &title, bool visible);
    };
}

#endif //!OFEC_DEMO_GRAPHICS_AREA_H