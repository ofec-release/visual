#ifndef OFEC_DEMO_OUTPUT_DOCK_H
#define OFEC_DEMO_OUTPUT_DOCK_H

#include <QDockWidget>
#include <QTextEdit>
#include <QToolButton>
#include <QComboBox>

namespace ofec_demo {
    class OutputDock : public QDockWidget {
        Q_OBJECT
    public:
        OutputDock();

    public slots:
        void appendLog(const QString& text);

    private:
        void createTextEdit();
        void createTitleBox();
        void createTitleBar();
        void createTitleIcon();
        void createClearBtn();
        void createOutputBtn();

        QTextEdit *m_text_edit;
        QWidget *m_title_bar;
        QComboBox *m_title_box;
        QToolButton *m_title_icon, *m_clear_btn, *m_output_btn;

    private slots:
        void outputText();
    };
} 

#endif // !OFEC_DEMO_OUTPUT_DOCK_H