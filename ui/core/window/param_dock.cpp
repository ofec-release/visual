#include "param_dock.h"
#include <QHBoxLayout>
#include "../global_ui.h"
#include <QGuiApplication>
#include <QScreen>
#include <QLabel>

namespace ofec_demo {
    ParamDock::ParamDock(const QString& title) : QDockWidget(title) {
		createScrollAreas();
		setWidget(m_construction);
		createTitleBar();
		setTitleBarWidget(m_title_bar);
		QSize available_size = QGuiApplication::screens().front()->availableGeometry().size();
		setMinimumWidth(available_size.width() / 8);
    }

	void ParamDock::updateConstruction(QWidget *widget) {
		m_construction->setWidget(widget);
	}

	void ParamDock::updateVisualization(QWidget *widget) {
		m_visualization->setWidget(widget);
	}

	void ParamDock::switchConstruction() {
		m_title_box->setCurrentText("construction");
	}

	void ParamDock::switchVisualization() {
		m_title_box->setCurrentText("visualization");
	}

	void ParamDock::createScrollAreas() {
		m_construction = new QScrollArea;
		m_construction->setWidgetResizable(true);

		m_visualization = new QScrollArea;
		m_visualization->setWidgetResizable(true);
	}

	void ParamDock::createTitleBar() {
		createTitleIcon();
		createTitleBox();

		auto layout = new QHBoxLayout;
		layout->addWidget(m_title_icon);
		layout->addWidget(new QLabel("Parameters of: "));
		layout->addWidget(m_title_box);
		layout->addStretch(1);
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		m_title_bar = new QWidget;
		m_title_bar->setLayout(layout);
		QPalette pal;
		pal.setColor(QPalette::Window, QColor(100, 100, 100, 20));
		pal.setColor(QPalette::WindowText, Qt::black);
		m_title_bar->setPalette(pal);
		m_title_bar->setAutoFillBackground(true);
	}

	void ParamDock::createTitleIcon() {
		m_title_icon = new QToolButton;
		m_title_icon->setIcon(QIcon(FIG_DIR "/setting.png"));
		m_title_icon->setStyleSheet(g_icon_button_style);
		m_title_icon->setEnabled(false);
	}

	void ParamDock::createTitleBox() {
		m_title_box = new QComboBox;
		m_title_box->addItem("construction");
		m_title_box->addItem("visualization");
		connect(m_title_box, &QComboBox::currentTextChanged, this, &ParamDock::onTitleBoxTextChanged);
	}

	void ParamDock::onTitleBoxTextChanged(const QString &text) {
		if (text == "construction")
			setWidget(m_construction);
		else
			setWidget(m_visualization);
	}
}