#ifndef OFEC_DEMO_ABOUT_DIALOG_H
#define OFEC_DEMO_ABOUT_DIALOG_H

#include <QDialog>

namespace ofec_demo {
    class AboutDialog : public QDialog {
        Q_OBJECT
    public:
        AboutDialog(QWidget *parent);
    };
}

#endif // ! OFEC_DEMO_ABOUT_DIALOG_H