#ifndef OFEC_DEMO_PROGRESS_BAR_H
#define OFEC_DEMO_PROGRESS_BAR_H

#include <QToolBar>
#include <QSlider>
#include <QSpinBox>
#include <QToolButton>
#include <QLabel>
#include "../thread/play_animation.h"

namespace ofec_demo {
	class ProgressBar : public QToolBar {
		Q_OBJECT
	private:
		int m_play_speed;
		int m_min_frame_id, m_cur_frame_id, m_max_frame_id;
		int m_max_num_prior_frames;
		int m_buffer_size;
		QSlider *m_slider;
		//QSpinBox* m_spin_box;
		QToolButton *m_pause_btn, *m_speed_up_btn, *m_slow_down_btn;
		QLabel *m_min_frame_id_label, *m_max_frame_id_labl, 
			*m_cur_frame_id_label, *m_play_speed_label, *m_cur_fes_label;
		ThreadPlayAnimation *m_thrd_play;

	public:
		ProgressBar();
		~ProgressBar();
		void play();
		void stop();
		bool isPlayThrdFinished();

	public slots:
		void onMaxFrameIdIncreased(int max_frame_id);
		void increaseCurFrameId();
		void finishPlayThrd();

	private:
		void createLabels();
		void createSlider();
		void createSlowDownBtn();
		void createPauseBtn();
		void createSpeedUpBtn();

	private slots:
		void speedUp();
		void pause();
		void slowDown();
		void onProgressChanged(int val);
		void onPlayThrdExited();

	signals:
		void paused(bool flag = true);
		void curFrameIdModified(int cur_frame_id);
		void minFrameIdIncreased(int min_frame_id);
		void playThrdExited();
	};
}

#endif //!OFEC_DEMO_PROGRESS_BAR_H