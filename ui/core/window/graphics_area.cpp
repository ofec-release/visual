#include "graphics_area.h"
#include <QHBoxLayout>
#include <QPalette>
#include <QLabel>
#include <QEvent>
#include <QPixmap>
#include "../global_ui.h"

namespace ofec_demo {
    QWidget* GraphicsArea::addNewWidget(const QString &title, graphicView *view) {
        auto reset_cam_btn = new QToolButton;
        reset_cam_btn->setText("Expand View Full");
        reset_cam_btn->setIcon(QIcon(FIG_DIR "/reset_cam.png"));
#ifndef Q_OS_MACOS
        reset_cam_btn->setAutoRaise(true);
#else
        reset_cam_btn->setStyleSheet(g_tool_button_style);
#endif
        connect(reset_cam_btn, &QToolButton::clicked, view, &graphicView::resetCamera);

        auto save_fig_btn = new QToolButton;
        save_fig_btn->setText("Save Figure");
        save_fig_btn->setIcon(QIcon(FIG_DIR "/save_fig.png"));
#ifndef Q_OS_MACOS
        save_fig_btn->setAutoRaise(true);
#else
        save_fig_btn->setStyleSheet(g_tool_button_style);
#endif
        connect(save_fig_btn, &QToolButton::clicked, view, &graphicView::saveFigure);
     
        auto tools_layout = new QHBoxLayout;
        tools_layout->addStretch(1);
        tools_layout->addWidget(new QLabel(" " + title));
        tools_layout->addStretch(1);
        tools_layout->addWidget(reset_cam_btn);
        tools_layout->addWidget(save_fig_btn);
        tools_layout->setContentsMargins(0, 0, 0, 0);
        tools_layout->setSpacing(0);

        auto tools = new QWidget;
        tools->setLayout(tools_layout);
        QPalette pal;
        pal.setColor(QPalette::Window, QColor(100, 100, 100, 20));
        pal.setColor(QPalette::WindowText, Qt::black);
        tools->setPalette(pal);
        tools->setAutoFillBackground(true);
        tools->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

        auto vlayout = new QVBoxLayout;
        vlayout->addWidget(tools);
        vlayout->addWidget(view->graphicWidget());
        vlayout->setSpacing(0);
        vlayout->setContentsMargins(2, 2, 2, 2);
        auto widget = new QWidget;
        widget->setLayout(vlayout);

        return widget;
    }

    GraphicsArea::GraphicsArea() : m_view_menu(nullptr) {}
    
    void GraphicsArea::setViewMenu(QMenu *view_menu) {
        m_view_menu = view_menu;
    }

    void GraphicsArea::addGraphic(const QString &title, graphicView *view) {
        m_sub_windows.insert(title, addSubWindow(addNewWidget(title, view)));
        m_sub_windows.value(title)->setWindowFlags(Qt::FramelessWindowHint);
        m_sub_windows.value(title)->show();
        tileSubWindows();

        if (m_view_menu) {
            auto action = new QAction(title);
            action->setCheckable(true);
            action->setChecked(m_sub_windows.value(title)->isVisible());
            connect(action, &QAction::triggered, m_sub_windows.value(title), &QMdiSubWindow::setVisible);
            connect(action, &QAction::triggered, this, &QMdiArea::tileSubWindows);
            m_actions.insert(title, action);
            m_view_menu->addAction(action);
        }
    }

    void GraphicsArea::removeGraphic(const QString &title) {
        if (m_sub_windows.contains(title)) {
            if (m_sub_windows.value(title) == activeSubWindow()) {
                for (auto sub_window : subWindowList()) {
                    if (sub_window != activeSubWindow()) {
                        setActiveSubWindow(sub_window);
                        break;
                    }
                }
            }
            removeSubWindow(m_sub_windows.value(title));
            m_sub_windows.remove(title);
            tileSubWindows();
        }
        if (m_view_menu && m_actions.contains(title)) {
            m_view_menu->removeAction(m_actions.value(title));
            delete m_actions.value(title);
            m_actions.remove(title);
        }
    }
   
    void GraphicsArea::setVisible(const QString &title, bool visible) {
        if (m_actions.contains(title)) {
            m_sub_windows[title]->setVisible(visible);
            m_actions[title]->setChecked(visible);
            tileSubWindows();
        }
    }
}