#ifndef OFEC_DEMO_PARAM_DOCK_H
#define OFEC_DEMO_PARAM_DOCK_H

#include <QDockWidget>
#include <QToolButton>
#include <QComboBox>
#include <QScrollArea>

namespace ofec_demo {
    class ParamDock : public QDockWidget {
        Q_OBJECT
    public:
        ParamDock(const QString &title);
        void updateConstruction(QWidget *widget);
        void updateVisualization(QWidget *widget);
        void switchConstruction();
        void switchVisualization();

    private:
        void createScrollAreas();
        void createTitleBar();
        void createTitleIcon();
        void createTitleBox();

        QWidget *m_title_bar;
        QComboBox *m_title_box;
        QToolButton *m_title_icon;
        QScrollArea *m_construction, *m_visualization;

    private slots:
        void onTitleBoxTextChanged(const QString &text);
    };
}

#endif //!OFEC_DEMO_PARAM_DOCK_H