#include "progress_bar.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include "../global_ui.h"

namespace ofec_demo {
    ProgressBar::ProgressBar() :
    	m_min_frame_id(0),
		m_cur_frame_id(0),
		m_max_frame_id(0),
		m_max_num_prior_frames(90),
        m_play_speed(1),
        m_thrd_play(nullptr),
        m_buffer_size(5)
    {
        createLabels();
        createSlider();
        createSlowDownBtn();
        createSpeedUpBtn();
        createPauseBtn();

        auto top_layout = new QHBoxLayout;
        top_layout->addWidget(m_min_frame_id_label);
        top_layout->addStretch(1);
        top_layout->addWidget(m_cur_frame_id_label);
        top_layout->addStretch(1);
        top_layout->addWidget(m_max_frame_id_labl);

        auto bot_layout = new QHBoxLayout;
        bot_layout->addWidget(new QLabel("Play Speed:"));
        bot_layout->addWidget(m_play_speed_label);
        bot_layout->addStretch(1);
        bot_layout->addWidget(m_slow_down_btn);
        bot_layout->addWidget(m_pause_btn);
        bot_layout->addWidget(m_speed_up_btn);
        bot_layout->addStretch(1);
        bot_layout->addWidget(new QLabel("Current FEs:"));
        bot_layout->addWidget(m_cur_fes_label);

        auto layout = new QVBoxLayout;
        layout->addLayout(top_layout);
        layout->addWidget(m_slider);
        layout->addLayout(bot_layout);

		layout->setSpacing(0);

		auto widget = new QWidget;
		widget->setLayout(layout);

        addWidget(widget);
		setMovable(false);

        m_thrd_play = new ThreadPlayAnimation(m_play_speed);
        connect(m_thrd_play, &ThreadPlayAnimation::oneLapsePassed, this, &ProgressBar::increaseCurFrameId);
        connect(m_thrd_play, &ThreadPlayAnimation::finished, this, &ProgressBar::onPlayThrdExited);
        m_thrd_play->start();
    }

    ProgressBar::~ProgressBar() {
        delete m_thrd_play;
    }

   void ProgressBar::finishPlayThrd() {
       m_thrd_play->terminate();
   }

   void ProgressBar::onPlayThrdExited() {
       emit playThrdExited();
   }

   void ProgressBar::createLabels() {
        m_min_frame_id_label = new QLabel(QString::number(m_min_frame_id));
        m_max_frame_id_labl = new QLabel(QString::number(m_max_frame_id));
        m_cur_frame_id_label = new QLabel(QString::number(m_cur_frame_id));
        m_play_speed_label = new QLabel("×" + QString::number(m_play_speed));
        m_cur_fes_label = new QLabel(QString::number(0));
   }

   void ProgressBar::createSlider() {
        m_slider = new QSlider(Qt::Horizontal);
        m_slider->setSingleStep(1);
        m_slider->setRange(0, 0);
        m_slider->setValue(0);
        connect(m_slider, &QSlider::valueChanged, this, &ProgressBar::onProgressChanged);
        connect(m_slider, &QSlider::sliderPressed, this, &ProgressBar::pause);
    }

    void ProgressBar::createSlowDownBtn() {
        m_slow_down_btn = new QToolButton;
		m_slow_down_btn->setIcon(QIcon(FIG_DIR "/slow.png"));
		m_slow_down_btn->setText("Slow");
		m_slow_down_btn->setEnabled(false);
		m_slow_down_btn->setCheckable(false);
		m_slow_down_btn->setShortcut(QKeySequence("Left"));
#ifndef Q_OS_MACOS
		m_slow_down_btn->setAutoRaise(true);
#else
		m_slow_down_btn->setStyleSheet(g_tool_button_style);
#endif
		connect(m_slow_down_btn, &QToolButton::clicked, this, &ProgressBar::slowDown);
    }

    void ProgressBar::createPauseBtn() {
		m_pause_btn = new QToolButton;
		m_pause_btn->setIcon(QIcon(FIG_DIR "/pause.png"));
		m_pause_btn->setText("Pause");
		m_pause_btn->setEnabled(false);
		m_pause_btn->setCheckable(false);
		m_pause_btn->setShortcut(QKeySequence("Space"));
#ifndef Q_OS_MACOS
		m_pause_btn->setAutoRaise(true);
#else
		m_pause_btn->setStyleSheet(g_tool_button_style);
#endif
		connect(m_pause_btn, &QToolButton::clicked, this, &ProgressBar::pause);
    }

    void ProgressBar::createSpeedUpBtn() {
		m_speed_up_btn = new QToolButton;
		m_speed_up_btn->setIcon(QIcon(FIG_DIR "/speed.png"));
		m_speed_up_btn->setText("Speed");
		m_speed_up_btn->setEnabled(true);
		m_speed_up_btn->setCheckable(false);
		m_speed_up_btn->setShortcut(QKeySequence("Right"));
#ifndef Q_OS_MACOS
		m_speed_up_btn->setAutoRaise(true);
#else
		m_speed_up_btn->setStyleSheet(g_tool_button_style);
#endif
		connect(m_speed_up_btn, &QToolButton::clicked, this, &ProgressBar::speedUp);
    }

    void ProgressBar::play() {
        if (g_pause)
            g_pause = false;
        m_pause_btn->setEnabled(true);
        if (m_cur_frame_id + m_buffer_size < m_max_frame_id)
            m_thrd_play->resume();
    }

    void ProgressBar::stop() {
        m_min_frame_id = 0;
        m_max_frame_id = 0;
        m_min_frame_id_label->setText(QString::number(0));
        m_max_frame_id_labl->setText(QString::number(0));
        m_slider->setMinimum(0);
        m_slider->setMaximum(0);
        m_pause_btn->setEnabled(false);
        g_pause = false;
    }

    bool ProgressBar::isPlayThrdFinished() {
        return m_thrd_play->isFinished();
    }

    void ProgressBar::pause() {
        g_pause = true;
        m_pause_btn->setEnabled(false);
        emit paused();
    }

    void ProgressBar::increaseCurFrameId() {
        if (!g_term_alg && !g_pause && m_cur_frame_id + m_buffer_size < m_max_frame_id) {
            m_slider->setValue(m_cur_frame_id + 1);
        }
    }

    void ProgressBar::onProgressChanged(int val) {
        if (m_cur_frame_id == val)
            return;
        m_cur_frame_id = val;
        m_cur_frame_id_label->setText(QString::number(m_cur_frame_id));
        m_cur_fes_label->setText(QString::number(g_buffer->numEvals(m_cur_frame_id)));
        emit curFrameIdModified(m_cur_frame_id);
        while (m_min_frame_id + m_max_num_prior_frames < m_cur_frame_id) {
            m_min_frame_id_label->setText(QString::number(m_min_frame_id+1));
            m_slider->setMinimum(m_min_frame_id + 1);
            emit minFrameIdIncreased(m_min_frame_id);
            ++m_min_frame_id;
        }
        if (!g_pause && m_cur_frame_id + m_buffer_size < m_max_frame_id)
            m_thrd_play->resume(); 
    }

    void ProgressBar::onMaxFrameIdIncreased(int max_frame_id) {
        if (g_term_alg) 
            return;
        m_max_frame_id = max_frame_id;
        m_slider->setMaximum(m_max_frame_id);
        m_max_frame_id_labl->setText(QString::number(m_max_frame_id));
        if (!g_pause && m_cur_frame_id + m_buffer_size < m_max_frame_id)
            m_thrd_play->resume();
    }

    void ProgressBar::speedUp() {
        if (m_play_speed < 64) {
            m_play_speed *= 2;
            m_play_speed_label->setText("×" + QString::number(m_play_speed));
        }
        if (m_play_speed == 64)
            m_speed_up_btn->setEnabled(false);
        if (!m_slow_down_btn->isEnabled() && m_play_speed > 1)
            m_slow_down_btn->setEnabled(true);
        if (m_thrd_play)
            m_thrd_play->speedChanged(m_play_speed);
    }

    void ProgressBar::slowDown() {
        if (m_play_speed > 1) {
            m_play_speed /= 2;
            m_play_speed_label->setText("×" + QString::number(m_play_speed));
        }
        if (m_play_speed == 1)
            m_slow_down_btn->setEnabled(false);
        if (!m_speed_up_btn->isEnabled() && m_play_speed < 64)
            m_speed_up_btn->setEnabled(true);
        if (m_thrd_play)
            m_thrd_play->speedChanged(m_play_speed);
    }
}