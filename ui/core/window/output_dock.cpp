#include "output_dock.h"
#include <QHBoxLayout>
#include <QFile>
#include <QTextStream>
#include "../global_ui.h"
#include <QFileDialog>
#include <QLabel>
#include <QGuiApplication>
#include <QScreen>
#include <QDateTime>

namespace ofec_demo {
    OutputDock::OutputDock() : QDockWidget("Output") {
        createTextEdit();
        createTitleBar();
        setWidget(m_text_edit);
		setTitleBarWidget(m_title_bar);
        QSize available_size = QGuiApplication::screens().front()->availableGeometry().size();
        setMinimumWidth(available_size.width() / 8);
    }

    void OutputDock::appendLog(const QString& text) {
        m_text_edit->append(text);
    }

    void OutputDock::createTextEdit() {
        m_text_edit = new QTextEdit;
		m_text_edit->setReadOnly(true);
		m_text_edit->setContentsMargins(20, 5, 20, 5);
        m_text_edit->viewport()->setAutoFillBackground(false);
    }

    void OutputDock::createTitleBar() {
        createTitleIcon();
        createTitleBox();
        createClearBtn();
        createOutputBtn();

        auto layout = new QHBoxLayout;
        layout->addWidget(m_title_icon);
        layout->addWidget(new QLabel("Output from: "));
        layout->addWidget(m_title_box);
        layout->addStretch(1);
		layout->addWidget(m_clear_btn);
		layout->addWidget(m_output_btn);
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		m_title_bar = new QWidget;
		m_title_bar->setLayout(layout);
        QPalette pal;
        pal.setColor(QPalette::Window, QColor(100, 100, 100, 20));
        pal.setColor(QPalette::WindowText, Qt::black);
        m_title_bar->setPalette(pal);
        m_title_bar->setAutoFillBackground(true);
    }

    void OutputDock::createTitleIcon() {
        m_title_icon = new QToolButton;
		m_title_icon->setIcon(QIcon(FIG_DIR "/text.png"));
		m_title_icon->setStyleSheet(g_icon_button_style);
		m_title_icon->setEnabled(false);
    }

    void OutputDock::createTitleBox() {
        m_title_box = new QComboBox;
        m_title_box->addItem("log");
    }

    void OutputDock::createClearBtn() {
        m_clear_btn = new QToolButton;
		m_clear_btn->setIcon(QIcon(FIG_DIR "/clear_text.png"));
        connect(m_clear_btn, &QToolButton::clicked, m_text_edit, &QTextEdit::clear);
#ifndef Q_OS_MACOS
			m_clear_btn->setAutoRaise(true);
#else
			m_clear_btn->setStyleSheet(g_tool_button_style);
#endif
    }

    void OutputDock::createOutputBtn() {
		m_output_btn = new QToolButton;
		m_output_btn->setIcon(QIcon(FIG_DIR "/save_text.png"));
        connect(m_output_btn, &QToolButton::clicked, this, &OutputDock::outputText);
#ifndef Q_OS_MACOS
			m_output_btn->setAutoRaise(true);
#else
			m_output_btn->setStyleSheet(g_tool_button_style);
#endif
    }

    void OutputDock::outputText() {
		QString file_name = QFileDialog::getSaveFileName(nullptr, "", 
            m_title_box->currentText() + "_" + QDateTime::currentDateTime().toString("yyMMddhhmm"), "TXT files (*.txt)");
        QFile file(file_name);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;
        QTextStream out(&file);
        out << m_text_edit->toPlainText();
    }
}