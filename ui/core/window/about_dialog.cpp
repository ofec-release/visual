#include "about_dialog.h"
#include <QGuiApplication>
#include <QScreen>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QDate>

namespace ofec_demo {
    AboutDialog::AboutDialog(QWidget* parent) : QDialog(parent) {
        setWindowTitle("About OFEC");
		auto screen_size = QGuiApplication::screens().front()->size();
        int minSize = screen_size.width() > screen_size.height() ? screen_size.height() : screen_size.width();

		auto logo_label = new QLabel;
#ifndef Q_OS_MACOS
        auto pix_map = QPixmap(FIG_DIR "/ofec.png").scaledToWidth(minSize / 8, Qt::SmoothTransformation);
#else
        auto pix_map = QPixmap(FIG_DIR "/ofec.png").scaledToWidth(minSize / 4, Qt::SmoothTransformation);
        pix_map.setDevicePixelRatio(2);
#endif
        logo_label->setPixmap(pix_map);

        QString text_content;
        text_content += "Visual OFEC 1.0.0\n\n";
        text_content += "Based on Qt " QT_VERSION_STR " and VTK " VTK_VERSION_STR "\n\n";
		text_content += "Built on " + QDate::currentDate().toString("MMM dd yyyy") + "\n\n";
		text_content += "The program is provided AS IS with NO WARRANTY OF ANY KIND,"
			"INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS"
			"FOR A PARTICULAR PURPOSE.";
		auto text_label = new QLabel(text_content);
		text_label->setFixedWidth(screen_size.width() / 4);
		text_label->setWordWrap(true);
        
        auto first_row_layout = new QHBoxLayout;
		first_row_layout->addWidget(logo_label);
		first_row_layout->addSpacing(minSize / 50);
		first_row_layout->addWidget(text_label);
        		
        auto closeDialogButton = new QPushButton("Close");
		connect(closeDialogButton, &QPushButton::clicked, this, &AboutDialog::close);

        auto layout = new QVBoxLayout;
		layout->addLayout(first_row_layout);
		layout->addWidget(closeDialogButton, 1, Qt::AlignRight);

        setLayout(layout);
        setFixedSize(sizeHint());
        setVisible(false);
    }
}