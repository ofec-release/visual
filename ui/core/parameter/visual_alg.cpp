#include "visual_alg.h"
#include <QLabel>
#include <QCheckBox>
#include "../global_ui.h"

namespace ofec_demo {
	std::atomic<bool> VisualAlg::ms_show_solution(true);

	VisualAlg::VisualAlg(const QString &name, const std::set<ofec::ProTag> &pro_tags) :
		m_name(name),
		m_pro_tags(pro_tags),
		m_layout(new QVBoxLayout) {}

	void VisualAlg::updateLayout() {
		m_layout->addWidget(new QLabel("[" + m_name + "]"), 0, Qt::AlignHCenter);
		updateLayout_();
		m_layout->addStretch(1);
		setLayout(m_layout);
	}

	void VisualAlg::updateLayout_() {
		auto check_box = new QCheckBox("show solution");
		check_box->setChecked(ms_show_solution);
		connect(check_box, &QCheckBox::stateChanged, this, &VisualAlg::onCheckBoxToggled);

		auto VBox = new QVBoxLayout;
		VBox->addWidget(check_box);
#ifdef Q_OS_MACOS
		VBox->setSpacing(0);
		VBox->setContentsMargins(0, 0, 0, 0);
#else
		VBox->setSpacing(2);
		VBox->setContentsMargins(2, 2, 2, 2);
#endif

		m_layout->addLayout(VBox);
	}

	void VisualAlg::onCheckBoxToggled(bool checked) {
		if (ms_show_solution != checked) {
			ms_show_solution = checked;
			g_buffer->setSolutionVisible(checked);
			//g_buffer->updateViews();
		}
	}
}