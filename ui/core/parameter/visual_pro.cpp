#include "visual_pro.h"
#include <QLabel>
#include <QCheckBox>
#include "../global_ui.h"

namespace ofec_demo {
	std::atomic<bool> VisualPro::ms_show_optima(false);

	VisualPro::VisualPro(const QString &name, const std::set<ofec::ProTag> &pro_tags) :
		m_name(name),
		m_pro_tags(pro_tags),
		m_layout(new QVBoxLayout) {}

	void VisualPro::updateLayout() {
		m_layout->addWidget(new QLabel("[" + m_name + "]"), 0, Qt::AlignHCenter);
		updateLayout_();
		m_layout->addStretch(1);
		setLayout(m_layout);
	}

	void VisualPro::updateLayout_() {
		auto check_box = new QCheckBox("Show optima");
		check_box->setChecked(ms_show_optima);
		connect(check_box, &QCheckBox::stateChanged, this, &VisualPro::onCheckBoxToggled);

		auto VBox = new QVBoxLayout;
		VBox->addWidget(check_box);
#ifdef Q_OS_MACOS
		VBox->setSpacing(0);
		VBox->setContentsMargins(0, 0, 0, 0);
#else
		VBox->setSpacing(2);
		VBox->setContentsMargins(2, 2, 2, 2);
#endif

		m_layout->addLayout(VBox);
	}

	void VisualPro::onCheckBoxToggled(bool checked) {
		if (ms_show_optima != checked) {
			ms_show_optima = checked;
			g_buffer->setOptimaVisible(checked);
			//g_buffer->updateViews();
		}
	}
}