#include "cnstrct.h"

namespace ofec_demo {
	Cnstrct::Cnstrct(const QString& name, const std::set<ofec::ProTag> &pro_tags) :
		m_layout(new QVBoxLayout),
		m_name(name),
		m_pro_tags(pro_tags) {}

	void Cnstrct::updateLayout() {
		updateLayout_();
		m_layout->addStretch(1);
		setLayout(m_layout);
	}
}