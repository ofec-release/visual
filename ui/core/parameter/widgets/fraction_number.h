#ifndef OFEC_DEMO_FRACTION_NUMBER_H
#define OFEC_DEMO_FRACTION_NUMBER_H

#include "param_widget.h"
#include <QSpinBox>

namespace ofec_demo {
	class FractionNumber : public ParamWidget {
		Q_OBJECT
	private:
		QSpinBox *m_num, *m_denom;
	public:
		FractionNumber(ParamType type, const std::string &name, int num, int denom, int denom_max);
	private slots:
		void updateNum(int value);
		void updateDenom(int value);
	};
}

#endif // !OFEC_DEMO_FRACTION_NUMBER_H
