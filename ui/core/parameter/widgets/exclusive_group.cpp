#include "exclusive_group.h"
#include "../../../core/global_ui.h"
#include <QRadioButton>
#include <QVBoxLayout>
#include <QButtonGroup>
#include <QLabel>

namespace ofec_demo {
	ExclusiveGroup::ExclusiveGroup(ParamType type, const std::string& name, const std::list<std::string>& group_content, int id) :
		ParamWidget(type, name)
	{
		if (group_content.empty())
			return;
		else {
			std::vector<QRadioButton*> radios;
			for (const auto& item : group_content)
				radios.push_back(new QRadioButton(QString::fromStdString(item)));

			if (g_params[m_type].count(m_name) > 0)
				id = std::get<int>(g_params[m_type][m_name]);
			else
				g_params[m_type][m_name] = id;

			if (id >= 0 && id < radios.size())
				radios[id]->setChecked(true);
			else {
				radios[0]->setChecked(true);
				g_params[m_type][m_name] = 0;
			}

			auto button_group = new QButtonGroup;
			for (size_t i = 0; i < radios.size(); i++)
				button_group->addButton(radios[i], i);
			button_group->setExclusive(true);
			connect(button_group, &QButtonGroup::idToggled, this, &ExclusiveGroup::onToggled);

			auto vbox = new QVBoxLayout;
			for (auto radio : radios)
				vbox->addWidget(radio);

			auto VBox = new QVBoxLayout;
			VBox->addWidget(new QLabel(QString::fromStdString(m_name) + ":"));
			VBox->addLayout(vbox);
#ifdef Q_OS_MACOS
			VBox->setSpacing(0);
			VBox->setContentsMargins(0, 0, 0, 0);
#else
			VBox->setSpacing(2);
			VBox->setContentsMargins(2, 2, 2, 2);
#endif

			this->setLayout(VBox);
		}
	}

	void ExclusiveGroup::onToggled(int id, bool checked) {
		if (checked) {
			g_params[m_type][m_name] = id;
		}
	}
}