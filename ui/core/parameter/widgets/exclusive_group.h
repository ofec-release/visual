#ifndef OFEC_DEMO_EXCLUSIVE_GROUP_H
#define OFEC_DEMO_EXCLUSIVE_GROUP_H

#include "param_widget.h"
#include <list>

namespace ofec_demo {
	class ExclusiveGroup : public ParamWidget {
		Q_OBJECT
	public:		
		ExclusiveGroup(ParamType type, const std::string& name, const std::list<std::string>& group_content, int id);
	private slots:
		void onToggled(int id, bool checked);
	};
}

#endif // !OFEC_DEMO_EXCLUSIVE_GROUP_H
