#ifndef OFEC_DEMO_INT_SLIDER_H
#define OFEC_DEMO_INT_SLIDER_H

#include "param_widget.h"

namespace ofec_demo {
	class IntSlider : public ParamWidget {
		Q_OBJECT
	public:
		IntSlider(ParamType type, const std::string& name, int min, int max, int val, int step);
	private slots:
		void updateParam(int value);
	};
}

#endif // !OFEC_DEMO_INT_SLIDER_H
