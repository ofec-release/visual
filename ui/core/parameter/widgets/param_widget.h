#ifndef OFEC_DEMO_PARAM_WIDGET_H
#define OFEC_DEMO_PARAM_WIDGET_H

#include <QWidget>
#include <string>
#include "../../global_ui.h"

namespace ofec_demo {
	class ParamWidget : public QWidget {
	protected:
		ParamType m_type;
		std::string m_name;
		ParamWidget(ParamType type, const std::string &name);
	};
}

#endif // !OFEC_DEMO_PARAM_WIDGET_H