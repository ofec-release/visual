#include "fraction_number.h"
#include "../../../core/global_ui.h"
#include <QHBoxLayout>
#include <QLabel>

namespace ofec_demo {
	FractionNumber::FractionNumber(ParamType type, const std::string &name, int num, int denom, int denom_max)
		: ParamWidget(type, name) 
	{
		m_denom = new QSpinBox;
		m_denom->setRange(1, denom_max);
		m_denom->setValue(denom);
		m_num = new QSpinBox;
		m_num->setRange(1, denom);
		m_num->setValue(num);
		connect(m_denom, QOverload<int>::of(&QSpinBox::valueChanged), m_num, &QSpinBox::setMaximum);
		connect(m_num, QOverload<int>::of(&QSpinBox::valueChanged), this, &FractionNumber::updateNum);
		connect(m_denom, QOverload<int>::of(&QSpinBox::valueChanged), this, &FractionNumber::updateDenom);

		auto HBox = new QHBoxLayout;
		HBox->addStretch(1);
		HBox->addWidget(m_num);
		HBox->addWidget(new QLabel("/"));
		HBox->addWidget(m_denom);
		HBox->addStretch(1);
#ifdef Q_OS_MACOS
		HBox->setSpacing(0);
#else
		HBox->setSpacing(5);
#endif
		HBox->setContentsMargins(0, 0, 0, 0);

		auto VBox = new QVBoxLayout;
		VBox->addWidget(new QLabel(QString::fromStdString(m_name) + ":"));
		VBox->addLayout(HBox);
#ifdef Q_OS_MACOS
		VBox->setSpacing(0);
		VBox->setContentsMargins(0, 0, 0, 0);
#else
		VBox->setSpacing(2);
		VBox->setContentsMargins(2, 2, 2, 2);
#endif

		this->setLayout(VBox);
	}

	void FractionNumber::updateNum(int value) {
		g_params[m_type][m_name] = (ofec::Real)value / m_denom->value();
	}

	void FractionNumber::updateDenom(int value) {
		g_params[m_type][m_name] = (ofec::Real)m_num->value() / value;
	}
}