#ifndef OFEC_DEMO_DOUBLE_SLIDER_H
#define OFEC_DEMO_DOUBLE_SLIDER_H

#include "param_widget.h"

namespace ofec_demo {
	class DoubleSlider : public ParamWidget {
		Q_OBJECT
	public:
		DoubleSlider(ParamType type, const std::string& name, double min, double max, double val, double step);
	private slots:
		void doubleToInt(double val);
		void intToDouble(int val);
		void updateParam(double value);
	signals:
		void intTranlsated(int val);
		void doubleTranslated(double val);
	private:
		double m_min_d, m_step_d;
	};
}

#endif // !OFEC_DEMO_DOUBLE_SLIDER_H
