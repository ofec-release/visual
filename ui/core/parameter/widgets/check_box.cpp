#include "check_box.h"
#include <QCheckBox>
#include <QVBoxLayout>
#include "../../../core/global_ui.h"

namespace ofec_demo {
	CheckBox::CheckBox(ParamType type, const std::string &name, bool checked) : ParamWidget(type, name) {
		if (g_params[m_type].count(m_name) > 0)
			checked = std::get<bool>(g_params[m_type].at(m_name));
		else
			g_params[m_type][m_name] = checked;

		auto check_box = new QCheckBox(QString::fromStdString(m_name));
		check_box->setChecked(checked);
		connect(check_box, &QCheckBox::toggled, this, &CheckBox::onCheckBoxToggled);

		auto VBox = new QVBoxLayout;
		VBox->addWidget(check_box);
#ifdef Q_OS_MACOS
		VBox->setSpacing(0);
		VBox->setContentsMargins(0, 0, 0, 0);
#else
		VBox->setSpacing(2);
		VBox->setContentsMargins(2, 2, 2, 2);
#endif

		this->setLayout(VBox);
	}

	void CheckBox::onCheckBoxToggled(bool checked) {
		g_params[m_type][m_name] = checked;
	}
}