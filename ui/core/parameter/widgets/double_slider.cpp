#include "double_slider.h"
#include <QSlider>
#include <QDoubleSpinBox>
#include "../../../core/global_ui.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>

namespace ofec_demo {
	DoubleSlider::DoubleSlider(ParamType type, const std::string& name, double min, double max, double val, double step) :
		ParamWidget(type, name),
		m_min_d(min),
		m_step_d(step)
	{
		ofec::Real temp_real;
		if (g_params[m_type].count(m_name) > 0) {
			temp_real = std::get<ofec::Real>(g_params[m_type].at(m_name));
			val = static_cast<double>(temp_real);
		}

		auto spin_box = new QDoubleSpinBox;
		spin_box->setRange(min, max);
		spin_box->setSingleStep(step);
		spin_box->setValue(val);
		spin_box->setFocusPolicy(Qt::ClickFocus);
		if (step <= 0.001)
			spin_box->setDecimals(3);

		auto slider = new QSlider(Qt::Horizontal);
		slider->setRange(0, (max - min) / step);
		slider->setSingleStep(1);
		slider->setValue((val - min) / step);

		connect(slider, &QSlider::valueChanged, this, &DoubleSlider::intToDouble);
		connect(this, &DoubleSlider::doubleTranslated, spin_box, &QDoubleSpinBox::setValue);
		connect(spin_box, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &DoubleSlider::doubleToInt);
		connect(spin_box, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &DoubleSlider::updateParam);
		connect(this, &DoubleSlider::intTranlsated, slider, &QSlider::setValue);
		updateParam(spin_box->value());

		auto HBox = new QHBoxLayout;
		HBox->addWidget(slider);
		HBox->addWidget(spin_box);
#ifdef Q_OS_MACOS
		HBox->setSpacing(0);
#else
		HBox->setSpacing(5);
#endif
		HBox->setContentsMargins(0, 0, 0, 0);


		auto VBox = new QVBoxLayout;
		VBox->addWidget(new QLabel(QString::fromStdString(m_name) + ":"));
		VBox->addLayout(HBox);
#ifdef Q_OS_MACOS
		VBox->setSpacing(0);
		VBox->setContentsMargins(0, 0, 0, 0);
#else
		VBox->setSpacing(2);
		VBox->setContentsMargins(2, 2, 2, 2);
#endif

		this->setLayout(VBox);
	}

	void DoubleSlider::doubleToInt(double val) {
		emit intTranlsated((val - m_min_d) / m_step_d);
	}

	void DoubleSlider::intToDouble(int val) {
		emit doubleTranslated(m_min_d + m_step_d * val);
	}
	
	void DoubleSlider::updateParam(double value) {
		g_params[m_type][m_name] = static_cast<ofec::Real>(value);
	}
}