#ifndef OFEC_DEMO_DROP_DOWN_H
#define OFEC_DEMO_DROP_DOWN_H

#include "param_widget.h"
#include <list>

namespace ofec_demo {
	class DropDown : public ParamWidget {
		Q_OBJECT
	public:
		DropDown(ParamType type, const std::string& param, const std::list<std::string>& contents, int id);
		DropDown(ParamType type, const std::string& param, const std::list<std::string>& contents, const std::string& txt);
	private slots:
		void onTextChanged(const QString& text);
		void onIndexChanged(int index);
	};
}

#endif // !OFEC_DEMO_DROP_DOWN_H
