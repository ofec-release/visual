#include "drop_down.h"
#include "../../../core/global_ui.h"
#include <QComboBox>
#include <QVBoxLayout>
#include <QLabel>

namespace ofec_demo {
	DropDown::DropDown(ParamType type, const std::string& name, const std::list<std::string>& contents, int id) :
		ParamWidget(type, name)
	{
		if (contents.empty())
			return;
		else {
			int id_select;
			std::string str_select;
			if (g_params[m_type].count(m_name) > 0) {
				int id_glb = std::get<int>(g_params[m_type][m_name]);
				if (id_glb > -1 && id_glb < contents.size())
					id_select = id_glb;
				else {
					if (id > -1 && id < contents.size())
						id_select = id;
					else
						id_select = 0;
					g_params[m_type][m_name] = id_select;
				}
			}
			else {
				if (id > -1 && id < contents.size())
					id_select = id;
				else
					id_select = 0;
				g_params[m_type][m_name] = id_select;
			}
			auto box = new QComboBox;
			box->setMaxVisibleItems(50);
			for (auto entry : contents) {
				box->addItem(QString::fromStdString(entry));
			}
			box->setCurrentIndex(id_select);
			connect(box, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &DropDown::onIndexChanged);

			auto VBox = new QVBoxLayout;
			VBox->addWidget(new QLabel(QString::fromStdString(m_name) + ":"));
			VBox->addWidget(box);
#ifdef Q_OS_MACOS
			VBox->setSpacing(0);
			VBox->setContentsMargins(0, 0, 0, 0);
#else
			VBox->setSpacing(2);
			VBox->setContentsMargins(2, 2, 2, 2);
#endif

			this->setLayout(VBox);
		}
	}

	DropDown::DropDown(ParamType type, const std::string& name, const std::list<std::string>& contents, const std::string& txt) :
		ParamWidget(type, name) 
	{
		if (contents.empty())
			return;
		else {
			std::string str_select;
			if (g_params[m_type].count(m_name) > 0) {
				//std::string str_glb = static_cast<std::string>(g_params[m_type][m_name]);
				std::string str_glb = std::get<std::string>(g_params[m_type][m_name]);
				if (std::find(contents.begin(), contents.end(), str_glb) != contents.end())
					str_select = str_glb;
				else {
					if (std::find(contents.begin(), contents.end(), txt) != contents.end())
						str_select = txt;
					else
						str_select = contents.front();
					g_params[m_type][m_name] = str_select;
				}
			}
			else {
				if (std::find(contents.begin(), contents.end(), txt) != contents.end())
					str_select = txt;
				else
					str_select = contents.front();
				g_params[m_type][m_name] = str_select;
			}
			auto box = new QComboBox;
			box->setMaxVisibleItems(50);
			for (auto entry : contents) {
				box->addItem(QString::fromStdString(entry));
			}
			box->setCurrentText(QString::fromStdString(str_select));
			connect(box, &QComboBox::currentTextChanged, this, &DropDown::onTextChanged);

			auto VBox = new QVBoxLayout;
			VBox->addWidget(new QLabel(QString::fromStdString(m_name) + ":"));
			VBox->addWidget(box);
			VBox->setSpacing(0);
			VBox->setContentsMargins(0, 0, 0, 0);

			this->setLayout(VBox);
		}
	}

	void DropDown::onTextChanged(const QString& text) {
		g_params[m_type][m_name] = text.toStdString();
	}
	
	void DropDown::onIndexChanged(int index) {
		g_params[m_type][m_name] = index;
	}
}