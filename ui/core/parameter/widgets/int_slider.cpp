#include "int_slider.h"
#include "../../../core/global_ui.h"
#include <QSlider>
#include <QSpinBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>

namespace ofec_demo {
	IntSlider::IntSlider(ParamType type, const std::string& name, int min, int max, int val, int step) :
		ParamWidget(type, name)
	{
		if (g_params[m_type].count(m_name) > 0)
			val = std::get<int>(g_params[m_type][m_name]);

		auto slider = new QSlider(Qt::Horizontal);
		slider->setRange(min, max);
		if (val >= min && val <= max)
			slider->setValue(val);
		else
			slider->setValue(min);
		slider->setSingleStep(step);

		auto spin_box = new QSpinBox;
		spin_box->setRange(slider->minimum(), slider->maximum());
		spin_box->setValue(slider->value());
		spin_box->setSingleStep(step);
		spin_box->setFocusPolicy(Qt::ClickFocus);

		connect(slider, &QSlider::valueChanged, spin_box, &QSpinBox::setValue);
		connect(spin_box, QOverload<int>::of(&QSpinBox::valueChanged), slider, &QSlider::setValue);
		connect(slider, &QSlider::valueChanged, this, &IntSlider::updateParam);
		updateParam(slider->value());

		auto HBox = new QHBoxLayout;
		HBox->addWidget(slider);
		HBox->addWidget(spin_box);
#ifdef Q_OS_MACOS
		HBox->setSpacing(0);
#else
		HBox->setSpacing(5);
#endif
		HBox->setContentsMargins(0, 0, 0, 0);

		auto VBox = new QVBoxLayout;
		VBox->addWidget(new QLabel(QString::fromStdString(m_name) + ":"));
		VBox->addLayout(HBox);
#ifdef Q_OS_MACOS
		VBox->setSpacing(0);
		VBox->setContentsMargins(0, 0, 0, 0);
#else
		VBox->setSpacing(2);
		VBox->setContentsMargins(2, 2, 2, 2);
#endif

		this->setLayout(VBox);
	}

	void IntSlider::updateParam(int value) {
		g_params[m_type][m_name] = value;
	}
}