#ifndef OFEC_DEMO_CHECK_BOX_H
#define OFEC_DEMO_CHECK_BOX_H

#include "param_widget.h"

namespace ofec_demo {
	class CheckBox : public ParamWidget {
		Q_OBJECT
	public:
		CheckBox(ParamType type, const std::string &name, bool checked);
	private slots:
		void onCheckBoxToggled(bool checked);
	};
}

#endif // !OFEC_DEMO_CHECK_BOX_H
