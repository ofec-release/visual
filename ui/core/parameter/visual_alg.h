#ifndef OFEC_DEMO_VISUAL_ALG_H
#define OFEC_DEMO_VISUAL_ALG_H

#include <QWidget>
#include <atomic>
#include <QVBoxLayout>
#include <set>
#include <core/definition.h>

namespace ofec_demo {
	class VisualAlg : public QWidget {
		Q_OBJECT
	public:
		static std::atomic<bool> ms_show_solution;
	protected:
		QString m_name;
		std::set<ofec::ProTag> m_pro_tags;
		QVBoxLayout *m_layout;
	public:
		VisualAlg(const QString &name, const std::set<ofec::ProTag> &pro_tags);
		void updateLayout();
	protected:
		virtual void updateLayout_();
	private slots:
		void onCheckBoxToggled(bool checked);
	};
}

#endif //!OFEC_DEMO_VISUAL_PRO_H