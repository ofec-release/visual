#ifndef OFEC_DEMO_CNSTRCT_H
#define OFEC_DEMO_CNSTRCT_H

#include <QString>
#include <QVBoxLayout>
#include <QWidget>
#include <set>
#include <core/definition.h>

namespace ofec_demo {
	class Cnstrct : public QWidget {
		Q_OBJECT
	protected:
		QVBoxLayout* m_layout;
		QString m_name;
		std::set<ofec::ProTag> m_pro_tags;
	public:
		Cnstrct(const QString &name, const std::set<ofec::ProTag> &pro_tags);
		void updateLayout();
	protected:
		virtual void updateLayout_() {};
	};
}

#endif // !OFEC_DEMO_CNSTRCT_H
