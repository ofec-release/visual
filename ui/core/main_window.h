#ifndef OFEC_DEMO_MAINWINDOW_H
#define OFEC_DEMO_MAINWINDOW_H

#include <QMainWindow>
#include <QAction>
#include <QToolBar>
#include <QComboBox>
#include <QMessageBox>
#include "window/output_dock.h"
#include "window/graphics_area.h"
#include "window/param_dock.h"
#include "window/progress_bar.h"
#include "window/about_dialog.h"

namespace ofec_demo {
	class MainWindow : public QMainWindow {
		Q_OBJECT
	private:
		/* States of algorithm and problem */
		bool m_alg_running/*, m_pro_displaying*/;
		/* Central Widget */
		QMainWindow *m_central_widget;
		/* Log Dock */
		OutputDock *m_output_dock;
		QAction *m_show_log_dock_act;
		/* Graphics Area  */
		GraphicsArea *m_graphics_area;
		QAction *m_tile_subwindows_act;
		/* Problem Parameter Dock */
		ParamDock *m_pro_param_dock;
		QAction *m_show_pro_param_dock_act;
		/* Algorithm Parameter Dock */
		ParamDock *m_alg_param_dock;
		QAction *m_show_alg_param_dock_act;
		/* Problem Tool Bar */
		QToolBar *m_pro_tool_bar;
		QComboBox *m_pro_select_box;
		QAction *m_display_pro_act, *m_clear_pro_act;
		/* Algorithm Tool Bar */
		QToolBar *m_alg_tool_bar;
		QComboBox *m_alg_select_box;
		QAction *m_play_alg_act, *m_stop_alg_act;
		/* Progress Tool Bar */
		ProgressBar *m_progress_bar;
		/* About Dialog */
		AboutDialog *m_about_dialog;
		QAction *m_show_about_dialog_act;
		/* No-Match Message Box */
		QMessageBox *m_warning_msg_box;
		/* Quit App */
		bool m_sig_close;

	public:
		explicit MainWindow(QWidget* parent = nullptr);
		~MainWindow();

	private:
		void initRandomColors();
		void createAboutDialog();
		void createWarningMsgBox();
		void createOutputDock();
		void createProParamDock();
		void createAlgParamDock();
		void createProToolBar();
		void createAlgToolBar();
		void createGraphicsArea();
		void createProgressBar();
		void createCentralWidget();
		void createMenus();
		void updateProParamWidget();
		void updateAlgParamWidget();
		void startAlgorithm();
		void plotProblem();
		void closeEvent(QCloseEvent *event) override;

	private slots:
		void showMessage(const QString &text);
		void onShowLogDockActTriggered(bool checked);
		void onShowProParamDockActTriggered(bool checked);
		void onShowAlgParamDockActTriggered(bool checked);
		void onProNameChanged(const QString &name);
		void onAlgNameChanged(const QString& name);
		void setProChangable(bool flag);
		void setAlgChangable(bool flag);
		void onDisplayProActTriggered();
		void onClearProActTriggered();
		void onProBufferAppended();
		void onPlayAlgActTriggered();
		void onStopAlgActTriggered();
		void onAlgorithmTerminated();
		void onAlgorithmPaused();
		void onAlgExceptionMessage(const QString &txt);
		void onProExceptionMessage(const QString &txt);
	};
}

#endif // !OFEC_DEMO_MAINWINDOW_H