#include "global_ui.h"

namespace ofec_demo {
	std::atomic<bool> g_term_alg(false);
	std::atomic<bool> g_pause(false);
	std::unique_ptr<Buffer> g_buffer = nullptr;
	ofec::Random g_random(0.5);
	size_t g_num_rand_colors;
	std::vector<std::vector<double>> g_rand_colors;
	vtkSmartPointer<vtkLookupTable> g_rand_color_lut;
	std::map<ParamType, ofec::ParamMap> g_params;

#ifdef Q_OS_MACOS
	QString g_tool_button_style = "QToolButton { background-color: transparent; }"
							"QToolButton:hover { background-color: rgb(175,175,175); border-radius: 6px;}";
#endif
	QString g_icon_button_style = "QToolButton { background-color: transparent; }";
}